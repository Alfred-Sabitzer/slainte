# Password Processing

Ein Cron-Job liest regelmäßig eine im Nextcloud abgelegte Secrets.

Diese Secrets werden im Kubernetes eingespielt.
Wenn sich ein Secret geändert hat, dann werden alle Stateful-Sets im Namespace restartet (die dieses Secret benutzen).
Danach werden alle Deployments in diesem Namespace restartet.


