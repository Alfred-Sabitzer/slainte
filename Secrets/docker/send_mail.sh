#!/bin/bash
############################################################################################
#
# Versenden der Email
#
# https://www.linuxshelltips.com/curl-send-email-linux/
# https://think.unblog.ch/en/how-to-send-mail-use-curl/
# https://www.spamresource.com/2022/04/lets-send-emailwith-curl.html
# https://everything.curl.dev/usingcurl/smtp
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
#shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition

mail_send_file="${1}"

curl -v --ssl-reqd \
--url "smtp://${MAIL_HOST}" \
--user "${MAIL_USER}:${MAIL_PASSWORD}" \
--mail-from "${MAIL_USER}" \
--mail-rcpt "${MAIL_RECIPIENT}" \
--upload-file ${mail_send_file}

#
# Das geht nur von einem Mailserver aus, der eine gute IP-Adresse hat. Da kennt der Postfix nix:)
#
#2022-12-29T21:44:52.459394+01:00 mailu-postfix-59b7885885-kw5nk postfix/smtpd[189203]: NOQUEUE: reject: RCPT from unknown[78.152.90.82]: 554 5.7.1 <k8s@slainte.at>: Sender address rejected: Access denied; from=<k8s@slainte.at> to=<alfred@slainte.at> proto=ESMTP helo=<outmail.eml>
#2022-12-29T21:44:52.471061+01:00 mailu-postfix-59b7885885-kw5nk postfix/smtpd[189203]: disconnect from unknown[78.152.90.82] ehlo=2 xclient=0/1 mail=1 rcpt=0/1 quit=1 commands=4/6
#
