#!/bin/bash
############################################################################################
#
# Mail-Body zusammenkopieren.
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
#shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition

outdir="./secrets"
mail_file="${1}"

cat << EOF > ${mail_file}
Hi

Die Verarbeitung konnte erfolgreich abgeschlossen werden.

Die Letzten Logzeilen sind:

Log der Verarbeitung ("tail secrets_load.log"):

EOF

tail  secrets_load.log >> ${mail_file}

cat << EOF >> ${mail_file}

Log des Secrets-Einspielens ("tail secrets/kubectl_apply.log"):

EOF

tail ${outdir}/kubectl_apply.log >> ${mail_file}

cat << EOF >> ${mail_file}


Have FUN

EOF
