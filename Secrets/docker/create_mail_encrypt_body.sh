#!/bin/bash
############################################################################################
#
# Erzeugen des verschlüsselten Mail-bodys
#
# Usage: crate_mail_encrypt_body.sh  "crypt-File" "crypt"
#
############################################################################################
#shopt -o -s errexit # Terminates  the shell script  if a command returns an error code.
#shopt -o -s xtrace  # Displays each command before it’s executed.
shopt -o -s nounset  # No Variables without definition

body="$(cat ${1})"
boundary="ZZ_/afg6432dfgkl.94531q"

get_mimetype(){
  # warning: assumes that the passed file exists
  file --mime-type "$1" | sed 's/.*: //'
}

# Build Body
{
printf '%s\n' "MIME-Version: 1.0
Content-Type: multipart/encrypted;
 protocol=\"application/pgp-encrypted\";
 boundary=\"$boundary\"

This is an OpenPGP/MIME encrypted message (RFC 4880 and 3156)
--${boundary}
Content-Type: application/pgp-encrypted
Content-Description: PGP/MIME version identification

Version: 1

--${boundary}
Content-Type: application/octet-stream; name=\"encrypted.asc\"
Content-Description: OpenPGP encrypted message
Content-Disposition: inline; filename=\"encrypted.asc\"

$body

"
# print last boundary with closing --
printf '%s\n' "--${boundary}--"
}