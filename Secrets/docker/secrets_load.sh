#!/bin/bash
############################################################################################
#
# Holen der Secrets aus Nextcloud und Einspielen in den Cluster
# Muster: https://linuxfun.org/en/2021/07/02/nextcloud-operation-api-with-curl-en/
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
# Paradox: Token muß als Secret bereits vorhanden sein:)
# Wenn sich das Token ändert, muß dieses Secret per Hand eingespielt werden.
date

user="${NEXTCLOUD_USER}"
pass="${NEXTCLOUD_TOKEN}"
nextcloud_url="${NEXTCLOUD_SERVER}"
indir="secrets"
infile="secrets.txt"
outdir="./secrets"
outfile="secrets.txt"
certdir="./certs"
certfile="k8s.conf"

echo "###################################################################"
echo "# Vorbereiten des Verzeichnisses   "
echo "###################################################################"

rm -R -f "${outdir}"
mkdir -p "${outdir}"

rm -R -f "${certdir}"
mkdir -p "${certdir}"


touch ${outdir}/namespaces.txt
touch ${outdir}/kubectl_apply.log
touch ${outdir}/namespaces_secrets.txt
touch ${outdir}/namespaces_secrets_reason.txt
touch ${outdir}/namespaces_statefulsets.txt
touch ${outdir}/restart_commandos.txt
touch ${outdir}/mail_results.txt

curl -X GET -u ${user}:${pass} ${nextcloud_url}/remote.php/dav/files/${user}/${certdir}/${certfile} --output ${certdir}/${certfile}
curl -X GET -u ${user}:${pass} ${nextcloud_url}/remote.php/dav/files/${user}/${indir}/${infile} --output ${outdir}/${outfile}
#
cat ${outdir}/${outfile}
echo "###################################################################"
echo "# Lesen aller Secrets   "
echo "###################################################################"
#
cat ${outdir}/${outfile} |
while IFS="_" read -r Namespace Secretyaml
do
  echo "Namespace: ${Namespace} Secret: ${Secretyaml}"
# Laden der einzelnen Secrets
  curl -X GET -u ${user}:${pass} ${nextcloud_url}/remote.php/dav/files/${user}/${indir}/${Namespace}_${Secretyaml} --output ${outdir}/${Namespace}_${Secretyaml}
  kubectl --kubeconfig ${certdir}/${certfile} apply -f "${outdir}/${Namespace}_${Secretyaml}" > ${outdir}/kubectl.log
  ret=$?
  cat ${outdir}/kubectl.log >> ${outdir}/kubectl_apply.log
  if [ ${ret} -eq 0 ]
  then
# Merken Namespace und Secretname, wenn sich was geändert hat
    unchanged=$(cat ${outdir}/kubectl.log | grep -i unchanged)
    if [ -z "${unchanged}" ]
    then
      echo "${Namespace}" >> ${outdir}/namespaces.txt
      echo "${Namespace}_${Secretyaml}" >> ${outdir}/namespaces_secrets.txt
      echo "${Namespace}_${Secretyaml}" >> ${outdir}/namespaces_secrets_reason.txt
      cat ${outdir}/kubectl.log >> ${outdir}/namespaces_secrets_reason.txt
    else
      echo "Unverändert"
    fi
  else
    cat "${outdir}/${Namespace}_${Secretyaml}" >> ${outdir}/mail_results.txt
    cat ${outdir}/kubectl.log >> ${outdir}/mail_results.txt
    echo "--------------------------------" >> ${outdir}/mail_results.txt
    echo "################################################ "
    echo "# Geändert ${outdir}/${Namespace}_${Secretyaml} # "
    echo "################################################ "
    touch ${outdir}/send_email.mail
  fi
done

sort -u ${outdir}/namespaces.txt > ${outdir}/namespaces.sort
sort -u ${outdir}/namespaces_secrets.txt > ${outdir}/namespaces_secrets.sort

echo "###################################################################"
echo "# Suchen der Stateful-Sets im Namespace die das Secret verwenden   "
echo "###################################################################"
cat ${outdir}/namespaces_secrets.sort
cat ${outdir}/namespaces_secrets.sort |
while IFS="_" read -r Namespace Secretyaml
do
  Secret=${Secretyaml%.*}
  echo "Namespace: ${Namespace} Yaml: ${Secretyaml} Secret: ${Secret}"
# Merken der Statefulsets
  kubectl --kubeconfig ${certdir}/${certfile} get statefulsets.apps -n ${Namespace} | grep -v  READY > ${outdir}/statefulsets.txt
  cat ${outdir}/statefulsets.txt |
  while IFS=" " read -r Name Ready Age
  do
    echo "Statefulset: ${Name}"
    secret_used=$(kubectl describe -n ${Namespace} statefulsets.apps ${Name} | grep -i ${Secret})
    if [ -z "${secret_used}" ]
    then
      echo "${Namespace}_${Name} - ${secret_used}" >> ${outdir}/namespaces_statefulsets_not_used.txt
    else
      echo "${Namespace}_${Name}" >> ${outdir}/namespaces_statefulsets.txt
    fi
  done
done

# Sortieren auf unique (Statefulset nur einmal restarten, auch wenn mehrere Passwoorte geändert wurden).
sort -u ${outdir}/namespaces_statefulsets.txt > ${outdir}/namespaces_statefulsets.sort

echo "###################################################################"
echo "# Restarten der Statefulsets im Namespace "
echo "###################################################################"
cat ${outdir}/namespaces_statefulsets.sort
cat ${outdir}/namespaces_statefulsets.sort |
while IFS="_" read -r Namespace Statefulset
do
  touch ${outdir}/statefulsets.done
  echo "kubectl --kubeconfig ${certdir}/${certfile} -n ${Namespace} rollout restart statefulset ${Statefulset}" >> ${outdir}/restart_commandos.txt
  kubectl --kubeconfig ${certdir}/${certfile} -n ${Namespace} rollout restart statefulset ${Statefulset} > ${outdir}/kubectl.log
  ret=$?
  if [ ${ret} -ne 0 ]
  then
    echo "${Namespace} Statefulset: ${Statefulset}" >> ${outdir}/mail_results.txt
    cat ${outdir}/kubectl.log >> ${outdir}/mail_results.txt
    echo "--------------------------------" >> ${outdir}/mail_results.txt
    echo "################################################ "
    echo "# Geändertes Statefulset ${Statefulset} # "
    echo "################################################ "
    touch ${outdir}/send_email.mail
  fi
done

if [[ ! -f "${outdir}/statefulsets.done" ]]; then
  echo "###################################################################"
  echo "# Es sind keine Statefulsets betroffen. "
  echo "###################################################################"
  date
else
  echo "###################################################################"
  echo "# Warten, bis alle Pods wieder laufen "
  echo "###################################################################"
  ./check_running_pods.sh "${outdir}" "${certfile}"
  date

  sleep 1m
  echo "###################################################################"
  echo "# Prüfen ob alle Statefulsets wieder Traffic annehmen. "
  echo "###################################################################"
  cat ${outdir}/namespaces_statefulsets.sort
  cat ${outdir}/namespaces_statefulsets.sort |
  while IFS="_" read -r Namespace Statefulset
  do
    set_status=$(kubectl --kubeconfig ${certdir}/${certfile} -n ${Namespace} describe statefulset ${Statefulset})
    waiting=$(echo ${set_status} | grep -i "/ 0 Waiting")
    failed=$(echo ${set_status} | grep -i "/ 0 Failed")
    if [ -z "${waiting}" ] || [ -z "${failed}" ]
    then
      echo "${Namespace} Statefulset nicht bereit: ${Statefulset} - ${set_status}" >> ${outdir}/mail_results.txt
      echo "--------------------------------" >> ${outdir}/mail_results.txt
      echo "################################################ "
      echo "# ${Namespace} Statefulset nicht bereit: ${Statefulset} - ${set_status} # "
      echo "################################################ "
      touch ${outdir}/send_email.mail
    else
      echo "${Namespace} Statefulset bereit: ${Statefulset} - ${set_status}"  >> ${outdir}/mail_results.txt
    fi
  done
fi

echo "###################################################################"
echo "# Restarten der Deployments in allen betroffenen Namespaces "
echo "###################################################################"
cat ${outdir}/namespaces.sort
cat ${outdir}/namespaces.sort |
while IFS="_" read -r Namespace
do
  kubectl --kubeconfig ${certdir}/${certfile} get deployments.apps -n ${Namespace} | grep -v  READY  > ${outdir}/deployments.txt
  cat ${outdir}/deployments.txt |
  while IFS=" " read -r Name Ready Age
  do
    echo "Deployment: ${Name} ${Ready} ${Age}"
    echo "kubectl --kubeconfig ${certdir}/${certfile} -n ${Namespace} rollout restart deployment ${Name}" >> ${outdir}/restart_commandos.txt
    kubectl --kubeconfig ${certdir}/${certfile} -n ${Namespace} rollout restart deployment ${Name} > ${outdir}/kubectl.log
    ret=$?
    if [ ${ret} -ne 0 ]
    then
      echo "${Namespace} Deployment: ${Name}" >> ${outdir}/mail_results.txt
      cat ${outdir}/kubectl.log >> ${outdir}/mail_results.txt
      echo "--------------------------------" >> ${outdir}/mail_results.txt
      echo "################################################ "
      echo "# ${Namespace} rollout restart deployment ${Name} # "
      echo "################################################ "
      touch ${outdir}/send_email.mail
    fi
  done
done

echo "###################################################################"
echo "# Verarbeitung beendet. "
echo "###################################################################"
date
touch ${outdir}/secrets_load.ok