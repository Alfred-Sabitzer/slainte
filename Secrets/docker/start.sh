#!/bin/bash
# Disable exit on non 0
set +e
echo "############################################################################################"
echo "#"
echo "# Starten der Containers mit den richtigen Werten."
echo "#"
echo "############################################################################################"
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
#shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
#
outdir="./secrets"
idir=$(pwd)
mail_header_file="/tmp/mail_header.txt"
mail_message_file="/tmp/mail_message.txt"
mail_body_file="/tmp/mail_body.txt"
mail_crypt_file="/tmp/mail_body.txt.asc"
mail_crypt_body_file="/tmp/mail_crypt_body.txt"
mail_send_file="/tmp/mail_send.txt"
mail_attachment_file="/tmp/mail_attachment.txt"

# Ausrollen der Secrets
${idir}/secrets_load.sh > ${idir}/secrets_load.log
# Check des Status - Mail-Senden
if  [ -f "${outdir}/send_email.mail" ]
then
  # Zippen des Verzeichnisses
  zip -r /tmp/logs.zip ${outdir}
  # Erzeugen der Mail-Message
  ${idir}/prepare_file.sh ${mail_message_file}
  # Erzeugen des Mail-boundary="ZZ_/afg6432dfgkl.94531q"Headers
  # Datei mit den Attachments
  echo "secrets_load.log" > ${mail_attachment_file}
  echo "secrets/kubectl.log" >> ${mail_attachment_file}
  echo "secrets/restart_commandos.txt" >> ${mail_attachment_file}
  echo "/tmp/logs.zip" >> ${mail_attachment_file}
  # Erzeugen des Mail-Bodys
else
  ${idir}/prepare_positiv_file.sh ${mail_message_file}
  # Erzeugen des Mail-boundary="ZZ_/afg6432dfgkl.94531q"Headers
  # Zippen des Verzeichnisses
  zip -r /tmp/logs.zip ${outdir}
#  touch /tmp/null_file.txt
#  echo /tmp/null_file.txt > ${mail_attachment_file}
  echo "secrets_load.log" > ${mail_attachment_file}
  echo "secrets/kubectl.log" >> ${mail_attachment_file}
  echo "secrets/restart_commandos.txt" >> ${mail_attachment_file}
  echo "/tmp/logs.zip" >> ${mail_attachment_file}
fi

${idir}/create_mail_header.sh \
  "${MAIL_USER}" \
  "K8S Roboter" \
  "${MAIL_RECIPIENT}" \
  "${MAIL_RECIPIENT_NAME}" \
  "Secrets Load - Status von ${MAIL_USER}" > ${mail_header_file}
${idir}/create_mail_body.sh \
  "${mail_message_file}" \
  "${mail_attachment_file}"  > ${mail_body_file}
                # Erzeugen des versandtbereiten Files - Normale Email
                # cat ${mail_header_file} ${mail_body_file} > ${mail_send_file}
                # ${idir}/send_mail.sh ${mail_send_file}
# Erzeugen des verschlüsselten Mail-Bodys
${idir}/create_mail_encrypt_envelope.sh "${mail_body_file}"
# Erzeugen des versandtbereiten Files - Verschlüsselte und signierte Email
${idir}/create_mail_encrypt_body.sh \
  "${mail_crypt_file}" > ${mail_crypt_body_file}
# Erzeugen des versandtbereiten Files - Verschlüsselte und signierte Email
cat ${mail_header_file} ${mail_crypt_body_file} > ${mail_send_file}
${idir}/send_mail.sh ${mail_send_file}
#
