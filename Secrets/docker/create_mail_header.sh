#!/bin/bash
############################################################################################
#
# Erzeugen des Mail-Headers
# https://backreference.org/2013/05/22/send-email-with-attachments-from-script-or-command-line/index.html
#
# Usage: crate_mail_header.sh "me@home.at" "you@somewhere.com" "Interesting Subject"
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
#shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition

from="${1}"
fromName="${2}"
to="${3}"
toName="${4}"
subject="${5}"

msgid=$(date '+%Y-%m-%d-%H:%M:%S')
# https://en.wikipedia.org/wiki/Message-ID
# Build headers
{
printf '%s\n' "Date: $(date +'%a, %-d %b %Y %H:%M:%S %z')
To: ${toName} <${to}>
From: ${fromName} <${from}>
Subject: $subject
Message-ID: <${msgid}@slainte.at>
Return-Path: <do_not_reply@slainte.at>"
}