#!/bin/bash
############################################################################################
#
# Erzeugen des Encryption Envelopes
#
# Usage: crate_mail_encrypt_envelope.sh "Message-Text-File"
#
############################################################################################
#shopt -o -s errexit # Terminates  the shell script  if a command returns an error code.
#shopt -o -s xtrace  # Displays each command before it’s executed.
shopt -o -s nounset  # No Variables without definition

infile="${1}"
user="${NEXTCLOUD_USER}"
pass="${NEXTCLOUD_TOKEN}"
nextcloud_url="${NEXTCLOUD_SERVER}"
gpgdir="./gpg"
secretkey="${MAIL_SECRET_KEY}"
publickey="${MAIL_PUBLIC_KEY}"
recipientkey="${MAIL_RECIPIENT_KEY}"

rm -R -f "${gpgdir}"
mkdir -p "${gpgdir}"

curl -X GET -u ${user}:${pass} ${nextcloud_url}/remote.php/dav/files/${user}/gpg/"${secretkey}"  --output ${gpgdir}/secretkey.asc
curl -X GET -u ${user}:${pass} ${nextcloud_url}/remote.php/dav/files/${user}/gpg/"${publickey}"  --output ${gpgdir}/publickey.asc
curl -X GET -u ${user}:${pass} ${nextcloud_url}/remote.php/dav/files/${user}/gpg/"${recipientkey}"  --output ${gpgdir}/recipientkey.asc

gpg -v --batch --allow-secret-key-import --passphrase ${MAIL_SECRET_KEY_PASSWORD} --import ${gpgdir}/secretkey.asc

gpg -v --batch --import ${gpgdir}/publickey.asc
gpg -v --batch --import ${gpgdir}/recipientkey.asc
gpg --list-keys
gpg --list-public-keys
gpg --list-secret-keys

# https://unix.stackexchange.com/questions/60213/gpg-asks-for-password-even-with-passphrase
# https://stackoverflow.com/questions/36356924/not-a-tty-error-in-alpine-based-duplicity-image
cat << EOF | gpg --no-tty --batch --yes --passphrase-fd 0 --pinentry-mode loopback --trust-model always -se --armor --recipient ${MAIL_RECIPIENT} ${infile}
${MAIL_SECRET_KEY_PASSWORD}
EOF

#