#!/bin/bash
############################################################################################
#
# Erzeugen des Mail-bodys
#
# Usage: crate_mail_body.sh  "Message-Text-File" "List of attachments"
#
############################################################################################
#shopt -o -s errexit # Terminates  the shell script  if a command returns an error code.
#shopt -o -s xtrace  # Displays each command before it’s executed.
shopt -o -s nounset  # No Variables without definition

body="$(cat ${1})"
attachments=${2}
boundary="ZZ_/afg6432dfgkl.94531q"

get_mimetype(){
  # warning: assumes that the passed file exists
  file --mime-type "$1" | sed 's/.*: //'
}

# Build Body
{
printf '%s\n' "MIME-Version: 1.0
Content-Language: en-US, de-AT-frami
Content-Type: multipart/mixed; boundary=\"$boundary\"

--${boundary}
Content-Type: text/plain; charset=UTF-8; format=flowed
Content-Transfer-Encoding: 8bit
Content-Disposition: inline

$body
"

# now loop over the attachments, guess the type
# and produce the corresponding part, encoded base64
cat ${attachments} |
while IFS=$'\n' read -r file
do

  [ ! -f "$file" ] && echo "Warning: attachment $file not found, skipping" >&2 && continue
  if [ -s "$file" ] # Checks if file has size greater than 0
  then
    mimetype=$(get_mimetype "$file")
#
# qprint nicht unter Alpine verfügbar
#
# Es ist auch nicht so leicht festzustellen, ob alles nur "7-Bit" character sind.
# Dann könnte man mit gprint den Text als "quoted-printable" darstellen.
# Das bringt beim rspamd 1 Punkt:)
# So wird halt alles als base64 encoded.
#
#    echo "-------$mimetype-----------------"
#    if [ "${mimetype}" == "text/plain" ]
#    then
#      printf '%s\n' "--${boundary}
#Content-Type: $mimetype
#Content-Transfer-Encoding: quoted-printable
#Content-Disposition: attachment; filename=\"$file\"
#"
#      cat "$file"
#    elif [ "${mimetype}" == "text/html" ]
#    then
#      printf '%s\n' "--${boundary}
#Content-Type: $mimetype
#Content-Transfer-Encoding: quoted-printable
#Content-Disposition: attachment; filename=\"$file\"
#"
#      cat "$file"
#    elif [ "${mimetype}" == "text/enriched" ]
#    then
#      printf '%s\n' "--${boundary}
#Content-Type: $mimetype
#Content-Transfer-Encoding: quoted-printable
#Content-Disposition: attachment; filename=\"$file\"
#"
#      cat "$file"
#    else
#      printf '%s\n' "--${boundary}
#Content-Type: $mimetype
#Content-Transfer-Encoding: base64
#Content-Disposition: attachment; filename=\"$file\"
#"
#      base64 "$file"
#    fi
      printf '%s\n' "--${boundary}
Content-Type: $mimetype
Content-Transfer-Encoding: base64
Content-Disposition: attachment; filename=\"$file\"
"
      base64 "$file"
    echo
  fi
done

# print last boundary with closing --
printf '%s\n' "--${boundary}--"
}