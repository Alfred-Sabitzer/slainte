#!/bin/bash
# Connect in den Container
ns="secrets"
app="secret"
kubectl exec -i -t -n ${ns} $(kubectl get pod -n ${ns} | grep -i ${app} | awk '{print $1 }') -c ${app} "--" sh -c "clear; (bash || ash || sh)"
