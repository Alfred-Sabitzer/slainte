# Persistant Volume Claims

Definition aller Persistant Volume Claims für alle Teilprojekte.

Muster:

```
---
kind: PersistentVolumeClaim
apiVersion: v1
metadata:
  name: nextcloud-app-pvc
  namespace: slainte
spec:
  storageClassName: openebs-hostpath
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 250G
---
```
Es wird die OpenEBS-Klasse verwendet. Die Standard-Hostpath kommt manchmal in der Zuordnung der PV's zur PVC durcheinander (dann schreiben mehrere Appliaktionen in dasselbe Verzeichnis).