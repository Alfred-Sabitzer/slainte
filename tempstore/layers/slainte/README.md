# Kustomization Slainte

Wir verwenden kubectl kustomize
Siehe auch https://kubernetes.io/docs/tasks/manage-kubernetes-objects/kustomization/

Im Verzeichnis Base befinden sich alle Resourcen, für die Kustomize angewandt werden soll.

Es gibt dann noch Umgebungsspezifische Overlays.
Es wird zwischen "slainte" (das ist https://www.slainte.at) und "k8s" (das ist meine lokale Entwicklungsumgebung zu Hause) unterschieden.

Der Unterschied ist dann in den Hostnamen.

Siehe auch https://github.com/kubernetes-sigs/kustomize/tree/master/examples/transformerconfigs sowie https://www.jetstack.io/blog/kustomize-cert-manager/

```bash
kubectl kustomize slainte/
```

Dieses Statement dient zum Prüfen der generierten Ingresse.

```bash
kubectl apply -k slainte/
```

Dieses Statement deployed das in den Cluster.