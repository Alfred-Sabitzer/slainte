#!/bin/bash
############################################################################################
#
# Bauen und deployen
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
#shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
git pull --ff-only
# Bauen gegen das Ziel-Repository
tag=$(date +"%Y%m%d")
docker_registry="docker.slainte.at"
docker_username=$(cat ${HOME}/.password/docker_username.txt)
docker_password=$(cat ${HOME}/.password/docker_password.txt)
docker login ${docker_registry} -u ${docker_username} -p ${docker_password}
image="mailu/redis"
docker build --no-cache --force-rm . -t ${docker_registry}/${image}:${tag} -t ${docker_registry}/${image}:latest -f dockerfile
docker push ${docker_registry}/${image}:${tag}
docker push ${docker_registry}/${image}:latest
curl -u ${docker_username}:${docker_password} -k https://${docker_registry}/v2/${image}/tags/list
#
