#!/bin/sh
# Disable exit on non 0
set +ex
echo "############################################################################################"
echo "#"
echo "# Starten Mailu Admin container mit den richtigen Werten."
echo "#"
echo "############################################################################################"
#
echo "# Applikation starten"
/bin/sh -c /start.py
#echo "# Sicherstellen, dass der Prozess oben bleibt"
/dummy.sh
#