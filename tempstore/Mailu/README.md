# Mailu Mailserver

Installation eines K8S-Mailservers https://mailu.io

# Installation

```bash
helm repo add mailu https://mailu.github.io/helm-charts/
helm show values mailu/mailu > my-values-file.yaml
helm install mailu mailu/mailu -n mailu --values my-values-file.yaml


helm uninstall mailu --namespace=mailu
```

Am DNS-Server müssen ein paar Einstellungen gemacht werden. Dazu dienen die Empfehlungen des admins. 

https://dmarcly.com/tools/spf-dkim-dmarc-wizard/#create-spf-record
https://mxtoolbox.com/diagnostic.aspx
https://geekflare.com/de/smtp-testing-tools/
https://www.immuniweb.com/ssl/slainte.at/BHtGaATk/
https://www.wormly.com/test-smtp-server/host/mail.slainte.at/port/25/sendmail/1/id/W1LzVDaq
https://www.getresponse.com/de/hilfe/was-ist-dkim-und-wie-richte-ich-es-ein.html
https://easydmarc.com/tools/dkim-record-generator