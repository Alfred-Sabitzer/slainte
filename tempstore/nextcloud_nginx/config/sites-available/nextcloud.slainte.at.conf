#
# Siehe auch https://docs.nextcloud.com/server/24/admin_manual/installation/nginx.html
#

upstream php_nextcloud_slainte_at {
    server nextcloud-app.slainte.svc.cluster.local:9000;
    server nextcloud-app.slainte.svc.cluster.local:9000 backup;
}

# Set the `immutable` cache control options only for assets with a cache busting `v` argument
map $arg_v $asset_immutable {
    "" "";
    default "immutable";
}

set_real_ip_from  10.0.0.0/8;
set_real_ip_from  172.16.0.0/12;
set_real_ip_from  192.168.0.0/16;
real_ip_header    X-Real-IP;

#
# Inspiratioan https://wiki.mageia.org/en/Nextcloud_server_installation_with_NGINX
#

server {
    listen              443 ssl http2 default_server reuseport;
    server_name         nextcloud.slainte.at;
    set                 $base /var/www/html;
    root                $base;

    # SSL
    ssl_certificate     /etc/nginx/ssl/tls.crt;
    ssl_certificate_key /etc/nginx/ssl/tls.key;

    # Prevent nginx HTTP Server Detection
    server_tokens off;

    # HSTS settings
    # WARNING: Only add the preload option once you read about
    # the consequences in https://hstspreload.org/. This option
    # will add the domain to a hardcoded list that is shipped
    # in all major browsers and getting removed from this list
    # could take several months.
    #add_header Strict-Transport-Security "max-age=15768000; includeSubDomains; preload" always;

    # set max upload size and increase upload timeout:
    client_max_body_size 512M;
    client_body_timeout 300s;
    fastcgi_buffers 64 4K;


    # Enable gzip but do not remove ETag headers
    gzip on;
    gzip_vary on;
    gzip_comp_level 4;
    gzip_min_length 256;
    gzip_proxied expired no-cache no-store private no_last_modified no_etag auth;
    gzip_types application/atom+xml application/javascript application/json application/ld+json application/manifest+json application/rss+xml application/vnd.geo+json application/vnd.ms-fontobject application/wasm application/x-font-ttf application/x-web-app-manifest+json application/xhtml+xml application/xml font/opentype image/bmp image/svg+xml image/x-icon text/cache-manifest text/css text/plain text/vcard text/vnd.rim.location.xloc text/vtt text/x-component text/x-cross-domain-policy;

    # Pagespeed is not supported by Nextcloud, so if your server is built
    # with the `ngx_pagespeed` module, uncomment this line to disable it.
    #pagespeed off;

    # HTTP response headers borrowed from Nextcloud `.htaccess`
#    add_header Referrer-Policy                      "no-referrer"   always;
    add_header Referrer-Policy                      "no-referrer-when-downgrade" always;
    add_header X-Content-Type-Options               "nosniff"       always;
    add_header Content-Security-Policy              "default-src 'self' http: https: ws: wss: data: blob: 'unsafe-inline'; frame-ancestors 'self';" always;
    add_header X-Download-Options                   "noopen"        always;
    add_header X-Frame-Options                      "SAMEORIGIN"    always;
    add_header X-Permitted-Cross-Domain-Policies    "none"          always;
    add_header X-Robots-Tag                         "none"          always;
    add_header X-XSS-Protection                     "1; mode=block" always;
    add_header Permissions-Policy                   "interest-cohort=()" always;
# add_header Strict-Transport-Security "max-age=31536000; includeSubDomains; preload" always;


    # Remove X-Powered-By, which is an information leak
    fastcgi_hide_header X-Powered-By;

    # Specify how to handle directories -- specifying `/index.php$request_uri`
    # here as the fallback means that Nginx always exhibits the desired behaviour
    # when a client requests a path that corresponds to a directory that exists
    # on the server. In particular, if that directory contains an index.php file,
    # that file is correctly served; if it doesn't, then the request is passed to
    # the front-end controller. This consistent behaviour means that we don't need
    # to specify custom rules for certain paths (e.g. images and other assets,
    # `/updater`, `/ocm-provider`, `/ocs-provider`), and thus
    # `try_files $uri $uri/ /index.php$request_uri`
    # always provides the desired behaviour.
    index index.php index.html /index.php$request_uri;

    # Rule borrowed from `.htaccess` to handle Microsoft DAV clients
    location = / {
        if ( $http_user_agent ~ ^DavClnt ) {
            return 302 /remote.php/webdav/$is_args$args;
        }
        try_files $uri $uri/ /index.php$request_uri;
    }

    # nginx configuration by winginx.com
    autoindex off;

    charset utf-8;

    error_page 404 /;

    error_page 403 /;

#   RewriteEngine on
#   RewriteCond %{HTTP_USER_AGENT} DavClnt
#   RewriteRule ^$ /remote.php/webdav/ [L,R=302]
#   RewriteRule .* - [env=HTTP_AUTHORIZATION:%{HTTP:Authorization}]
#   RewriteRule ^\.well-known/carddav /remote.php/dav/ [R=301,L]
#   RewriteRule ^\.well-known/caldav /remote.php/dav/ [R=301,L]
#   RewriteRule ^remote/(.*) remote.php [QSA,L]
#   RewriteRule ^(?:build|tests|config|lib|3rdparty|templates)/.* - [R=404,L]
#   RewriteRule ^\.well-known/(?!acme-challenge|pki-validation) /index.php [QSA,L]
#   RewriteRule ^(?:\.(?!well-known)|autotest|occ|issue|indie|db_|console).* - [R=404,L]

    # https://docs.nextcloud.com/server/21/admin_manual/installation/nginx.html
    # Make a regex exception for `/.well-known` so that clients can still
    # access it despite the existence of the regex rule
    # `location ~ /(\.|autotest|...)` which would otherwise handle requests
    # for `/.well-known`.
    location ^~ /.well-known {
        # The rules in this block are an adaptation of the rules
        # in the Nextcloud `.htaccess` that concern `/.well-known`.

        location = /.well-known/carddav { return 301 $scheme://$host/remote.php/dav/; }
        location = /.well-known/caldav  { return 301 $scheme://$host/remote.php/dav/; }

        # https://help.nextcloud.com/t/nextcloud-21-und-webfinger/108772/17
        location = /.well-known/webfinger { return 301 $scheme://$host/public.php?service=webfinger/; }
        location = /.well-known/nodeinfo  { return 301 $scheme://$host/public.php?service=nodeinfo/; }

        location /.well-known/acme-challenge    { try_files $uri $uri/ =404; }
        location /.well-known/pki-validation    { try_files $uri $uri/ =404; }

        # Let Nextcloud's API for `/.well-known` URIs handle all other
        # requests by passing them to the front-end controller.
        return 301 $scheme://$host/index.php$request_uri;
    }

    location / {
        rewrite ^ /index.php;
    }

    location ~ ^\/(?:build|tests|config|lib|3rdparty|templates|data)\/ {
        deny all;
    }
    location ~ ^\/(?:\.|autotest|occ|issue|indie|db_|console) {
        deny all;
    }

    # https://docs.nextcloud.com/server/24/admin_manual/installation/nginx.html
    # Required for legacy support
    rewrite ^/nextcloud/(?!index|remote|public|cron|core\/ajax\/update|status|ocs\/v[12]|updater\/.+|oc[ms]-provider\/.+|.+\/richdocumentscode\/proxy) /nextcloud/index.php$request_uri;

    # https://github.com/nextcloud/docker/blob/65f3f6d167defbef27d01819da34314e0569040d/.examples/docker-compose/with-nginx-proxy/mariadb/fpm/web/nginx.conf#L78
    location ~ ^\/(?:index|remote|public|cron|core\/ajax\/update|status|ocs\/v[12]|updater\/.+|oc[ms]-provider\/.+)\.php(?:$|\/) {
        fastcgi_split_path_info ^(.+?\.php)(\/.*|)$;
        set $path_info $fastcgi_path_info;
        try_files $fastcgi_script_name =404;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $fastcgi_script_name;
        #fastcgi_param PATH_INFO $path_info;
        # fastcgi_param HTTPS on;

        # Avoid sending the security headers twice
        fastcgi_param modHeadersAvailable true;

        # Enable pretty urls
        fastcgi_param front_controller_active true;
        fastcgi_pass php_nextcloud_slainte_at;
        fastcgi_intercept_errors on;
        fastcgi_request_buffering off;
    }

    location ~ ^\/(?:updater|oc[ms]-provider)(?:$|\/) {
        try_files $uri/ =404;
        index index.php;
    }

    # https://github.com/nextcloud/documentation/blob/75fc0d26c345fd972c3b03b5ed34bfd94bc4a8d2/admin_manual/installation/nginx.rst
    # https://bobcares.com/blog/nextcloud-nginx-ssl/
    # Adding the cache control header for js and css files
    # Make sure it is BELOW the PHP block
    location ~ \.(?:css|js|woff2?|svg|gif|map)$ {
        try_files $uri /index.php$request_uri;
        add_header Cache-Control "public, max-age=15778463";
        add_header Referrer-Policy "no-referrer" always;
        add_header X-Content-Type-Options "nosniff" always;
        add_header X-Download-Options "noopen" always;
        add_header X-Frame-Options "SAMEORIGIN" always;
        add_header X-Permitted-Cross-Domain-Policies "none" always;
        add_header X-Robots-Tag "none" always;
        add_header X-XSS-Protection "1; mode=block" always;

        access_log off;
    }

    location ~ \.(?:png|html|ttf|ico|jpg|jpeg|bcmap|mp4|webm)$ {
        try_files $uri /index.php$request_uri;
        # Optional: Don't log access to other assets
        access_log off;
    }

    # Rules borrowed from `.htaccess` to hide certain paths from clients
    location ~ ^\/(?:build|tests|config|lib|3rdparty|templates|data)\/ {
        deny all;
    }

    # security
    include             nginxconfig.io/security.conf;

    location ~ \.(?:css|js|svg|gif|png|jpg|ico|wasm|tflite|map)$ {
        try_files $uri /index.php$request_uri;
        add_header Cache-Control "public, max-age=15778463, $asset_immutable";
        access_log off;     # Optional: Don't log access to assets

        location ~ \.wasm$ {
            default_type application/wasm;
        }
    }

    # index.php fallback
    location ~ ^/api/ {
        try_files $uri $uri index.php?$query_string;
    }

    # additional config
    include nginxconfig.io/general.conf;

    location = /data/htaccesstest.txt {
      allow all;
      log_not_found off;
      access_log off;
    }

}

# non-www, subdomains redirect
server {
    listen              443 ssl http2;
    server_name         .nextcloud.slainte.at;

    access_log  off;

    # SSL
    ssl_certificate     /etc/nginx/ssl/tls.crt;
    ssl_certificate_key /etc/nginx/ssl/tls.key;
    return              301 https://www.nextcloud.slainte.at$request_uri;
}

server {
    listen      80  default_server reuseport;
    server_name _;
    set                 $base /var/www/html;
    root                $base;

    access_log  off;

    # Prevent nginx HTTP Server Detection
    server_tokens off;

    # health checks in cloud providers require the use of port 80
    location = /healthz {
        allow 127.0.0.1;
        allow 192.168.0.0/16;
        allow 10.0.0.0/8;
        deny all;
        return 200 'healthy';
    }

    location ~ ^/(status|ping)$ {
        allow 127.0.0.1;
        allow 192.168.0.0/16;
        allow 10.0.0.0/8;
        deny all;
        fastcgi_param SCRIPT_FILENAME $fastcgi_script_name;
        fastcgi_index index.php;
        include fastcgi_params;
        fastcgi_pass php_nextcloud_slainte_at;
    }

	location = /nginx_status {
        allow 127.0.0.1;
        allow 192.168.0.0/16;
        allow 10.0.0.0/8;
        deny all;
        stub_status on;
    }

	location ~ ^/ {
        return      301 https://nextcloud.slainte.at$request_uri;
    }
}