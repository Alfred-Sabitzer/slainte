#!/bin/sh
# Disable exit on non 0
set +e
echo "############################################################################################"
echo "#"
echo "# Starten des Exporters mit den richtigen Werten."
echo "#"
echo "############################################################################################"
#
echo "$@"
#$ nextcloud-exporter --help
#Usage of nextcloud-exporter:
#  -a, --addr string          Address to listen on for connections. (default ":9205")
#      --auth-token string    Authentication token. Can replace username and password when using Nextcloud 22 or newer.
#  -c, --config-file string   Path to YAML configuration file.
#      --login                Use interactive login to create app password.
#  -p, --password string      Password for connecting to Nextcloud.
#  -s, --server string        URL to Nextcloud server.
#  -t, --timeout duration     Timeout for getting server info document. (default 5s)
#      --tls-skip-verify      Skip certificate verification of Nextcloud server.
#  -u, --username string      Username for connecting to Nextcloud.
#  -V, --version              Show version information and exit.
/bin/nextcloud-exporter --auth-token ${NEXTCLOUD_AUTH_TOKEN}
# Das kann gar nicht passieren. Im Fehlerfalle wird der Prozess hier gefangen.
/dummy.sh
#