#!/bin/bash
############################################################################################
#
# Erzeugen einer einfachen Mail
# https://backreference.org/2013/05/22/send-email-with-attachments-from-script-or-command-line/index.html
#
############################################################################################
outdir="./secrets"

from="${MAIL_USER}"
to="${MAIL_RECIPIENT}"
subject="Secrets Load - Status von ${MAIL_USER}"
boundary="ZZ_/afg6432dfgkl.94531q"

cat << EOF > /tmp/mail_message
Hi

Es sind in der Verarbeitung Fehler aufgetreten.
Genaue Details finden sich in den angehängten Dateien.

Die Letzten Logzeilen sind:

Log der Verarbeitung ("tail secrets_load.log"):

EOF

tail  secrets_load.log >> /tmp/mail_message

cat << EOF >> /tmp/mail_message

Log des Secrets-Einspielens ("tail secrets/kubectl_apply.log"):

EOF

tail ${outdir}/kubectl_apply.log >> /tmp/mail_message

cat << EOF >> /tmp/mail_message

Hoffe, damit kann der Fehler eingegrenzt werden.

Have FUN

EOF

attachments

attachments=( "secrets_load.log" "secrets/kubectl.log" "secrets/restart_commandos.txt" "/tmp/logs.zip" )

get_mimetype(){
  # warning: assumes that the passed file exists
  file --mime-type "$1" | sed 's/.*: //'
}

# Build headers
{

printf '%s\n' "From: $from
Date: $(date +'%a, %-d %b %Y %H:%M:%S %z')
To: $to
Subject: $subject
MIME-Version: 1.0
Content-Language: en-US, de-AT-frami
Content-Type: multipart/mixed; boundary=\"$boundary\"

--${boundary}
Content-Type: text/plain; charset=UTF-8; format=flowed
Content-Transfer-Encoding: 7bit
Content-Disposition: inline

$body
"

# now loop over the attachments, guess the type
# and produce the corresponding part, encoded base64
for file in "${attachments[@]}"; do

  [ ! -f "$file" ] && echo "Warning: attachment $file not found, skipping" >&2 && continue

  mimetype=$(get_mimetype "$file")

  printf '%s\n' "--${boundary}
Content-Type: $mimetype
Content-Transfer-Encoding: base64
Content-Disposition: attachment; filename=\"$file\"
"

  base64 "$file"
  echo
done

# print last boundary with closing --
printf '%s\n' "--${boundary}--"
}