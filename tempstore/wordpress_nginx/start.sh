#!/bin/sh
# Disable exit on non 0
set +e
echo "############################################################################################"
echo "#"
echo "# Starten des Nginx mit den richtigen Werten."
echo "#"
echo "############################################################################################"
#
#sed -i 's|set -eu|set -eux|' /docker-entrypoint.sh
# Dieser Entrypoint startet den nginx im Vordergrund
/docker-entrypoint.sh nginx -c /etc/nginx/nginx.conf -g 'daemon off;'
# Sicherstellen, dass der Prozess oben bleibt
/dummy.sh
#