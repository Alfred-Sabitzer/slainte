#!/bin/bash
############################################################################################
#
# Erzeugen einer Mail
# https://backreference.org/2013/05/22/send-email-with-attachments-from-script-or-command-line/index.html
#
# Usage: crate_mail.sh "me@home.at" "you@somewhere.com" "Interesting Subject" "Message-Text-File" "List of attachments" "Y/N"
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
#shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition

outdir="./secrets"
from="${1}"
to="${2}"
subject="${3}"
body=$(cat ${4})
attachments=( ${5} )
do_pgp="${6}"

boundary="ZZ_/afg6432dfgkl.94531q"

get_mimetype(){
  # warning: assumes that the passed file exists
  file --mime-type "$1" | sed 's/.*: //'
}

# Build headers
{

printf '%s\n' "From: $from
Date: $(date +'%a, %-d %b %Y %H:%M:%S %z')
To: $to
Subject: $subject
"
./create_mail_body.sh ${body} ${attachments}  > /tmp/mail_body.txt

if [ "${do_pgp}" == "Y" ]
then
  encrypt_message /tmp/mail_body.txt /tmp/mail_body.asc
  cat /tmp/mail_body.asc
else
  cat /tmp/mail_body.txt
fi

# print last boundary with closing --
printf '%s\n' "--${boundary}--"
}