#!/bin/bash
############################################################################################
#
# Verschlüsseln der Mail-Info
#
############################################################################################
#shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition

infile="${1}"
outfile="${2}"
user="${NEXTCLOUD_USER}"
pass="${NEXTCLOUD_TOKEN}"
nextcloud_url="${NEXTCLOUD_SERVER}"
gpgdir="./gpg"
secretkey="${MAIL_KEY}"
publickey="${MAIL_RECIPIENT_KEY}"

rm -R -f "${gpgdir}"
mkdir -p "${gpgdir}"

curl -X GET -u ${user}:${pass} ${nextcloud_url}/remote.php/dav/files/${user}/gpg/"${secretkey}"  --output ${gpgdir}/secretkey.asc
curl -X GET -u ${user}:${pass} ${nextcloud_url}/remote.php/dav/files/${user}/gpg/"${publickey}"  --output ${gpgdir}/publickey.asc

echo ${MAIL_KEY_PASSWORD} | gpg --import ${gpgdir}/secretkey.asc
gpg --import ${gpgdir}/publickey.asc
gpg --list-keys
gpg --list-public-keys
gpg --list-secret-keys

gpg --trust-model always --encrypt --sign --armor --recipient ${MAIL_RECIPIENT} ${infile} --output ${outfile}

echo "# Output ist ${outfile} "