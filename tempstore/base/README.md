# Kustomization Base

Wir verwenden kubectl kustomize
Siehe auch https://kubernetes.io/docs/tasks/manage-kubernetes-objects/kustomization/
sowie https://kubectl.docs.kubernetes.io/guides/config_management/components/

Im Verzeichnis Base befinden sich alle Resourcen, für die Kustomize angewandt werden soll.

Es gibt dann noch Umgebungsspezifische Overlays.
Es wird zwischen "slainte" (das ist https://www.slainte.at) und "k8s" (das ist meine lokale Entwicklungsumgebung zu Hause) unterschieden.

Der Unterschied ist dann in den Services und Zertifikaten.
