# Kubernetes Access-Tokens

Anlagen von Kube-Configs für den jeweiligen Nutzer. Muster aus https://github.com/kubernetes/dashboard/blob/master/docs/user/access-control/creating-sample-user.md

```bash
./k8s_authenticate.sh <username> <role>
```
Nach Durchführung dieses Skripts entstehen einige Dateien.

```bash
~/k8s$ ls -lisa
total 32
1575989 4 drwxrwxr-x  2 slainte slainte 4096 Dez 19 13:55 .
1576332 4 drwxr-x--- 11 slainte slainte 4096 Dez 19 12:30 ..
1576046 8 -rw-rw-r--  1 slainte slainte 6702 Dez 19 13:55 alfred.conf
1574398 4 -rw-rw-r--  1 slainte slainte 1237 Dez 19 13:55 alfred.crt
1576187 4 -rw-rw-r--  1 slainte slainte 1058 Dez 19 13:55 alfred.csr
1576016 4 -rw-------  1 slainte slainte 1704 Dez 19 13:55 alfred.key
1576186 4 -rwxr-xr-x  1 slainte slainte 4015 Dez 19 13:55 k8s_authenticate.sh
```
Für die Anmeldung über das Dashboard ist in diesem Beispiel das **_alfred.conf_** interessant.
Diese Conf-Datei mu8 dann jeder bei sich lokal haben, und bei der Anmeldung an das Dashboard verwenden.

![kubernetes_dashboard.png](kubernetes_dashboard.png)

Nach erfolgter Anmeldung ist man direkt im Dashboard.

![Kubernetes_Dashboard_logon.png](Kubernetes_Dashboard_logon.png)

Der Zugang erfolgt mit den Berechtigungen der Cluster-Role **"view"**. Somit kann nichts geändert werden, und manche sensitiven Inhalte sind nicht sichtbar.
