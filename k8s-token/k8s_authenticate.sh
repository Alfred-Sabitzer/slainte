#!/bin/bash
############################################################################################
#
# Certificate für einen bestimmten User. Dieses Skript muß im Cluster laufen (wegen der CA-Certificates)
#
# https://kubernetes.io/docs/tasks/administer-cluster/certificates/
# https://kubernetes.io/docs/reference/access-authn-authz/authentication/
# https://kubernetes.io/docs/reference/access-authn-authz/certificate-signing-requests/
# https://kubernetes.io/docs/tasks/access-application-cluster/configure-access-multiple-clusters/
# https://betterprogramming.pub/k8s-tips-give-access-to-your-clusterwith-a-client-certificate-dfb3b71a76fe
# https://stackoverflow.com/questions/72136038/what-does-adding-a-secret-to-a-serviceaccount-in-kubernetes-do
# https://stackoverflow.com/questions/70287656/kubernetes-dashboard-internal-error-500-not-enough-data-to-create-auth-info
# https://www.frakkingsweet.com/adding-a-full-admin-user-in-kubernetes/
# https://microk8s.io/docs/services-and-ports
#
############################################################################################
shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
#
user=${1}
role=${2}
echo "Certificate für User ${user} with role ${role}"
domain="${user}"
#password="${user}"
country="AT"
state="Österreich"
locality="ZuHause"
organizationalunit="microk8s"
email="${user}@slainte.at"

cat <<EOF | kubectl apply -f -
apiVersion: v1
kind: Secret
metadata:
  name: ${user}-sa-secret
  namespace: kube-system
  annotations:
    kubernetes.io/service-account.name: ${user}-sa
type: kubernetes.io/service-account-token
EOF

cat <<EOF | kubectl apply -f -
apiVersion: v1
kind: ServiceAccount
metadata:
  name: ${user}-sa
  namespace: kube-system
secrets:
  - name: ${user}-sa-secret
EOF

token=$(kubectl -n kube-system get secret $(kubectl -n kube-system get serviceaccount ${user}-sa -o jsonpath="{.secrets[0].name}") -o go-template="{{.data.token | base64decode}}")

cat <<EOF | microk8s.kubectl apply -f -
kind: ClusterRoleBinding
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: ${user}-cr
  namespace: kube-system
subjects:
  - kind: User
    name: ${user}
    apiGroup: 'rbac.authorization.k8s.io'
  - kind: ServiceAccount
    name: ${user}-sa
    namespace: kube-system
roleRef:
  kind: ClusterRole
  name: ${role}
  apiGroup: 'rbac.authorization.k8s.io'
EOF

organization="slainte"
commonname=$domain
#
pw=$(echo $RANDOM | md5sum | head -c 20)
openssl genrsa -out ${domain}.key -passout pass:$pw
#
openssl req -new -key ${domain}.key \
            -nodes \
            -out ${domain}.csr \
            -subj "/C=$country/ST=$state/L=$locality/O=$organization/OU=$organizationalunit/CN=$commonname/emailAddress=$email"
#
kubectl delete certificatesigningrequests.certificates.k8s.io $domain
#
b64=$(cat ${domain}.csr | base64 | tr -d "\n")
cat <<EOF | kubectl apply -f -
apiVersion: certificates.k8s.io/v1
kind: CertificateSigningRequest
metadata:
  name: $domain
spec:
  groups:
  - system:authenticated
  - view
  request: $b64
  signerName: kubernetes.io/kube-apiserver-client
  expirationSeconds: 86400000  # thousand days
  usages:
  - digital signature
  - key encipherment
  - client auth
EOF
#
kubectl get csr
kubectl certificate approve $domain
#kubectl get csr/$domain -o yaml
sleep 5s
kubectl get csr $domain -o jsonpath='{.status.certificate}'| base64 -d > ${domain}.crt
#cat  ${domain}.crt
# Jetzt müssen wir eine gültige config erzeugen, die alles für diesen einen User beinhaltet.
cp ${HOME}/.kube/config ./${domain}.conf
kubectl config --kubeconfig ./${domain}.conf delete-user admin
kubectl config --kubeconfig ./${domain}.conf set-credentials ${domain} --client-key=${domain}.key --client-certificate=${domain}.crt --embed-certs=true --token=${token}
sed -i -r "s/    user: admin/    user: ${domain}/g" ./${domain}.conf
# Diese Config wird dann versandt.
exit
#
