#!/bin/sh
# Disable exit on non 0
set +e
echo "############################################################################################"
echo "#"
echo "# Starten Postfix mit den richtigen Werten."
echo "#"
echo "############################################################################################"
#
chown postfix:postdrop /queue
chown -R postfix:postdrop /queue/*

ls -lisa /queue

python3 /start.py
#/dummy.sh
#