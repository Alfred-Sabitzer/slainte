#!/bin/bash
############################################################################################
#
# Bauen und deployen - Alle Komponenten für den mailu-Service
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
git pull --ff-only
cd ./mailu-admin
./do.sh
cd ../mailu-redis
./do.sh
cd ../mailu-clamav
./do.sh
cd ../mailu-dovecot
./do.sh
cd ../mailu-front
./do.sh
cd ../mailu-postfix
./do.sh
cd ../mailu-roundcube
./do.sh
cd ../mailu-rspamd
./do.sh
cd ../mailu-fetchmail
./do.sh
cd ../mailu-radicale
./do.sh