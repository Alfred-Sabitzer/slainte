# Mailu Mailserver

Installation eines K8S-Mailservers https://mailu.io

# Installation

```bash
helm repo add mailu https://mailu.github.io/helm-charts/
helm repo list
helm show values mailu/mailu > my-values-file.yaml
helm install mailu mailu/mailu -n mailu --values my-values-file.yaml

helm ls --all-namespaces

helm uninstall mailu --namespace=mailu
```

# DNS-Server Einstellungen

Am DNS-Server müssen ein paar Einstellungen gemacht werden. Dazu dienen die Empfehlungen des admins. 
Siehe https://www.customercontrolpanel.de/domains.php

```bash
@ 600 IN MX 10 mail.slainte.at.
@ 600 IN TXT "v=spf1 mx:mail.slainte.at ip4:89.58.16.23 a:mail.slainte.at a:imap.slainte.at a:smtp.slainte.at a:slainte.at -all"
dkim._domainkey 600 IN TXT "v=DKIM1; k=rsa; p=MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAz4zO6d5AEtHGKDc5M9SQ8DF0DnfV17sjtHhMv4oedmB4fSM0F8Bi8UhpuohnY25i2PeymA09S3mMxPhgcyIwxzxj6NzwdTL32E0zBiXIPYmJcbLBQVlmjDcah5AzGWsFXLyRcqxPjQrUwA87yk7EzPoXcYquMzrCc4nPBqmV/0dzdLNVbQ0UpsKX3gMQ" "3zm+i3WXUXeEWkfQBMv8w2IlbgL8j0QKJX1hNJZAe8TsRrR7lJqtifCzZOLGErEMaCfPlYePw+E/4gfzbWD6990m9UbeCeVXZTqjOs2cfvKZDHKuzWiQTXKMboo/CsgGIK8lrUWS34Zjdqvnr8i7Gq76YQIDAQAB"
_dmarc 600 IN TXT "v=DMARC1; p=quarantine; rua=mailto:dmarc_aggregate@slainte.at; ruf=mailto:dmarc_forensic@slainte.at; sp=quarantine; aspf=s; fo=0:1:d:s;"
slainte.at._report._dmarc 600 IN TXT "v=DMARC1"
_submission._tcp 600 IN SRV 1 1 587 mail.slainte.at.
_imap._tcp 600 IN SRV 1 1 143 mail.slainte.at.
_pop3._tcp 600 IN SRV 1 1 110 mail.slainte.at.
_imaps._tcp 600 IN SRV 1 1 993 mail.slainte.at.
_pop3s._tcp 600 IN SRV 1 1 995 mail.slainte.at.
autoconfig 600 IN CNAME mail.slainte.at
_autodiscover._tcp  600 IN SRV 10 1 443 mail.slainte.at
mail 600 IN A 89.58.16.23
imap 600 IN A 89.58.16.23
smtp 600 IN A 89.58.16.23
mail 600 IN TXT v=spf1 mx:mail.slainte.at ip4:89.58.16.23 a:mail.slainte.at a:imap.slainte.at a:smtp.slainte.at a:slainte.at -all
_report._dmarc 600 IN TXT "v=DMARC1"
```

spf-Eintrag ist doppelt (für slainte.at und mail.slainte.at).

Da Mailu am Schirm den dkim-Schlüssel mit Leerzeichen usw. ausgibt, kann man den öffentlichen Schlüssel
mit folgendem Statement generieren.

```bash
$ echo $(openssl rsa -in ./slainte.at.dkim.key  -pubout -outform der 2>/dev/null | openssl base64) | sed -e 's| ||g'
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAz4zO6d5AEtHGKDc5M9SQ8DF0DnfV17sjtHhMv4oedmB4fSM0F8Bi8UhpuohnY25i2PeymA09S3mMxPhgcyIwxzxj6NzwdTL32E0zBiXIPYmJcbLBQVlmjDcah5AzGWsFXLyRcqxPjQrUwA87yk7EzPoXcYquMzrCc4nPBqmV/0dzdLNVbQ0UpsKX3gMQ3zm+i3WXUXeEWkfQBMv8w2IlbgL8j0QKJX1hNJZAe8TsRrR7lJqtifCzZOLGErEMaCfPlYePw+E/4gfzbWD6990m9UbeCeVXZTqjOs2cfvKZDHKuzWiQTXKMboo/CsgGIK8lrUWS34Zjdqvnr8i7Gq76YQIDAQAB
```

Wichtig: Der DKIM-Eintrag bei Netcup lautet

```bash
dkim._domainkey
```

ohne Domain (die hängt Netcup selbst dazu).
Überprüft werden kann z,b,  der DKIM-Eintrag mit

```bash
~$  nslookup -q=TXT dkim._domainkey.slainte.at
Server:		127.0.0.53
Address:	127.0.0.53#53

Non-authoritative answer:
dkim._domainkey.slainte.at	text = "v=DKIM1; k=rsa; p=MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAz4zO6d5AEtHGKDc5M9SQ8DF0DnfV17sjtHhMv4oedmB4fSM0F8Bi8UhpuohnY25i2PeymA09S3mMxPhgcyIwxzxj6NzwdTL32E0zBiXIPYmJcbLBQVlmjDcah5AzGWsFXLyRcqxPjQrUwA87yk7EzPoXcYquMzrCc4nPBqmV/0dzdLNVbQ0UpsKX3gMQ3zm+i" "3WXUXeEWkfQBMv8w2IlbgL8j0QKJX1hNJZAe8TsRrR7lJqtifCzZOLGErEMaCfPlYePw+E/4gfzbWD6990m9UbeCeVXZTqjOs2cfvKZDHKuzWiQTXKMboo/CsgGIK8lrUWS34Zjdqvnr8i7Gq76YQIDAQAB" ""

Authoritative answers can be found from:
slainte.at	nameserver = second-dns.netcup.net.
```
Achtung: Es kann bis zu 48 Stunden dauern, bis der DNS-Eintrag überall verfügbar ist.

Um die Email zu prüfen ist https://www.mail-tester.com praktisch.

![https_www_mail-tester.com.png](https_www_mail-tester.com.png)

Zum überprüfen der Korrektheit der Einträge sind die folgenden Links nützlich.

https://dmarcly.com/tools/spf-dkim-dmarc-wizard/#create-spf-record
https://mxtoolbox.com/diagnostic.aspx
https://geekflare.com/de/smtp-testing-tools/
https://www.immuniweb.com/ssl/slainte.at/BHtGaATk/
https://www.wormly.com/test-smtp-server/host/mail.slainte.at/port/25/sendmail/1/id/W1LzVDaq
https://www.getresponse.com/de/hilfe/was-ist-dkim-und-wie-richte-ich-es-ein.html
https://easydmarc.com/tools/dkim-record-generator
https://dkimcore.org/c/keycheck
https://www.appmaildev.com/
https://mecsa.jrc.ec.europa.eu/
https://mailtrap.io/blog/email-testing-checklist/

# RDNS-Check

https://phoenixnap.com/kb/reverse-dns-lookup

```bash
dig -x 89.58.16.23

; <<>> DiG 9.18.1-1ubuntu1.2-Ubuntu <<>> -x 89.58.16.23
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 20429
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 2, ADDITIONAL: 5

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 65494
;; QUESTION SECTION:
;23.16.58.89.in-addr.arpa.	IN	PTR

;; ANSWER SECTION:
23.16.58.89.in-addr.arpa. 86400	IN	PTR	slainte.at.

;; AUTHORITY SECTION:
16.58.89.in-addr.arpa.	86400	IN	NS	root-dns.netcup.net.
16.58.89.in-addr.arpa.	86400	IN	NS	second-dns.netcup.net.

;; ADDITIONAL SECTION:
second-dns.netcup.net.	6355	IN	A	37.221.199.199
root-dns.netcup.net.	1133	IN	A	46.38.225.225
second-dns.netcup.net.	128356	IN	AAAA	2a03:4000:2:24b::c7c7
root-dns.netcup.net.	4733	IN	AAAA	2a03:4000:0:1::e1e1

;; Query time: 83 msec
;; SERVER: 127.0.0.53#53(127.0.0.53) (UDP)
;; WHEN: Mon Jan 02 14:58:50 CET 2023
;; MSG SIZE  rcvd: 223
```

Ein RDNS muß vorhanden sein, und auf den Mailserver zeigen, sonst wird die Mail recht schnell als SPAM eingestuft.

# Ergebnisse:

https://mxtoolbox.com/diagnostic.aspx

```bash
Connecting to 89.58.16.23

220 slainte.at ESMTP ready [199 ms]
EHLO keeper-us-east-1d.mxtoolbox.com
250-slainte.at
250-PIPELINING
250-SIZE 52428800
250-ETRN
250-ENHANCEDSTATUSCODES
250-8BITMIME
250-DSN
250 STARTTLS [215 ms]
MAIL FROM:<supertool@mxtoolboxsmtpdiag.com>
250 2.0.0 OK [216 ms]
RCPT TO:<test@mxtoolboxsmtpdiag.com>
554 5.7.1 <test@mxtoolboxsmtpdiag.com>: Relay access denied [275 ms]

```
Report von https://www.appmaildev.com/ ist im Directory abgespeichert.

Guter Test ist auch https://de.ssl-tools.net/mailservers, wenngleich nicht alle Tests zu "sinnvollen" Ergebenissen führen.

Nett ist auch https://app.mailgenius.com, wenngleich nicht alle Tests zu "sinnvollen" Ergebenissen führen.

Interessanter Ansatz ist https://glockapps.com/ . Das ist als Telegram-Bot realisiert.
