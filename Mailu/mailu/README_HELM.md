# Testen, ob das Template funktioniert.

```bash
helm template . --name-template mailu --namespace mailu --kube-version 1.25 --values ./values.yaml --debug > x.yaml
install.go:178: [debug] Original chart version: ""
install.go:199: [debug] CHART PATH: /home/alfred/gitlab/slainte/Mailu/mailu
```

Dann sollte alles passen.
