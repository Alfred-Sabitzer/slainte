# Velero System Update

https://microk8s.io/docs/velero

# Installation Minio

Erst wird der Namespace und das Repo eingerichtet.

```bash
microk8s kubectl create namespace velero

```
Dann erfolgt die Installation

```bash
microk8s helm3 install -n velero --set buckets[0].name=velero,buckets[0].policy=none,buckets[0].purge=false minio minio/minio
namespace/velero created
"minio" has been added to your repositories
NAME: minio
LAST DEPLOYED: Wed Jan  4 21:07:23 2023
NAMESPACE: velero
STATUS: deployed
REVISION: 1
TEST SUITE: None
NOTES:
Minio can be accessed via port 9000 on the following DNS name from within your cluster:
minio.velero.svc.cluster.local

To access Minio from localhost, run the below commands:

1. export POD_NAME=$(kubectl get pods --namespace velero -l "release=minio" -o jsonpath="{.items[0].metadata.name}")

2. kubectl port-forward $POD_NAME 9000 --namespace velero

Read more about port forwarding here: http://kubernetes.io/docs/user-guide/kubectl/kubectl_port-forward/

You can now access Minio server on http://localhost:9000. Follow the below steps to connect to Minio server with mc client:

1. Download the Minio mc client - https://docs.minio.io/docs/minio-client-quickstart-guide

2. Get the ACCESS_KEY=$(kubectl get secret minio -o jsonpath="{.data.accesskey}" | base64 --decode) and the SECRET_KEY=$(kubectl get secret minio -o jsonpath="{.data.secretkey}" | base64 --decode)

3. mc alias set minio-local http://localhost:9000 "$ACCESS_KEY" "$SECRET_KEY" --api s3v4

4. mc ls minio-local

Alternately, you can use your browser or the Minio SDK to access the server - https://docs.minio.io/categories/17
```
Die erfolgreiche Installation kann auch gesehen werdern.

```bash
helm ls --all-namespaces
```
Die Zugangsdaten bekommt man mit folgenden Statements.

```bash
ACCESS_KEY=$(kubectl get secret minio -n velero -o jsonpath="{.data.accesskey}" | base64 --decode)
SECRET_KEY=$(kubectl get secret minio -n velero -o jsonpath="{.data.secretkey}" | base64 --decode)
```
Leider kann MinIO nicht leicht über einen Ingress publicziert werden. Aber lokal kann man darauf zugreifen.

```bash
MINIO_POD=$(k get pods -n velero -o jsonpath='{.items[0].metadata.name}')
kubectl port-forward $MINIO_POD -n velero 9000:9000
```
Dann kann man sich das mit dem lokalen Browser ansehen.
Die Daten befinden sich in einer "normalen" PVC

```bash
kubectl get -n velero pvc
NAME    STATUS   VOLUME                                     CAPACITY   ACCESS MODES   STORAGECLASS       AGE
minio   Bound    pvc-2d294b5b-a374-497c-ae2e-4a0d25dac003   500Gi      RWO            openebs-hostpath   19h
```

# Installation Velero

Auf einem Node wird lokal Velero installiert. Das ganze geht zu Hause auch auf dem eigenen Rechner (wenn man kubectl und damit Zugriff auf den Server hat).
Auf unserem Internet-Node muß das direkt am Node installiert werden.

```bash
root@bureau:~# sudo su -l
root@bureau:~# cd /usr/local/bin
root@bureau:/usr/local/bin# wget https://github.com/vmware-tanzu/velero/releases/download/v1.7.1/velero-v1.7.1-linux-amd64.tar.gz 

root@bureau:/usr/local/bin# tar -xzf velero-v1.7.1-linux-amd64.tar.gz
root@bureau:/usr/local/bin# chmod +x velero-v1.7.1-linux-amd64/velero
root@bureau:/usr/local/bin# chown root:root velero-v1.7.1-linux-amd64/velero
root@bureau:/usr/local/bin# cp velero-v1.7.1-linux-amd64/velero .
root@bureau:/usr/local/bin# rm velero-v1.7.1-linux-amd64.tar.gz 
root@bureau:/usr/local/bin#  ls -lisa
insgesamt 248520
14157789      4 drwxr-xr-x  3 root root      4096 Jän  5 17:35 .
13369354      4 drwxr-xr-x 10 root root      4096 Apr 19  2022 ..
14156476 130456 -r-xr-xr-x  1 root root 133582243 Nov 26 08:39 argocd
14222347  46408 -rwxr-xr-x  1 root root  47521792 Jän  2 16:10 starboard
14237296  71644 -rwxr-xr-x  1 root root  73362635 Jän  5 17:35 velero
14410476      4 drwxr-xr-x  3 root root      4096 Jän  5 17:21 velero-v1.7.1-linux-amd64
```

Wir speichern die Zugangsdaten in unser .password

```bash
ACCESS_KEY=$(kubectl -n velero get secret minio -o jsonpath="{.data.accesskey}" | base64 --decode)
SECRET_KEY=$(kubectl -n velero get secret minio -o jsonpath="{.data.secretkey}" | base64 --decode)
cat <<EOF > ${HOME}/.password/credentials-velero
[default]
aws_access_key_id=${ACCESS_KEY}
aws_secret_access_key=${SECRET_KEY}
EOF
```

Jetzt wird Velero selbst installiert

```bash
velero install \
--use-restic \
--provider aws \
--plugins velero/velero-plugin-for-aws:v1.3.0 \
--bucket velero \
--secret-file ${HOME}/.password/credentials-velero \
--backup-location-config region=minio,s3ForcePathStyle="true",s3Url=http://minio.velero.svc.cluster.local:9000 \
--snapshot-location-config region=minio

CustomResourceDefinition/backups.velero.io: attempting to create resource
CustomResourceDefinition/backups.velero.io: attempting to create resource client
CustomResourceDefinition/backups.velero.io: created
CustomResourceDefinition/backupstoragelocations.velero.io: attempting to create resource
CustomResourceDefinition/backupstoragelocations.velero.io: attempting to create resource client
CustomResourceDefinition/backupstoragelocations.velero.io: created
CustomResourceDefinition/deletebackuprequests.velero.io: attempting to create resource
CustomResourceDefinition/deletebackuprequests.velero.io: attempting to create resource client
CustomResourceDefinition/deletebackuprequests.velero.io: created
CustomResourceDefinition/downloadrequests.velero.io: attempting to create resource
CustomResourceDefinition/downloadrequests.velero.io: attempting to create resource client
CustomResourceDefinition/downloadrequests.velero.io: created
CustomResourceDefinition/podvolumebackups.velero.io: attempting to create resource
CustomResourceDefinition/podvolumebackups.velero.io: attempting to create resource client
CustomResourceDefinition/podvolumebackups.velero.io: created
CustomResourceDefinition/podvolumerestores.velero.io: attempting to create resource
CustomResourceDefinition/podvolumerestores.velero.io: attempting to create resource client
CustomResourceDefinition/podvolumerestores.velero.io: created
CustomResourceDefinition/resticrepositories.velero.io: attempting to create resource
CustomResourceDefinition/resticrepositories.velero.io: attempting to create resource client
CustomResourceDefinition/resticrepositories.velero.io: created
CustomResourceDefinition/restores.velero.io: attempting to create resource
CustomResourceDefinition/restores.velero.io: attempting to create resource client
CustomResourceDefinition/restores.velero.io: created
CustomResourceDefinition/schedules.velero.io: attempting to create resource
CustomResourceDefinition/schedules.velero.io: attempting to create resource client
CustomResourceDefinition/schedules.velero.io: created
CustomResourceDefinition/serverstatusrequests.velero.io: attempting to create resource
CustomResourceDefinition/serverstatusrequests.velero.io: attempting to create resource client
CustomResourceDefinition/serverstatusrequests.velero.io: created
CustomResourceDefinition/volumesnapshotlocations.velero.io: attempting to create resource
CustomResourceDefinition/volumesnapshotlocations.velero.io: attempting to create resource client
CustomResourceDefinition/volumesnapshotlocations.velero.io: created
Waiting for resources to be ready in cluster...
Namespace/velero: attempting to create resource
Namespace/velero: attempting to create resource client
Namespace/velero: already exists, proceeding
Namespace/velero: created
ClusterRoleBinding/velero: attempting to create resource
ClusterRoleBinding/velero: attempting to create resource client
ClusterRoleBinding/velero: created
ServiceAccount/velero: attempting to create resource
ServiceAccount/velero: attempting to create resource client
ServiceAccount/velero: created
Secret/cloud-credentials: attempting to create resource
Secret/cloud-credentials: attempting to create resource client
Secret/cloud-credentials: created
BackupStorageLocation/default: attempting to create resource
BackupStorageLocation/default: attempting to create resource client
BackupStorageLocation/default: created
VolumeSnapshotLocation/default: attempting to create resource
VolumeSnapshotLocation/default: attempting to create resource client
VolumeSnapshotLocation/default: created
Deployment/velero: attempting to create resource
Deployment/velero: attempting to create resource client
Deployment/velero: created
DaemonSet/restic: attempting to create resource
DaemonSet/restic: attempting to create resource client
DaemonSet/restic: created
Velero is installed! ⛵ Use 'kubectl logs deployment/velero -n velero' to view the status.

```

Nun muß noch der Hostpath für Restic angepasst werden (microk8s ist da ein wenig anders).

```bash
kubectl -n velero patch daemonset.apps/restic --type='json' -p='[{"op": "replace", "path": "/spec/template/spec/volumes/0/hostPath/path", "value":"/var/snap/microk8s/common/var/lib/kubelet/pods"}]'
```

Nun machen wir ein Backup (am besten für den Velero-Namespace)

```bash
alfred@bureau:~/gitlab/slainte/Velero$ velero backup create workloads-backup --include-namespaces=velero
Backup request "workloads-backup" submitted successfully.
Run `velero backup describe workloads-backup` or `velero backup logs workloads-backup` for more details.
alfred@bureau:~/gitlab/slainte/Velero$ velero backup describe workloads-backup
Name:         workloads-backup
Namespace:    velero
Labels:       velero.io/storage-location=default
Annotations:  velero.io/source-cluster-k8s-gitversion=v1.25.4
              velero.io/source-cluster-k8s-major-version=1
              velero.io/source-cluster-k8s-minor-version=25

Phase:  Completed

Errors:    0
Warnings:  0

Namespaces:
  Included:  velero
  Excluded:  <none>

Resources:
  Included:        *
  Excluded:        <none>
  Cluster-scoped:  auto

Label selector:  <none>

Storage Location:  default

Velero-Native Snapshot PVs:  auto

TTL:  720h0m0s

Hooks:  <none>

Backup Format Version:  1.1.0

Started:    2023-01-05 17:46:28 +0100 CET
Completed:  2023-01-05 17:46:32 +0100 CET

Expiration:  2023-02-04 17:46:28 +0100 CET

Total items to be backed up:  63
Items backed up:              63

Velero-Native Snapshots: <none included>
```

Das Ergebnis ist erwartungsgemäß in der Minio-PV im Bucket "velero"

```bash
root@k8s:/var/snap/microk8s/common/var/openebs/local/pvc-2d294b5b-a374-497c-ae2e-4a0d25dac003/velero# tree
.
├── backups
│   └── workloads-backup
│       ├── velero-backup.json
│       ├── workloads-backup-csi-volumesnapshotcontents.json.gz
│       ├── workloads-backup-csi-volumesnapshots.json.gz
│       ├── workloads-backup-logs.gz
│       ├── workloads-backup-podvolumebackups.json.gz
│       ├── workloads-backup-resource-list.json.gz
│       ├── workloads-backup.tar.gz
│       └── workloads-backup-volumesnapshots.json.gz
└── minio-dev.yaml
```

https://velero.io/docs/v1.4/resource-filtering/

Ein Backup des gesamten Clusters wird so gemacht:

```bash
alfred@bureau:~/Downloads$  velero backup create alfred-backup
Backup request "alfred-backup" submitted successfully.
Run `velero backup describe alfred-backup` or `velero backup logs alfred-backup` for more details.
alfred@bureau:~/Downloads$ velero backup describe alfred-backup
Name:         alfred-backup
Namespace:    velero
Labels:       velero.io/storage-location=default
Annotations:  velero.io/source-cluster-k8s-gitversion=v1.25.4
              velero.io/source-cluster-k8s-major-version=1
              velero.io/source-cluster-k8s-minor-version=25

Phase:  Completed

Errors:    0
Warnings:  0

Namespaces:
  Included:  *
  Excluded:  <none>

Resources:
  Included:        *
  Excluded:        <none>
  Cluster-scoped:  auto

Label selector:  <none>

Storage Location:  default

Velero-Native Snapshot PVs:  auto

TTL:  720h0m0s

Hooks:  <none>

Backup Format Version:  1.1.0

Started:    2023-01-05 17:58:16 +0100 CET
Completed:  2023-01-05 17:58:33 +0100 CET

Expiration:  2023-02-04 17:58:16 +0100 CET

Total items to be backed up:  1381
Items backed up:              1381

Velero-Native Snapshots: <none included>
alfred@bureau:~/Downloads$ 

```

Seltsamerweise liefert das logs einen Fehler, wenn es fertig ist.

```bash
alfred@bureau:~/Downloads$ velero backup logs alfred-backup
Logs for backup "alfred-backup" are not available until it's finished processing. Please wait until the backup has a phase of Completed or Failed and try again.

alfred@bureau:~/Downloads$ velero backup logs alfred-backup
An error occurred: Get "http://minio.velero.svc.cluster.local:9000/velero/backups/alfred-backup/alfred-backup-logs.gz?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=CetVTzyIcfib9ZIlsf9q%2F20230105%2Fminio%2Fs3%2Faws4_request&X-Amz-Date=20230105T165946Z&X-Amz-Expires=600&X-Amz-SignedHeaders=host&X-Amz-Signature=38f4afdd28cad19cb2b83e0fdf22d839cf6d852ee3e5c2d920db12271e76c0a5": dial tcp: lookup minio.velero.svc.cluster.local on 127.0.0.53:53: server misbehaving
```

Aber das Backup wird ordnungsgemäß durchgeführt.

Man kann auch noch die TTL angeben.

```bash
      --ttl duration                                    How long before the backup can be garbage collected. (default 720h0m0s)
```
Aber 30 Tage scheint mir ein guter Default zu sein.

# Velero Schedule

```bash
velero schedule create slainte --schedule "0 1 * * *"
```

Das erzeugt einen automatischen Schedule für ein Backup, das täglich um 01:00 in der Früh startet.

# Velero praktische Kommandos

```bash
Anzeige aller Backups

velero backup get
NAME                        STATUS      ERRORS   WARNINGS   CREATED                         EXPIRES   STORAGE LOCATION   SELECTOR
alfred-backup               Completed   0        0          2023-01-05 17:58:16 +0100 CET   29d       default            <none>
slainte-20230105171045      Completed   0        0          2023-01-05 18:10:45 +0100 CET   29d       default            <none>
this-is-my-cluster-backup   Completed   0        0          2023-01-05 17:55:37 +0100 CET   29d       default            <none>
workloads-backup            Completed   0        0          2023-01-05 17:46:28 +0100 CET   29d       default            <none>

Anzeige alle Schedule Options

velero schedule create --help
The --schedule flag is required, in cron notation, using UTC time:

| Character Position | Character Period | Acceptable Values |
| -------------------|:----------------:| -----------------:|
| 1                  | Minute           | 0-59,*            |
| 2                  | Hour             | 0-23,*            |
| 3                  | Day of Month     | 1-31,*            |
| 4                  | Month            | 1-12,*            |
| 5                  | Day of Week      | 0-6,*             |

The schedule can also be expressed using "@every <duration>" syntax. The duration
can be specified using a combination of seconds (s), minutes (m), and hours (h), for
example: "@every 2h30m".

Usage:
velero schedule create NAME --schedule [flags]

Examples:
# Create a backup every 6 hours.
velero create schedule NAME --schedule="0 */6 * * *"

# Create a backup every 6 hours with the @every notation.
velero create schedule NAME --schedule="@every 6h"

# Create a daily backup of the web namespace.
velero create schedule NAME --schedule="@every 24h" --include-namespaces web

# Create a weekly backup, each living for 90 days (2160 hours).
velero create schedule NAME --schedule="@every 168h" --ttl 2160h0m0s

Flags:
--default-volumes-to-restic optionalBool[=true]   Use restic by default to backup all pod volumes
--exclude-namespaces stringArray                  Namespaces to exclude from the backup.
--exclude-resources stringArray                   Resources to exclude from the backup, formatted as resource.group, such as storageclasses.storage.k8s.io.
-h, --help                                            help for create
--include-cluster-resources optionalBool[=true]   Include cluster-scoped resources in the backup
--include-namespaces stringArray                  Namespaces to include in the backup (use '*' for all namespaces). (default *)
--include-resources stringArray                   Resources to include in the backup, formatted as resource.group, such as storageclasses.storage.k8s.io (use '*' for all resources).
--label-columns stringArray                       A comma-separated list of labels to be displayed as columns
--labels mapStringString                          Labels to apply to the backup.
--ordered-resources string                        Mapping Kinds to an ordered list of specific resources of that Kind.  Resource names are separated by commas and their names are in format 'namespace/resourcename'. For cluster scope resource, simply use resource name. Key-value pairs in the mapping are separated by semi-colon.  Example: 'pods=ns1/pod1,ns1/pod2;persistentvolumeclaims=ns1/pvc4,ns1/pvc8'.  Optional.
-o, --output string                                   Output display format. For create commands, display the object but do not send it to the server. Valid formats are 'table', 'json', and 'yaml'. 'table' is not valid for the install command.
--schedule string                                 A cron expression specifying a recurring schedule for this backup to run
-l, --selector labelSelector                          Only back up resources matching this label selector. (default <none>)
--show-labels                                     Show labels in the last column
--snapshot-volumes optionalBool[=true]            Take snapshots of PersistentVolumes as part of the backup.
--storage-location string                         Location in which to store the backup.
--ttl duration                                    How long before the backup can be garbage collected. (default 720h0m0s)
--use-owner-references-in-backup                  Specifies whether to use OwnerReferences on backups created by this Schedule
--volume-snapshot-locations strings               List of locations (at most one per provider) where volume snapshots should be stored.

Global Flags:
--add_dir_header                   If true, adds the file directory to the header
--alsologtostderr                  log to standard error as well as files
--colorized optionalBool           Show colored output in TTY. Overrides 'colorized' value from $HOME/.config/velero/config.json if present. Enabled by default
--features stringArray             Comma-separated list of features to enable for this Velero process. Combines with values from $HOME/.config/velero/config.json if present
--kubeconfig string                Path to the kubeconfig file to use to talk to the Kubernetes apiserver. If unset, try the environment variable KUBECONFIG, as well as in-cluster configuration
--kubecontext string               The context to use to talk to the Kubernetes apiserver. If unset defaults to whatever your current-context is (kubectl config current-context)
--log_backtrace_at traceLocation   when logging hits line file:N, emit a stack trace (default :0)
--log_dir string                   If non-empty, write log files in this directory
--log_file string                  If non-empty, use this log file
--log_file_max_size uint           Defines the maximum size a log file can grow to. Unit is megabytes. If the value is 0, the maximum file size is unlimited. (default 1800)
--logtostderr                      log to standard error instead of files (default true)
-n, --namespace string                 The namespace in which Velero should operate (default "velero")
--skip_headers                     If true, avoid header prefixes in the log messages
--skip_log_headers                 If true, avoid headers when opening log files
--stderrthreshold severity         logs at or above this threshold go to stderr (default 2)
-v, --v Level                          number for the log level verbosity
--vmodule moduleSpec               comma-separated list of pattern=N settings for file-filtered logging
```

Auslösen eines Backups, das auf den Optionen des Schedules basiert:

```bash
velero backup create --from-schedule slainte
```

Löschen von Backups

```bash
# Delete all backups triggered by schedule "slainte".
velero backup delete --selector velero.io/schedule-name=slainte
```

Anzeige und Manipulation von Schedules

```bash
alfred@bureau:~/gitlab/slainte/Velero$ kubectl get -n velero schedule
NAME      AGE
slainte   20m
alfred@bureau:~/gitlab/slainte/Velero$ kubectl describe -n velero schedule
Name:         slainte
Namespace:    velero
Labels:       <none>
Annotations:  <none>
API Version:  velero.io/v1
Kind:         Schedule
Metadata:
Creation Timestamp:  2023-01-05T17:10:45Z
Generation:          3
Managed Fields:
API Version:  velero.io/v1
Fields Type:  FieldsV1
fieldsV1:
f:spec:
.:
f:schedule:
f:template:
.:
f:hooks:
f:includedNamespaces:
f:metadata:
f:ttl:
f:useOwnerReferencesInBackup:
f:status:
Manager:      velero
Operation:    Update
Time:         2023-01-05T17:10:45Z
API Version:  velero.io/v1
Fields Type:  FieldsV1
fieldsV1:
f:status:
f:lastBackup:
f:phase:
Manager:         velero-server
Operation:       Update
Time:            2023-01-05T17:10:45Z
Resource Version:  418709
UID:               73338173-70ba-4ce4-a7d6-e81dc57bf761
Spec:
Schedule:  0 1 * * *
Template:
Hooks:
Included Namespaces:
*
Metadata:
Ttl:                           720h0m0s
Use Owner References In Backup:  false
Status:
Last Backup:  2023-01-05T17:10:45Z
Phase:        Enabled
Events:         <none>
alfred@bureau:~/gitlab/slainte/Velero$ 
```
# Backup von Hostpath-Dateien

https://openebs.io/docs/main/user-guides/localpv-hostpath

## Beispiel: Registry.

Es muß eine Annotation eingefügt werden. Angebeben wird der Name des Volume-Mount

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: registryk8s-depl
  namespace: admin
  annotations:
    backup.velero.io/backup-volumes: repo-vol
```
Nun wird ein Backup am slainte.at gemacht.

```bash
velero backup create admin --include-namespaces admin
velero describe backups admin --details
```

Die richtigen Ordner **"admin"** aus backups und restic werden dann auf den k8s.slainte.at Entwicklungsrechner kopiert.

```bash
slainte@v22022112011208453:~$ sudo ls /var/snap/microk8s/common/var/openebs/local/pvc-1fcc*/velero -lisa
total 16
3281997 4 drwxr-sr-x 4 alfred alfred 4096 Jan  5 20:12 .
3281641 4 drwxrwsrwx 4 root   alfred 4096 Jan  5 20:08 ..
3548263 4 drwxr-sr-x 3 alfred alfred 4096 Jan  5 20:12 backups
3282057 4 drwxr-sr-x 3 alfred alfred 4096 Jan  5 20:10 restic
```

Nun kontrollieren wir ob alles da ist.

```bash
alfred@bureau:~$ velero get backups
NAME                     STATUS      ERRORS   WARNINGS   CREATED                         EXPIRES   STORAGE LOCATION   SELECTOR
admin                    Completed   0        0          2023-01-05 20:09:55 +0100 CET   29d       default            <none>
slainte-20230105175254   Completed   0        0          2023-01-05 18:52:54 +0100 CET   29d       default            <none>
wordpress                Completed   0        0          2023-01-05 19:23:59 +0100 CET   29d       default            <none>
alfred@bureau:~$ velero describe backups admin --details
Name:         admin
Namespace:    velero
Labels:       velero.io/storage-location=default
Annotations:  velero.io/source-cluster-k8s-gitversion=v1.25.4
velero.io/source-cluster-k8s-major-version=1
velero.io/source-cluster-k8s-minor-version=25

Phase:  Completed

Errors:    0
Warnings:  0

Namespaces:
Included:  admin
Excluded:  <none>

Resources:
Included:        *
Excluded:        <none>
Cluster-scoped:  auto

Label selector:  <none>

Storage Location:  default

Velero-Native Snapshot PVs:  auto

TTL:  720h0m0s

Hooks:  <none>

Backup Format Version:  1.1.0

Started:    2023-01-05 20:09:55 +0100 CET
Completed:  2023-01-05 20:12:09 +0100 CET

Expiration:  2023-02-04 20:09:55 +0100 CET

Total items to be backed up:  97
Items backed up:              97

Resource List:  <error getting backup resource list: Get "http://minio.velero.svc.cluster.local:9000/velero/backups/admin/admin-resource-list.json.gz?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=CetVTzyIcfib9ZIlsf9q%2F20230105%2Fminio%2Fs3%2Faws4_request&X-Amz-Date=20230105T194558Z&X-Amz-Expires=600&X-Amz-SignedHeaders=host&X-Amz-Signature=4421e01b5af8e6f4dd75491bc9a46d318b5fa70a357c2d3e6cb7a9c4c6e915ed": dial tcp: lookup minio.velero.svc.cluster.local on 127.0.0.53:53: server misbehaving>

Velero-Native Snapshots: <none included>

Restic Backups:
Completed:
admin/registryk8s-depl-568977f6d4-9hd7b: repo-vol
alfred@bureau:~$ 
```

Jetzt spielen wir das Backup zurück.

```bash
alfred@bureau:~$ velero restore create --from-backup admin
Restore request "admin-20230105205338" submitted successfully.


alfred@bureau:~$ velero restore describe admin-20230105205338 --details
Name:         admin-20230105205338
Namespace:    velero
Labels:       <none>
Annotations:  <none>

Phase:                       PartiallyFailed (run 'velero restore logs admin-20230105205338' for more information)
Total items to be restored:  96
Items restored:              96

Started:    2023-01-05 20:53:38 +0100 CET
Completed:  2023-01-05 20:56:24 +0100 CET
Warnings:   <error getting warnings: Get "http://minio.velero.svc.cluster.local:9000/velero/restores/admin-20230105205338/restore-admin-20230105205338-results.gz?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=CetVTzyIcfib9ZIlsf9q%2F20230105%2Fminio%2Fs3%2Faws4_request&X-Amz-Date=20230105T195841Z&X-Amz-Expires=600&X-Amz-SignedHeaders=host&X-Amz-Signature=8a453d428207814fe24317d3c9d082139dc9d1c94841736e489f272591f0beba": dial tcp: lookup minio.velero.svc.cluster.local on 127.0.0.53:53: server misbehaving>

Errors:  <error getting errors: Get "http://minio.velero.svc.cluster.local:9000/velero/restores/admin-20230105205338/restore-admin-20230105205338-results.gz?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=CetVTzyIcfib9ZIlsf9q%2F20230105%2Fminio%2Fs3%2Faws4_request&X-Amz-Date=20230105T195841Z&X-Amz-Expires=600&X-Amz-SignedHeaders=host&X-Amz-Signature=8a453d428207814fe24317d3c9d082139dc9d1c94841736e489f272591f0beba": dial tcp: lookup minio.velero.svc.cluster.local on 127.0.0.53:53: server misbehaving>

Backup:  admin

Namespaces:
  Included:  all namespaces found in the backup
  Excluded:  <none>

Resources:
  Included:        *
  Excluded:        nodes, events, events.events.k8s.io, backups.velero.io, restores.velero.io, resticrepositories.velero.io
  Cluster-scoped:  auto

Namespace mappings:  <none>

Label selector:  <none>

Restore PVs:  auto

Restic Restores:
  Completed:
    admin/registryk8s-depl-568977f6d4-9hd7b: repo-vol

Preserve Service NodePorts:  auto

kubectl get -n velero restore admin-20230105205338 -o yaml
apiVersion: velero.io/v1
kind: Restore
metadata:
  creationTimestamp: "2023-01-05T19:53:38Z"
  generation: 12
  name: admin-20230105205338
  namespace: velero
  resourceVersion: "456793"
  uid: 4b4d0a80-beab-4f21-a48f-1d830685c4d6
spec:
  backupName: admin
  excludedResources:
  - nodes
  - events
  - events.events.k8s.io
  - backups.velero.io
  - restores.velero.io
  - resticrepositories.velero.io
  hooks: {}
  includedNamespaces:
  - '*'
status:
  completionTimestamp: "2023-01-05T19:56:24Z"
  errors: 1
  phase: PartiallyFailed
  progress:
    itemsRestored: 96
    totalItems: 96
  startTimestamp: "2023-01-05T19:53:38Z"
  warnings: 10


kubectl logs -n velero restic-t5j95 
time="2023-01-05T18:00:49Z" level=info msg="Setting log-level to INFO"
time="2023-01-05T18:00:49Z" level=info msg="Starting Velero restic server v1.7.1 (4729274d07eae7e788233d5c995d7f45f40c9c61)" logSource="pkg/cmd/cli/restic/server.go:87"
2023-01-05T18:00:49.798Z	INFO	controller-runtime.metrics	metrics server is starting to listen	{"addr": ":8080"}
time="2023-01-05T18:00:49Z" level=info msg="Starting controllers" logSource="pkg/cmd/cli/restic/server.go:198"
time="2023-01-05T18:00:49Z" level=info msg="Starting metric server for restic at address [:8085]" logSource="pkg/cmd/cli/restic/server.go:189"
time="2023-01-05T18:00:49Z" level=info msg="Controllers starting..." logSource="pkg/cmd/cli/restic/server.go:249"
time="2023-01-05T18:00:49Z" level=info msg="Starting controller" controller=pod-volume-backup logSource="pkg/controller/generic_controller.go:76"
time="2023-01-05T18:00:49Z" level=info msg="Waiting for caches to sync" controller=pod-volume-backup logSource="pkg/controller/generic_controller.go:81"
time="2023-01-05T18:00:49Z" level=info msg="Starting controller" controller=pod-volume-restore logSource="pkg/controller/generic_controller.go:76"
time="2023-01-05T18:00:49Z" level=info msg="Waiting for caches to sync" controller=pod-volume-restore logSource="pkg/controller/generic_controller.go:81"
2023-01-05T18:00:49.904Z	INFO	starting metrics server	{"path": "/metrics"}
time="2023-01-05T18:00:50Z" level=info msg="Caches are synced" controller=pod-volume-backup logSource="pkg/controller/generic_controller.go:85"
time="2023-01-05T18:00:50Z" level=info msg="Caches are synced" controller=pod-volume-restore logSource="pkg/controller/generic_controller.go:85"
time="2023-01-05T19:54:00Z" level=info msg="Restore starting" controller=pod-volume-restore logSource="pkg/controller/pod_volume_restore_controller.go:280" name=admin-20230105205338-hqrwj namespace=velero restore=velero/admin-20230105205338
time="2023-01-05T19:56:23Z" level=info msg="Restore completed" controller=pod-volume-restore logSource="pkg/controller/pod_volume_restore_controller.go:321" name=admin-20230105205338-hqrwj namespace=velero restore=velero/admin-20230105205338



alfred@bureau:~$ kubectl logs -n velero velero-8844c97c8-jrknq | grep -i error
Defaulted container "velero" out of: velero, velero-velero-plugin-for-aws (init)
time="2023-01-05T19:56:24Z" level=error msg="Error uploading restore results to backup storage" controller=restore error="rpc error: code = Unknown desc = error putting object restores/admin-20230105205338/restore-admin-20230105205338-results.gz: InternalError: We encountered an internal error, please try again.: cause(mkdir /export/.minio.sys/buckets/velero/restores/admin-20230105205338: no space left on device)\n\tstatus code: 500, request id: 173781DFB90D92A9, host id: " error.file="/go/src/velero-plugin-for-aws/velero-plugin-for-aws/object_store.go:287" error.function="main.(*ObjectStore).PutObject" logSource="pkg/controller/restore_controller.go:516"
alfred@bureau:~$ 


```

Es gab einen "unbedeutenden" Fehler, der auf Berechtigungen zurückzuführen ist. Wildes kopieren als root ist immer gefährlich.

Die PVC wurde aber ordnungsgemäß erstellt. Die Services laufen.
Generell: Man sollte beim Restore auch Reihenfolgen beachten (erst die PVC, dann die Services).