```bash
kubectl get clustercompliancedetailreports.aquasecurity.github.io -o yaml
```

```yaml
apiVersion: v1
items:
  - apiVersion: aquasecurity.github.io/v1alpha1
    kind: ClusterComplianceDetailReport
    metadata:
      creationTimestamp: "2022-12-28T09:01:54Z"
      generation: 6
      name: nsa-details
      resourceVersion: "312168"
      uid: ab8b18cd-e78f-4573-b84c-9a17e89bc195
    report:
      controlCheck:
        - checkResults:
            - details:
                - msg: Container 'openebs-ndm' of DaemonSet 'openebs-ndm' should set 'securityContext.privileged'
                    to false
                  name: daemonset-openebs-ndm
                  namespace: openebs
                  status: FAIL
                - msg: Container 'cstor-csi-plugin' of DaemonSet 'openebs-cstor-csi-node'
                    should set 'securityContext.privileged' to false
                  name: daemonset-openebs-cstor-csi-node
                  namespace: openebs
                  status: FAIL
                - msg: Container 'jiva-csi-plugin' of DaemonSet 'openebs-jiva-csi-node' should
                    set 'securityContext.privileged' to false
                  name: daemonset-openebs-jiva-csi-node
                  namespace: openebs
                  status: FAIL
              id: KSV017
              objectType: DaemonSet
          description: Controls whether Pods can run privileged containers
          id: "1.2"
          name: Preventing privileged containers
          severity: HIGH
        - checkResults:
            - details:
                - msg: Container 'application-controller' of StatefulSet 'argo-cd-argocd-application-controller'
                    should set 'securityContext.allowPrivilegeEscalation' to false
                  name: statefulset-argo-cd-argocd-application-controller
                  namespace: argocd
                  status: FAIL
                - msg: Container 'csi-attacher' of StatefulSet 'openebs-jiva-csi-controller'
                    should set 'securityContext.allowPrivilegeEscalation' to false
                  name: statefulset-openebs-jiva-csi-controller
                  namespace: openebs
                  status: FAIL
                - msg: Container 'csi-attacher' of StatefulSet 'openebs-cstor-csi-controller'
                    should set 'securityContext.allowPrivilegeEscalation' to false
                  name: statefulset-openebs-cstor-csi-controller
                  namespace: openebs
                  status: FAIL
                - msg: Container 'tempo' of StatefulSet 'tempo' should set 'securityContext.allowPrivilegeEscalation'
                    to false
                  name: statefulset-tempo
                  namespace: observability
                  status: FAIL
                - msg: Container 'loki' of StatefulSet 'loki' should set 'securityContext.allowPrivilegeEscalation'
                    to false
                  name: statefulset-loki
                  namespace: observability
                  status: FAIL
              id: KSV001
              objectType: StatefulSet
            - details:
                - msg: Container 'nginx-ingress-microk8s' of DaemonSet 'nginx-ingress-microk8s-controller'
                    should set 'securityContext.allowPrivilegeEscalation' to false
                  name: daemonset-nginx-ingress-microk8s-controller
                  namespace: ingress
                  status: FAIL
                - msg: Container 'node-exporter' of DaemonSet 'kube-prom-stack-prometheus-node-exporter'
                    should set 'securityContext.allowPrivilegeEscalation' to false
                  name: daemonset-kube-prom-stack-prometheus-node-exporter
                  namespace: observability
                  status: FAIL
                - msg: Container 'openebs-ndm' of DaemonSet 'openebs-ndm' should set 'securityContext.allowPrivilegeEscalation'
                    to false
                  name: daemonset-openebs-ndm
                  namespace: openebs
                  status: FAIL
                - msg: Container 'csi-node-driver-registrar' of DaemonSet 'openebs-cstor-csi-node'
                    should set 'securityContext.allowPrivilegeEscalation' to false
                  name: daemonset-openebs-cstor-csi-node
                  namespace: openebs
                  status: FAIL
                - msg: Container 'csi-node-driver-registrar' of DaemonSet 'openebs-jiva-csi-node'
                    should set 'securityContext.allowPrivilegeEscalation' to false
                  name: daemonset-openebs-jiva-csi-node
                  namespace: openebs
                  status: FAIL
              id: KSV001
              objectType: DaemonSet
            - details:
                - msg: Container 'copyutil' of ReplicaSet 'argo-cd-argocd-repo-server-85f4c4f5bd'
                    should set 'securityContext.allowPrivilegeEscalation' to false
                  name: replicaset-argo-cd-argocd-repo-server-85f4c4f5bd
                  namespace: argocd
                  status: FAIL
                - msg: Container 'notifications-controller' of ReplicaSet 'argo-cd-argocd-notifications-controller-545c4cc97c'
                    should set 'securityContext.allowPrivilegeEscalation' to false
                  name: replicaset-argo-cd-argocd-notifications-controller-545c4cc97c
                  namespace: argocd
                  status: FAIL
                - msg: Container 'notifications-controller' of ReplicaSet 'argo-cd-argocd-notifications-controller-65dc9867cd'
                    should set 'securityContext.allowPrivilegeEscalation' to false
                  name: replicaset-argo-cd-argocd-notifications-controller-65dc9867cd
                  namespace: argocd
                  status: FAIL
                - msg: Container 'metrics-api' of ReplicaSet 'metrics-api-b65bb8879' should
                    set 'securityContext.allowPrivilegeEscalation' to false
                  name: replicaset-metrics-api-b65bb8879
                  namespace: linkerd-viz
                  status: FAIL
                - msg: Container 'tap' of ReplicaSet 'tap-784868b795' should set 'securityContext.allowPrivilegeEscalation'
                    to false
                  name: replicaset-tap-784868b795
                  namespace: linkerd-viz
                  status: FAIL
                - msg: Container 'applicationset-controller' of ReplicaSet 'argo-cd-argocd-applicationset-controller-6c65b5799'
                    should set 'securityContext.allowPrivilegeEscalation' to false
                  name: replicaset-argo-cd-argocd-applicationset-controller-6c65b5799
                  namespace: argocd
                  status: FAIL
                - msg: Container 'openebs-cstor-cspc-operator' of ReplicaSet 'openebs-cstor-cspc-operator-654996b8c'
                    should set 'securityContext.allowPrivilegeEscalation' to false
                  name: replicaset-openebs-cstor-cspc-operator-654996b8c
                  namespace: openebs
                  status: FAIL
                - msg: Container 'prometheus' of ReplicaSet 'prometheus-6489cb6c86' should
                    set 'securityContext.allowPrivilegeEscalation' to false
                  name: replicaset-prometheus-6489cb6c86
                  namespace: linkerd-viz
                  status: FAIL
                - msg: Container 'server' of ReplicaSet 'argo-cd-argocd-server-788f98966b'
                    should set 'securityContext.allowPrivilegeEscalation' to false
                  name: replicaset-argo-cd-argocd-server-788f98966b
                  namespace: argocd
                  status: FAIL
                - msg: Container 'tap-injector' of ReplicaSet 'tap-injector-64b6649d9f' should
                    set 'securityContext.allowPrivilegeEscalation' to false
                  name: replicaset-tap-injector-64b6649d9f
                  namespace: linkerd-viz
                  status: FAIL
              id: KSV001
              objectType: ReplicaSet
          description: Control check restrictions escalation to root privileges
          id: "1.7"
          name: Restricts escalation to root privileges
          severity: MEDIUM
        - checkResults:
            - details:
                - msg: Resource do not exist in cluster
                  status: FAIL
              objectType: ResourceQuota
          description: Control check the use of ResourceQuota policy to limit aggregate
            resource usage within namespace
          id: "4.0"
          name: Use ResourceQuota policies to limit resources
          severity: MEDIUM
        - checkResults:
            - details:
                - msg: DaemonSet 'kube-prom-stack-prometheus-node-exporter' should not set
                    'spec.template.spec.hostNetwork' to true
                  name: daemonset-kube-prom-stack-prometheus-node-exporter
                  namespace: observability
                  status: FAIL
                - msg: DaemonSet 'openebs-ndm' should not set 'spec.template.spec.hostNetwork'
                    to true
                  name: daemonset-openebs-ndm
                  namespace: openebs
                  status: FAIL
                - msg: DaemonSet 'openebs-cstor-csi-node' should not set 'spec.template.spec.hostNetwork'
                    to true
                  name: daemonset-openebs-cstor-csi-node
                  namespace: openebs
                  status: FAIL
                - msg: DaemonSet 'openebs-jiva-csi-node' should not set 'spec.template.spec.hostNetwork'
                    to true
                  name: daemonset-openebs-jiva-csi-node
                  namespace: openebs
                  status: FAIL
              id: KSV009
              objectType: DaemonSet
          description: Controls whether share host process namespaces
          id: "1.4"
          name: Share host process namespaces
          severity: HIGH
        - checkResults:
            - details:
                - msg: DaemonSet 'kube-prom-stack-prometheus-node-exporter' should not set
                    'spec.template.spec.hostPID' to true
                  name: daemonset-kube-prom-stack-prometheus-node-exporter
                  namespace: observability
                  status: FAIL
              id: KSV010
              objectType: DaemonSet
          description: Controls whether containers can use the host network
          id: "1.5"
          name: Use the host network
          severity: HIGH
        - checkResults:
            - details:
                - msg: Container 'application-controller' of StatefulSet 'argo-cd-argocd-application-controller'
                    should set 'securityContext.readOnlyRootFilesystem' to true
                  name: statefulset-argo-cd-argocd-application-controller
                  namespace: argocd
                  status: FAIL
                - msg: Container 'csi-attacher' of StatefulSet 'openebs-jiva-csi-controller'
                    should set 'securityContext.readOnlyRootFilesystem' to true
                  name: statefulset-openebs-jiva-csi-controller
                  namespace: openebs
                  status: FAIL
                - msg: Container 'csi-attacher' of StatefulSet 'openebs-cstor-csi-controller'
                    should set 'securityContext.readOnlyRootFilesystem' to true
                  name: statefulset-openebs-cstor-csi-controller
                  namespace: openebs
                  status: FAIL
                - msg: Container 'tempo' of StatefulSet 'tempo' should set 'securityContext.readOnlyRootFilesystem'
                    to true
                  name: statefulset-tempo
                  namespace: observability
                  status: FAIL
              id: KSV014
              objectType: StatefulSet
            - details:
                - msg: Container 'nginx-ingress-microk8s' of DaemonSet 'nginx-ingress-microk8s-controller'
                    should set 'securityContext.readOnlyRootFilesystem' to true
                  name: daemonset-nginx-ingress-microk8s-controller
                  namespace: ingress
                  status: FAIL
                - msg: Container 'node-exporter' of DaemonSet 'kube-prom-stack-prometheus-node-exporter'
                    should set 'securityContext.readOnlyRootFilesystem' to true
                  name: daemonset-kube-prom-stack-prometheus-node-exporter
                  namespace: observability
                  status: FAIL
                - msg: Container 'openebs-ndm' of DaemonSet 'openebs-ndm' should set 'securityContext.readOnlyRootFilesystem'
                    to true
                  name: daemonset-openebs-ndm
                  namespace: openebs
                  status: FAIL
                - msg: Container 'csi-node-driver-registrar' of DaemonSet 'openebs-cstor-csi-node'
                    should set 'securityContext.readOnlyRootFilesystem' to true
                  name: daemonset-openebs-cstor-csi-node
                  namespace: openebs
                  status: FAIL
                - msg: Container 'csi-node-driver-registrar' of DaemonSet 'openebs-jiva-csi-node'
                    should set 'securityContext.readOnlyRootFilesystem' to true
                  name: daemonset-openebs-jiva-csi-node
                  namespace: openebs
                  status: FAIL
              id: KSV014
              objectType: DaemonSet
            - details:
                - msg: Container 'copyutil' of ReplicaSet 'argo-cd-argocd-repo-server-85f4c4f5bd'
                    should set 'securityContext.readOnlyRootFilesystem' to true
                  name: replicaset-argo-cd-argocd-repo-server-85f4c4f5bd
                  namespace: argocd
                  status: FAIL
                - msg: Container 'notifications-controller' of ReplicaSet 'argo-cd-argocd-notifications-controller-545c4cc97c'
                    should set 'securityContext.readOnlyRootFilesystem' to true
                  name: replicaset-argo-cd-argocd-notifications-controller-545c4cc97c
                  namespace: argocd
                  status: FAIL
                - msg: Container 'notifications-controller' of ReplicaSet 'argo-cd-argocd-notifications-controller-65dc9867cd'
                    should set 'securityContext.readOnlyRootFilesystem' to true
                  name: replicaset-argo-cd-argocd-notifications-controller-65dc9867cd
                  namespace: argocd
                  status: FAIL
                - msg: Container 'cert-manager' of ReplicaSet 'cert-manager-655bf9748f' should
                    set 'securityContext.readOnlyRootFilesystem' to true
                  name: replicaset-cert-manager-655bf9748f
                  namespace: cert-manager
                  status: FAIL
                - msg: Container 'metrics-api' of ReplicaSet 'metrics-api-b65bb8879' should
                    set 'securityContext.readOnlyRootFilesystem' to true
                  name: replicaset-metrics-api-b65bb8879
                  namespace: linkerd-viz
                  status: FAIL
                - msg: Container 'tap' of ReplicaSet 'tap-784868b795' should set 'securityContext.readOnlyRootFilesystem'
                    to true
                  name: replicaset-tap-784868b795
                  namespace: linkerd-viz
                  status: FAIL
                - msg: Container 'applicationset-controller' of ReplicaSet 'argo-cd-argocd-applicationset-controller-6c65b5799'
                    should set 'securityContext.readOnlyRootFilesystem' to true
                  name: replicaset-argo-cd-argocd-applicationset-controller-6c65b5799
                  namespace: argocd
                  status: FAIL
                - msg: Container 'openebs-cstor-cspc-operator' of ReplicaSet 'openebs-cstor-cspc-operator-654996b8c'
                    should set 'securityContext.readOnlyRootFilesystem' to true
                  name: replicaset-openebs-cstor-cspc-operator-654996b8c
                  namespace: openebs
                  status: FAIL
                - msg: Container 'prometheus' of ReplicaSet 'prometheus-6489cb6c86' should
                    set 'securityContext.readOnlyRootFilesystem' to true
                  name: replicaset-prometheus-6489cb6c86
                  namespace: linkerd-viz
                  status: FAIL
                - msg: Container 'server' of ReplicaSet 'argo-cd-argocd-server-788f98966b'
                    should set 'securityContext.readOnlyRootFilesystem' to true
                  name: replicaset-argo-cd-argocd-server-788f98966b
                  namespace: argocd
                  status: FAIL
              id: KSV014
              objectType: ReplicaSet
            - details:
                - msg: Container 'heartbeat' of CronJob 'linkerd-heartbeat' should set 'securityContext.readOnlyRootFilesystem'
                    to true
                  name: cronjob-linkerd-heartbeat
                  namespace: linkerd
                  status: FAIL
                - msg: Container 'curlmail' of CronJob 'curlmail' should set 'securityContext.readOnlyRootFilesystem'
                    to true
                  name: cronjob-curlmail
                  namespace: secrets
                  status: FAIL
              id: KSV014
              objectType: CronJob
          description: Check that container root file system is immutable
          id: "1.1"
          name: Immutable container file systems
          severity: LOW
        - checkResults:
            - details:
                - msg: StatefulSet 'alertmanager-kube-prom-stack-kube-prome-alertmanager'
                    should set 'spec.securityContext.runAsGroup', 'spec.securityContext.supplementalGroups[*]'
                    and 'spec.securityContext.fsGroup' to integer greater than 0
                  name: statefulset-757589978
                  namespace: observability
                  status: FAIL
                - msg: StatefulSet 'prometheus-kube-prom-stack-kube-prome-prometheus' should
                    set 'spec.securityContext.runAsGroup', 'spec.securityContext.supplementalGroups[*]'
                    and 'spec.securityContext.fsGroup' to integer greater than 0
                  name: statefulset-prometheus-kube-prom-stack-kube-prome-prometheus
                  namespace: observability
                  status: FAIL
                - msg: StatefulSet 'loki' should set 'spec.securityContext.runAsGroup', 'spec.securityContext.supplementalGroups[*]'
                    and 'spec.securityContext.fsGroup' to integer greater than 0
                  name: statefulset-loki
                  namespace: observability
                  status: FAIL
              id: KSV029
              objectType: StatefulSet
            - details:
                - msg: DaemonSet 'kube-prom-stack-prometheus-node-exporter' should set 'spec.securityContext.runAsGroup',
                    'spec.securityContext.supplementalGroups[*]' and 'spec.securityContext.fsGroup'
                    to integer greater than 0
                  name: daemonset-kube-prom-stack-prometheus-node-exporter
                  namespace: observability
                  status: FAIL
                - msg: DaemonSet 'loki-promtail' should set 'spec.securityContext.runAsGroup',
                    'spec.securityContext.supplementalGroups[*]' and 'spec.securityContext.fsGroup'
                    to integer greater than 0
                  name: daemonset-loki-promtail
                  namespace: observability
                  status: FAIL
              id: KSV029
              objectType: DaemonSet
            - details:
                - msg: ReplicaSet 'kube-prom-stack-kube-prome-operator-7c8479fcb9' should
                    set 'spec.securityContext.runAsGroup', 'spec.securityContext.supplementalGroups[*]'
                    and 'spec.securityContext.fsGroup' to integer greater than 0
                  name: replicaset-kube-prom-stack-kube-prome-operator-7c8479fcb9
                  namespace: observability
                  status: FAIL
                - msg: ReplicaSet 'prometheus-6489cb6c86' should set 'spec.securityContext.runAsGroup',
                    'spec.securityContext.supplementalGroups[*]' and 'spec.securityContext.fsGroup'
                    to integer greater than 0
                  name: replicaset-prometheus-6489cb6c86
                  namespace: linkerd-viz
                  status: FAIL
                - msg: ReplicaSet 'kube-prom-stack-kube-state-metrics-85d569bb76' should set
                    'spec.securityContext.runAsGroup', 'spec.securityContext.supplementalGroups[*]'
                    and 'spec.securityContext.fsGroup' to integer greater than 0
                  name: replicaset-kube-prom-stack-kube-state-metrics-85d569bb76
                  namespace: observability
                  status: FAIL
                - msg: ReplicaSet 'kube-prom-stack-grafana-8664f56f69' should set 'spec.securityContext.runAsGroup',
                    'spec.securityContext.supplementalGroups[*]' and 'spec.securityContext.fsGroup'
                    to integer greater than 0
                  name: replicaset-kube-prom-stack-grafana-8664f56f69
                  namespace: observability
                  status: FAIL
              id: KSV029
              objectType: ReplicaSet
            - details:
                - msg: CronJob 'curlmail' should set 'spec.securityContext.runAsGroup', 'spec.securityContext.supplementalGroups[*]'
                    and 'spec.securityContext.fsGroup' to integer greater than 0
                  name: cronjob-curlmail
                  namespace: secrets
                  status: FAIL
              id: KSV029
              objectType: CronJob
          description: Controls whether container applications can run with root privileges
            or with root group membership
          id: "1.6"
          name: Run with root privileges or with root group membership
          severity: LOW
        - checkResults:
            - details:
                - msg: Container 'application-controller' of StatefulSet 'argo-cd-argocd-application-controller'
                    should set 'securityContext.runAsNonRoot' to true
                  name: statefulset-argo-cd-argocd-application-controller
                  namespace: argocd
                  status: FAIL
                - msg: Container 'csi-attacher' of StatefulSet 'openebs-jiva-csi-controller'
                    should set 'securityContext.runAsNonRoot' to true
                  name: statefulset-openebs-jiva-csi-controller
                  namespace: openebs
                  status: FAIL
                - msg: Container 'csi-attacher' of StatefulSet 'openebs-cstor-csi-controller'
                    should set 'securityContext.runAsNonRoot' to true
                  name: statefulset-openebs-cstor-csi-controller
                  namespace: openebs
                  status: FAIL
                - msg: Container 'tempo' of StatefulSet 'tempo' should set 'securityContext.runAsNonRoot'
                    to true
                  name: statefulset-tempo
                  namespace: observability
                  status: FAIL
              id: KSV012
              objectType: StatefulSet
            - details:
                - msg: Container 'nginx-ingress-microk8s' of DaemonSet 'nginx-ingress-microk8s-controller'
                    should set 'securityContext.runAsNonRoot' to true
                  name: daemonset-nginx-ingress-microk8s-controller
                  namespace: ingress
                  status: FAIL
                - msg: Container 'promtail' of DaemonSet 'loki-promtail' should set 'securityContext.runAsNonRoot'
                    to true
                  name: daemonset-loki-promtail
                  namespace: observability
                  status: FAIL
                - msg: Container 'openebs-ndm' of DaemonSet 'openebs-ndm' should set 'securityContext.runAsNonRoot'
                    to true
                  name: daemonset-openebs-ndm
                  namespace: openebs
                  status: FAIL
                - msg: Container 'csi-node-driver-registrar' of DaemonSet 'openebs-cstor-csi-node'
                    should set 'securityContext.runAsNonRoot' to true
                  name: daemonset-openebs-cstor-csi-node
                  namespace: openebs
                  status: FAIL
                - msg: Container 'csi-node-driver-registrar' of DaemonSet 'openebs-jiva-csi-node'
                    should set 'securityContext.runAsNonRoot' to true
                  name: daemonset-openebs-jiva-csi-node
                  namespace: openebs
                  status: FAIL
              id: KSV012
              objectType: DaemonSet
            - details:
                - msg: Container 'copyutil' of ReplicaSet 'argo-cd-argocd-repo-server-85f4c4f5bd'
                    should set 'securityContext.runAsNonRoot' to true
                  name: replicaset-argo-cd-argocd-repo-server-85f4c4f5bd
                  namespace: argocd
                  status: FAIL
                - msg: Container 'metrics-api' of ReplicaSet 'metrics-api-b65bb8879' should
                    set 'securityContext.runAsNonRoot' to true
                  name: replicaset-metrics-api-b65bb8879
                  namespace: linkerd-viz
                  status: FAIL
                - msg: Container 'tap' of ReplicaSet 'tap-784868b795' should set 'securityContext.runAsNonRoot'
                    to true
                  name: replicaset-tap-784868b795
                  namespace: linkerd-viz
                  status: FAIL
                - msg: Container 'applicationset-controller' of ReplicaSet 'argo-cd-argocd-applicationset-controller-6c65b5799'
                    should set 'securityContext.runAsNonRoot' to true
                  name: replicaset-argo-cd-argocd-applicationset-controller-6c65b5799
                  namespace: argocd
                  status: FAIL
                - msg: Container 'openebs-cstor-cspc-operator' of ReplicaSet 'openebs-cstor-cspc-operator-654996b8c'
                    should set 'securityContext.runAsNonRoot' to true
                  name: replicaset-openebs-cstor-cspc-operator-654996b8c
                  namespace: openebs
                  status: FAIL
                - msg: Container 'server' of ReplicaSet 'argo-cd-argocd-server-788f98966b'
                    should set 'securityContext.runAsNonRoot' to true
                  name: replicaset-argo-cd-argocd-server-788f98966b
                  namespace: argocd
                  status: FAIL
                - msg: Container 'tap-injector' of ReplicaSet 'tap-injector-64b6649d9f' should
                    set 'securityContext.runAsNonRoot' to true
                  name: replicaset-tap-injector-64b6649d9f
                  namespace: linkerd-viz
                  status: FAIL
                - msg: Container 'destination' of ReplicaSet 'linkerd-destination-766d99df76'
                    should set 'securityContext.runAsNonRoot' to true
                  name: replicaset-linkerd-destination-766d99df76
                  namespace: linkerd
                  status: FAIL
                - msg: Container 'tap-injector' of ReplicaSet 'tap-injector-8c885568c' should
                    set 'securityContext.runAsNonRoot' to true
                  name: replicaset-tap-injector-8c885568c
                  namespace: linkerd-viz
                  status: FAIL
                - msg: Container 'copyutil' of ReplicaSet 'argo-cd-argocd-dex-server-5595f9b6cf'
                    should set 'securityContext.runAsNonRoot' to true
                  name: replicaset-argo-cd-argocd-dex-server-5595f9b6cf
                  namespace: argocd
                  status: FAIL
              id: KSV012
              objectType: ReplicaSet
            - details:
                - msg: Container 'heartbeat' of CronJob 'linkerd-heartbeat' should set 'securityContext.runAsNonRoot'
                    to true
                  name: cronjob-linkerd-heartbeat
                  namespace: linkerd
                  status: FAIL
              id: KSV012
              objectType: CronJob
          description: Check that container is not running as root
          id: "1.0"
          name: Non-root containers
          severity: MEDIUM
        - checkResults:
            - details:
                - msg: Resource do not exist in cluster
                  status: FAIL
              objectType: NetworkPolicy
          description: Control check validate the pod and/or namespace Selectors usage
          id: "2.0"
          name: Pod and/or namespace Selectors usage
          severity: MEDIUM
        - checkResults:
            - details:
                - msg: Resource do not exist in cluster
                  status: FAIL
              objectType: LimitRange
          description: Control check the use of LimitRange policy limit resource usage
            for namespaces or nodes
          id: "4.1"
          name: Use LimitRange policies to limit resources
          severity: MEDIUM
      summary:
        failCount: 137
        passCount: 506
      type:
        description: national security agency - kubernetes hardening guidance
        name: nsa-details
        version: "1.0"
      updateTimestamp: "2023-01-02T15:42:57Z"
kind: List
metadata:
  resourceVersion: ""
```
