```bash
kubectl get configauditreports.aquasecurity.github.io -n secrets -o yaml
```

```yaml
apiVersion: v1
items:
  - apiVersion: aquasecurity.github.io/v1alpha1
    kind: ConfigAuditReport
    metadata:
      creationTimestamp: "2023-01-03T11:00:21Z"
      generation: 1
      labels:
        plugin-config-hash: 669cfcf6ff
        resource-spec-hash: f5b594fdd
        starboard.resource.kind: CronJob
        starboard.resource.name: curlmail
        starboard.resource.namespace: secrets
      name: cronjob-curlmail
      namespace: secrets
      ownerReferences:
        - apiVersion: batch/v1
          blockOwnerDeletion: false
          controller: true
          kind: CronJob
          name: curlmail
          uid: a8dd3481-f8c3-414f-9708-466611849d63
      resourceVersion: "338929"
      uid: aa7422be-2f20-4a63-9363-b867f9792a01
    report:
      checks:
        - category: Kubernetes Security Check
          checkID: KSV028
          description: >-
            In addition to restricting HostPath volumes, usage of non-ephemeral
            volume types should be limited to those defined through
            PersistentVolumes.
          severity: LOW
          success: true
          title: Non-ephemeral volume types used
        - category: Kubernetes Security Check
          checkID: KSV004
          description: >-
            Security best practices require containers to run with minimal required
            capabilities.
          severity: LOW
          success: true
          title: Unused capabilities should be dropped (drop any)
        - category: Kubernetes Security Check
          checkID: KSV005
          description: >-
            SYS_ADMIN gives the processes running inside the container privileges
            that are equivalent to root.
          severity: HIGH
          success: true
          title: SYS_ADMIN capability added
        - category: Kubernetes Security Check
          checkID: KSV006
          description: >-
            Mounting docker.sock from the host can give the container full root
            access to the host.
          severity: HIGH
          success: true
          title: hostPath volume mounted with docker.sock
        - category: Kubernetes Security Check
          checkID: KSV018
          description: Enforcing memory limits prevents DoS via resource exhaustion.
          severity: LOW
          success: true
          title: Memory not limited
        - category: Kubernetes Security Check
          checkID: KSV010
          description: >-
            Sharing the host’s PID namespace allows visibility on host processes,
            potentially leaking information such as environment variables and
            configuration.
          severity: HIGH
          success: true
          title: Access to host PID
        - category: Kubernetes Security Check
          checkID: KSV026
          description: >-
            Sysctls can disable security mechanisms or affect all containers on a
            host, and should be disallowed except for an allowed 'safe' subset. A
            sysctl is considered safe if it is namespaced in the container or the
            Pod, and it is isolated from other Pods or processes on the same Node.
          severity: MEDIUM
          success: true
          title: Unsafe sysctl options set
        - category: Kubernetes Security Check
          checkID: KSV022
          description: >-
            Adding NET_RAW or capabilities beyond the default set must be
            disallowed.
          severity: MEDIUM
          success: true
          title: Non-default capabilities added
        - category: Kubernetes Security Check
          checkID: KSV002
          description: A program inside the container can bypass AppArmor protection policies.
          severity: MEDIUM
          success: true
          title: Default AppArmor profile not set
        - category: Kubernetes Security Check
          checkID: KSV037
          description: ensure that User pods are not placed in kube-system namespace
          severity: MEDIUM
          success: true
          title: User Pods should not be placed in kube-system namespace
        - category: Kubernetes Security Check
          checkID: KSV036
          description: >-
            ensure that Pod specifications disable the secret token being mounted by
            setting automountServiceAccountToken: false
          severity: MEDIUM
          success: true
          title: Protecting Pod service account tokens
        - category: Kubernetes Security Check
          checkID: KSV029
          description: >-
            Containers should be forbidden from running with a root primary or
            supplementary GID.
          messages:
            - >-
              CronJob 'curlmail' should set 'spec.securityContext.runAsGroup',
              'spec.securityContext.supplementalGroups[*]' and
              'spec.securityContext.fsGroup' to integer greater than 0
          severity: LOW
          success: false
          title: A root primary or supplementary GID set
        - category: Kubernetes Security Check
          checkID: KSV001
          description: >-
            A program inside the container can elevate its own privileges and run as
            root, which might give the program control over the container and node.
          severity: MEDIUM
          success: true
          title: Process can elevate its own privileges
        - category: Kubernetes Security Check
          checkID: KSV015
          description: >-
            When containers have resource requests specified, the scheduler can make
            better decisions about which nodes to place pods on, and how to deal
            with resource contention.
          severity: LOW
          success: true
          title: CPU requests not specified
        - category: Kubernetes Security Check
          checkID: KSV007
          description: >-
            Managing /etc/hosts aliases can prevent the container engine from
            modifying the file after a pod’s containers have already been started.
          severity: LOW
          success: true
          title: hostAliases is set
        - category: Kubernetes Security Check
          checkID: KSV012
          description: >-
            'runAsNonRoot' forces the running image to run as a non-root user to
            ensure least privileges.
          severity: MEDIUM
          success: true
          title: Runs as root user
        - category: Kubernetes Security Check
          checkID: KSV008
          description: >-
            Sharing the host’s IPC namespace allows container processes to
            communicate with processes on the host.
          severity: HIGH
          success: true
          title: Access to host IPC namespace
        - category: Kubernetes Security Check
          checkID: KSV033
          description: Containers should only use images from trusted GCR registries.
          messages:
            - >-
              container curlmail of cronjob curlmail in secrets namespace should
              restrict container image to your specific registry domain. See the
              full GCR list here:
              https://cloud.google.com/container-registry/docs/overview#registries
          severity: MEDIUM
          success: false
          title: All container images must start with a GCR domain
        - category: Kubernetes Security Check
          checkID: KSV025
          description: Setting a custom SELinux user or role option should be forbidden.
          severity: MEDIUM
          success: true
          title: SELinux custom options set
        - category: Kubernetes Security Check
          checkID: KSV011
          description: Enforcing CPU limits prevents DoS via resource exhaustion.
          severity: LOW
          success: true
          title: CPU not limited
        - category: Kubernetes Security Check
          checkID: KSV102
          description: Check if Helm Tiller component is deployed.
          severity: CRITICAL
          success: true
          title: Tiller Is Deployed
        - category: Kubernetes Security Check
          checkID: KSV009
          description: >-
            Sharing the host’s network namespace permits processes in the pod to
            communicate with processes bound to the host’s loopback adapter.
          severity: HIGH
          success: true
          title: Access to host network
        - category: Kubernetes Security Check
          checkID: KSV027
          description: >-
            The default /proc masks are set up to reduce attack surface, and should
            be required.
          severity: MEDIUM
          success: true
          title: Non-default /proc masks set
        - category: Kubernetes Security Check
          checkID: KSV030
          description: >-
            The RuntimeDefault seccomp profile must be required, or allow specific
            additional profiles.
          severity: LOW
          success: true
          title: Default Seccomp profile not set
        - category: Kubernetes Security Check
          checkID: KSV023
          description: HostPath volumes must be forbidden.
          severity: MEDIUM
          success: true
          title: hostPath volumes mounted
        - category: Kubernetes Security Check
          checkID: KSV020
          description: >-
            Force the container to run with user ID > 10000 to avoid conflicts with
            the host’s user table.
          severity: MEDIUM
          success: true
          title: Runs with low user ID
        - category: Kubernetes Security Check
          checkID: KSV013
          description: >-
            It is best to avoid using the ':latest' image tag when deploying
            containers in production. Doing so makes it hard to track which version
            of the image is running, and hard to roll back the version.
          severity: LOW
          success: true
          title: 'Image tag '':latest'' used'
        - category: Kubernetes Security Check
          checkID: KSV016
          description: >-
            When containers have memory requests specified, the scheduler can make
            better decisions about which nodes to place pods on, and how to deal
            with resource contention.
          severity: LOW
          success: true
          title: Memory requests not specified
        - category: Kubernetes Security Check
          checkID: KSV032
          description: Containers should only use images from trusted registries.
          messages:
            - >-
              container curlmail of cronjob curlmail in secrets namespace should
              restrict container image to your specific registry domain. For Azure
              any domain ending in 'azurecr.io'
          severity: MEDIUM
          success: false
          title: All container images must start with the *.azurecr.io domain
        - category: Kubernetes Security Check
          checkID: KSV014
          description: >-
            An immutable root file system prevents applications from writing to
            their local disk. This can limit intrusions, as attackers will not be
            able to tamper with the file system or write foreign executables to
            disk.
          severity: LOW
          success: true
          title: Root file system is not read-only
        - category: Kubernetes Security Check
          checkID: KSV017
          description: >-
            Privileged containers share namespaces with the host system and do not
            offer any security. They should be used exclusively for system
            containers that require high privileges.
          severity: HIGH
          success: true
          title: Privileged container
        - category: Kubernetes Security Check
          checkID: KSV021
          description: >-
            Force the container to run with group ID > 10000 to avoid conflicts with
            the host’s user table.
          severity: MEDIUM
          success: true
          title: Runs with low group ID
        - category: Kubernetes Security Check
          checkID: KSV024
          description: >-
            HostPorts should be disallowed, or at minimum restricted to a known
            list.
          severity: HIGH
          success: true
          title: Access to host ports
        - category: Kubernetes Security Check
          checkID: KSV003
          description: >-
            The container should drop all default capabilities and add only those
            that are needed for its execution.
          severity: LOW
          success: true
          title: Default capabilities not dropped
      containerChecks: {}
      podChecks:
        - category: Kubernetes Security Check
          checkID: KSV028
          description: >-
            In addition to restricting HostPath volumes, usage of non-ephemeral
            volume types should be limited to those defined through
            PersistentVolumes.
          severity: LOW
          success: true
          title: Non-ephemeral volume types used
        - category: Kubernetes Security Check
          checkID: KSV004
          description: >-
            Security best practices require containers to run with minimal required
            capabilities.
          severity: LOW
          success: true
          title: Unused capabilities should be dropped (drop any)
        - category: Kubernetes Security Check
          checkID: KSV005
          description: >-
            SYS_ADMIN gives the processes running inside the container privileges
            that are equivalent to root.
          severity: HIGH
          success: true
          title: SYS_ADMIN capability added
        - category: Kubernetes Security Check
          checkID: KSV006
          description: >-
            Mounting docker.sock from the host can give the container full root
            access to the host.
          severity: HIGH
          success: true
          title: hostPath volume mounted with docker.sock
        - category: Kubernetes Security Check
          checkID: KSV018
          description: Enforcing memory limits prevents DoS via resource exhaustion.
          severity: LOW
          success: true
          title: Memory not limited
        - category: Kubernetes Security Check
          checkID: KSV010
          description: >-
            Sharing the host’s PID namespace allows visibility on host processes,
            potentially leaking information such as environment variables and
            configuration.
          severity: HIGH
          success: true
          title: Access to host PID
        - category: Kubernetes Security Check
          checkID: KSV026
          description: >-
            Sysctls can disable security mechanisms or affect all containers on a
            host, and should be disallowed except for an allowed 'safe' subset. A
            sysctl is considered safe if it is namespaced in the container or the
            Pod, and it is isolated from other Pods or processes on the same Node.
          severity: MEDIUM
          success: true
          title: Unsafe sysctl options set
        - category: Kubernetes Security Check
          checkID: KSV022
          description: >-
            Adding NET_RAW or capabilities beyond the default set must be
            disallowed.
          severity: MEDIUM
          success: true
          title: Non-default capabilities added
        - category: Kubernetes Security Check
          checkID: KSV002
          description: A program inside the container can bypass AppArmor protection policies.
          severity: MEDIUM
          success: true
          title: Default AppArmor profile not set
        - category: Kubernetes Security Check
          checkID: KSV037
          description: ensure that User pods are not placed in kube-system namespace
          severity: MEDIUM
          success: true
          title: User Pods should not be placed in kube-system namespace
        - category: Kubernetes Security Check
          checkID: KSV036
          description: >-
            ensure that Pod specifications disable the secret token being mounted by
            setting automountServiceAccountToken: false
          severity: MEDIUM
          success: true
          title: Protecting Pod service account tokens
        - category: Kubernetes Security Check
          checkID: KSV029
          description: >-
            Containers should be forbidden from running with a root primary or
            supplementary GID.
          messages:
            - >-
              CronJob 'curlmail' should set 'spec.securityContext.runAsGroup',
              'spec.securityContext.supplementalGroups[*]' and
              'spec.securityContext.fsGroup' to integer greater than 0
          severity: LOW
          success: false
          title: A root primary or supplementary GID set
        - category: Kubernetes Security Check
          checkID: KSV001
          description: >-
            A program inside the container can elevate its own privileges and run as
            root, which might give the program control over the container and node.
          severity: MEDIUM
          success: true
          title: Process can elevate its own privileges
        - category: Kubernetes Security Check
          checkID: KSV015
          description: >-
            When containers have resource requests specified, the scheduler can make
            better decisions about which nodes to place pods on, and how to deal
            with resource contention.
          severity: LOW
          success: true
          title: CPU requests not specified
        - category: Kubernetes Security Check
          checkID: KSV007
          description: >-
            Managing /etc/hosts aliases can prevent the container engine from
            modifying the file after a pod’s containers have already been started.
          severity: LOW
          success: true
          title: hostAliases is set
        - category: Kubernetes Security Check
          checkID: KSV012
          description: >-
            'runAsNonRoot' forces the running image to run as a non-root user to
            ensure least privileges.
          severity: MEDIUM
          success: true
          title: Runs as root user
        - category: Kubernetes Security Check
          checkID: KSV008
          description: >-
            Sharing the host’s IPC namespace allows container processes to
            communicate with processes on the host.
          severity: HIGH
          success: true
          title: Access to host IPC namespace
        - category: Kubernetes Security Check
          checkID: KSV033
          description: Containers should only use images from trusted GCR registries.
          messages:
            - >-
              container curlmail of cronjob curlmail in secrets namespace should
              restrict container image to your specific registry domain. See the
              full GCR list here:
              https://cloud.google.com/container-registry/docs/overview#registries
          severity: MEDIUM
          success: false
          title: All container images must start with a GCR domain
        - category: Kubernetes Security Check
          checkID: KSV025
          description: Setting a custom SELinux user or role option should be forbidden.
          severity: MEDIUM
          success: true
          title: SELinux custom options set
        - category: Kubernetes Security Check
          checkID: KSV011
          description: Enforcing CPU limits prevents DoS via resource exhaustion.
          severity: LOW
          success: true
          title: CPU not limited
        - category: Kubernetes Security Check
          checkID: KSV102
          description: Check if Helm Tiller component is deployed.
          severity: CRITICAL
          success: true
          title: Tiller Is Deployed
        - category: Kubernetes Security Check
          checkID: KSV009
          description: >-
            Sharing the host’s network namespace permits processes in the pod to
            communicate with processes bound to the host’s loopback adapter.
          severity: HIGH
          success: true
          title: Access to host network
        - category: Kubernetes Security Check
          checkID: KSV027
          description: >-
            The default /proc masks are set up to reduce attack surface, and should
            be required.
          severity: MEDIUM
          success: true
          title: Non-default /proc masks set
        - category: Kubernetes Security Check
          checkID: KSV030
          description: >-
            The RuntimeDefault seccomp profile must be required, or allow specific
            additional profiles.
          severity: LOW
          success: true
          title: Default Seccomp profile not set
        - category: Kubernetes Security Check
          checkID: KSV023
          description: HostPath volumes must be forbidden.
          severity: MEDIUM
          success: true
          title: hostPath volumes mounted
        - category: Kubernetes Security Check
          checkID: KSV020
          description: >-
            Force the container to run with user ID > 10000 to avoid conflicts with
            the host’s user table.
          severity: MEDIUM
          success: true
          title: Runs with low user ID
        - category: Kubernetes Security Check
          checkID: KSV013
          description: >-
            It is best to avoid using the ':latest' image tag when deploying
            containers in production. Doing so makes it hard to track which version
            of the image is running, and hard to roll back the version.
          severity: LOW
          success: true
          title: 'Image tag '':latest'' used'
        - category: Kubernetes Security Check
          checkID: KSV016
          description: >-
            When containers have memory requests specified, the scheduler can make
            better decisions about which nodes to place pods on, and how to deal
            with resource contention.
          severity: LOW
          success: true
          title: Memory requests not specified
        - category: Kubernetes Security Check
          checkID: KSV032
          description: Containers should only use images from trusted registries.
          messages:
            - >-
              container curlmail of cronjob curlmail in secrets namespace should
              restrict container image to your specific registry domain. For Azure
              any domain ending in 'azurecr.io'
          severity: MEDIUM
          success: false
          title: All container images must start with the *.azurecr.io domain
        - category: Kubernetes Security Check
          checkID: KSV014
          description: >-
            An immutable root file system prevents applications from writing to
            their local disk. This can limit intrusions, as attackers will not be
            able to tamper with the file system or write foreign executables to
            disk.
          severity: LOW
          success: true
          title: Root file system is not read-only
        - category: Kubernetes Security Check
          checkID: KSV017
          description: >-
            Privileged containers share namespaces with the host system and do not
            offer any security. They should be used exclusively for system
            containers that require high privileges.
          severity: HIGH
          success: true
          title: Privileged container
        - category: Kubernetes Security Check
          checkID: KSV021
          description: >-
            Force the container to run with group ID > 10000 to avoid conflicts with
            the host’s user table.
          severity: MEDIUM
          success: true
          title: Runs with low group ID
        - category: Kubernetes Security Check
          checkID: KSV024
          description: >-
            HostPorts should be disallowed, or at minimum restricted to a known
            list.
          severity: HIGH
          success: true
          title: Access to host ports
        - category: Kubernetes Security Check
          checkID: KSV003
          description: >-
            The container should drop all default capabilities and add only those
            that are needed for its execution.
          severity: LOW
          success: true
          title: Default capabilities not dropped
      scanner:
        name: Starboard
        vendor: Aqua Security
        version: 0.15.0
      summary:
        criticalCount: 0
        highCount: 0
        lowCount: 1
        mediumCount: 2
      updateTimestamp: null
```