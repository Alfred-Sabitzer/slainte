# Curl - Lesen und Schreiben von Emails

Das ist ein einfacher Pod, der mit POP3 Mails lesen kann (auch verschlüsselt).
Dieser Pod kann auch Emails schreiben (auch verschlüsselt).

# Zweck

Das ist ein Show-Case. Der kann später für Email-Daemons verwendet werden.

# Design

Dieser Pod läuft als K8S-Cron-Job.
Die Realisierung erfolgt als Helm-Chart.
Namespace ist Secret.
Privater PGP-Schlüssel ist in Nextcloud.
Public-Keys registrierter Benutzer sind in einer ConfigMap.
Nextcloud-User ist K8S.
MailDaemon liest und schreibt als k8s@slainte.at.

# Security

Die Starboard-Anmerkungen werden soweit möglich berücksichtigt.
https://aquasecurity.github.io/starboard/

Starboard-CLI wird lokal downgeloaded und installiert.

```bash
starboard --help
Kubernetes-native security toolkit

Starboard CLI can be used to find risks, such as vulnerabilities or insecure
pod descriptors, in Kubernetes workloads. By default, the risk assessment
reports are stored as custom resources.

To get started execute the following one-time init command:

$ starboard init

As an example let's run in the current namespace an old version of nginx that
we know has vulnerabilities:

$ kubectl create deployment nginx --image nginx:1.16

Run the vulnerability scanner to generate vulnerability reports:

$ starboard scan vulnerabilityreports deployment/nginx

Once this has been done, you can retrieve the vulnerability report:

$ starboard get vulnerabilityreports deployment/nginx -o yaml

Usage:
starboard [command]

Available Commands:
completion  Generate the autocompletion script for the specified shell
config      View the configuration parameters used by Starboard scanners
get         Get security reports
help        Help about any command
install     Create Kubernetes resources used by Starboard
report      Generate an HTML security report for a specified Kubernetes object
scan        Manage security weakness identification tools
uninstall   Delete Kubernetes resources created by Starboard
version     Print the version information

Flags:
--as string                      Username to impersonate for the operation. User could be a regular user or a service account in a namespace.
--as-group stringArray           Group to impersonate for the operation, this flag can be repeated to specify multiple groups.
--as-uid string                  UID to impersonate for the operation.
--cache-dir string               Default cache directory (default "/home/alfred/.kube/cache")
--certificate-authority string   Path to a cert file for the certificate authority
--client-certificate string      Path to a client certificate file for TLS
--client-key string              Path to a client key file for TLS
--cluster string                 The name of the kubeconfig cluster to use
--context string                 The name of the kubeconfig context to use
-h, --help                           help for starboard
--insecure-skip-tls-verify       If true, the server's certificate will not be checked for validity. This will make your HTTPS connections insecure
--kubeconfig string              Path to the kubeconfig file to use for CLI requests.
-n, --namespace string               If present, the namespace scope for this CLI request
--request-timeout string         The length of time to wait before giving up on a single server request. Non-zero values should contain a corresponding time unit (e.g. 1s, 2m, 3h). A value of zero means don't timeout requests. (default "0")
-s, --server string                  The address and port of the Kubernetes API server
--tls-server-name string         Server name to use for server certificate validation. If it is not provided, the hostname used to contact the server is used
--token string                   Bearer token for authentication to the API server
--user string                    The name of the kubeconfig user to use
-v, --v Level                        number for the log level verbosity

Use "starboard [command] --help" for more information about a command.
```
Danach kann man die einzelnen Findings betrachten.

```bash
starboard get vulnerabilityreports cj/curlmail -n secrets -o yaml
starboard get configaudit cj/curlmail --namespace secrets -o yaml
starboard get clustercompliancereports nsa -o yaml --detail
```

Alternativ kann auch vieles mit kubectl gemacht werden.

```bash
kubectl get vulnerabilityreports.aquasecurity.github.io -n secrets -o yaml
kubectl get configauditreports.aquasecurity.github.io -n secrets -o yaml
kubectl get clustercompliancereports.aquasecurity.github.io -o yaml
kubectl get clustercompliancedetailreports.aquasecurity.github.io -o yaml
kubectl get ciskubebenchreports.aquasecurity.github.io -o yaml
kubectl get kubehunterreports.aquasecurity.github.io -o yaml
```

# Helm

Siehe auch https://phoenixnap.com/kb/create-helm-chart

```bash
helm create curlmail
```
Danach muß man die Templates und Values anpassen.
Überprüfen kann man das dann wie folgt:

```bash
helm template . --set namespace=secrets --name-template curlmail --values ./values.yaml --debug > x.yaml
install.go:178: [debug] Original chart version: ""
install.go:199: [debug] CHART PATH: /home/alfred/gitlab/slainte/CurlMail/curlmail
```

Installieren kann man das mit
```bash
helm install curlmail --set namespace=secrets . --values ./values.yaml
NAME: curlmail
LAST DEPLOYED: Sun Jan  1 17:29:45 2023
NAMESPACE: default
STATUS: deployed
REVISION: 1
TEST SUITE: None
NOTES:
1. Get the application URL by running these commands:
  export POD_NAME=$(kubectl get pods --namespace default -l "app.kubernetes.io/name=curlmail,app.kubernetes.io/instance=curlmail" -o jsonpath="{.items[0].metadata.name}")
  export CONTAINER_PORT=$(kubectl get pod --namespace default $POD_NAME -o jsonpath="{.spec.containers[0].ports[0].containerPort}")
  echo "Visit http://127.0.0.1:8080 to use your application"
  kubectl --namespace default port-forward $POD_NAME 8080:$CONTAINER_PORT
```

Deinstallieren kann man das mit
```bash
helm uninstall curlmail 
release "curlmail" uninstalled
```
Danach ist alles wieder sauber