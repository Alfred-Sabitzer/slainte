#
# Zerlegt eine Mail-Datei in ihre Bestandteile
#
# Wenn PGP-Mail, dann muss das PGP-File entschlüsselt werden.
# Danach wird eine künstliche Mail-Datei gemacht, die nochmals zerlegt wird.
#
# Ergebnisse sind immer im
#   body.html
#   body.txt            -> Hier sollte eigentlich die Nachricht drinnen sein.
#   header.txt          -> Hier findet man die Sender-Adresse und das Subject (wenn nicht verschlüsselt).
#
# Daneben "liegen" ein paar Attachments.
#

import os
import email
import sys
import getopt

from email.header import decode_header

#
# Aufsplitten der Mail
#
def split_mail(path,outpath,fle):
    os.system("rm -rf "+outpath+"/"+fle+"_attachments/")
    os.mkdir(outpath+"/"+fle+"_attachments/")
    BodyFile = open(outpath +"/"+fle+"_attachments/body.txt", 'w')
    HTMLFile = open(outpath +"/"+fle+"_attachments/body.html", 'w')
    HeaderFile = open(outpath +"/"+fle+"_attachments/header.txt", 'w')
    if str.lower(fle[-3:])=="eml":
        msg = email.message_from_file(open(path +"/"+ fle))
        attachments=msg.get_payload()
        # decode the email subject
        subject, encoding = decode_header(msg["Subject"])[0]
        if isinstance(subject, bytes):
            # if it's a bytes, decode to str
            subject = subject.decode(encoding)
        print("Subject:", subject, file = HeaderFile)
        # decode email sender
        From, encoding = decode_header(msg.get("From"))[0]
        if isinstance(From, bytes):
            From = From.decode(encoding)
        print("From:", From, file = HeaderFile)
        # decode email recipient
        To, encoding = decode_header(msg.get("To"))[0]
        if isinstance(To, bytes):
            To = To.decode(encoding)
        print("To:", To, file = HeaderFile)
        # if the email message is multipart
        if msg.is_multipart():
            # iterate over email parts
            for part in msg.walk():
                # extract content type of email
                content_type = part.get_content_type()
                content_disposition = str(part.get("Content-Disposition"))
                if content_type == "text/plain" and "attachment" not in content_disposition:
                    # print text/plain emails and skip attachments
                    body=""
                    try:
                        # get the email body
                        body = part.get_payload(decode=True).decode()
                    except:
                        pass
                    print(body, file = BodyFile)
                elif content_type == "text/html" and "attachment" not in content_disposition:
                    # print text/plain emails and skip attachments
                    body=""
                    try:
                        # get the email body
                        body = part.get_payload(decode=True).decode()
                    except:
                        pass
                    print(body, file = HTMLFile)
                elif content_type == "application/octet-stream" and "attachment" not in content_disposition:
                    # print text/plain emails and skip attachments
                    body=""
                    try:
                        # get the email body
                        body = part.get_payload(decode=True).decode()
                    except:
                        pass
                    # Kann nur einen geben!
                    PGPFile = open(outpath +"/"+fle+"_attachments/PGP.pgp", 'w')
                    print(body, file = PGPFile)
                    PGPFile.close()
                elif "attachment" in content_disposition:
                    # download attachment
                    filename = part.get_filename()
                    if filename:
                        folder_name = outpath +"/"+fle+"_attachments"
                        if not os.path.isdir(folder_name):
                            # make a folder for this email (named after the subject)
                            os.mkdir(folder_name)
                        filepath = os.path.join(folder_name, filename)
                        # download attachment and save it
                        open(filepath, "wb").write(part.get_payload(decode=True))
                elif content_type == "multipart/mixed" and content_disposition == "None": # Hier sind vielleicht die Protected headers drinnen (oder auch nicht).
                    result=part.as_string().find(' protected-headers="v1"')
                    if result > 0:
                        HeaderFile.close()
                        HeaderFile = open(outpath +"/"+fle+"_attachments/header.txt", 'w')
                        try:
                            subject, encoding = decode_header(part["Subject"])[0]
                            if isinstance(subject, bytes):
                                # if it's a bytes, decode to str
                                subject = subject.decode(encoding)
                            print("Subject:", subject, file = HeaderFile)
                            # decode email sender
                            From, encoding = decode_header(part.get("From"))[0]
                            if isinstance(From, bytes):
                                From = From.decode(encoding)
                            print("From:", From, file = HeaderFile)
                            # decode email recipient
                            To, encoding = decode_header(part.get("To"))[0]
                            if isinstance(To, bytes):
                                To = To.decode(encoding)
                            print("To:", To, file = HeaderFile)
                        except:
                            pass
        else:
            # extract content type of email
            content_type = msg.get_content_type()
            # get the email body
            body = msg.get_payload(decode=True).decode()
            if content_type == "text/plain":
                # print only text email parts
                print(body, file = BodyFile)
            if content_type == "text/html":
                # if it's HTML, create a new HTML file and open it in browser
                folder_name = outpath
                if not os.path.isdir(folder_name):
                    # make a folder for this email (named after the subject)
                    os.mkdir(folder_name)
                filename = "index.html"
                filepath = os.path.join(folder_name, filename)
                print(body, file = HTMLFile)
    BodyFile.close()
    HTMLFile.close()
    HeaderFile.close()

#
# Variablen und Directorys
#
def main(argv):
    opts, args = getopt.getopt(argv,"hi:o:",["idir=","odir=","pw=","secretkey=","publickey="])
    outpath=""
    path=""
    pgppw=""
    secretkey=""
    publickey=""
    for opt, arg in opts:
        if opt == '-h':
            print("Usage: --idir=<Verzeichnis mit EML-Dateien> --odir=<Verzeichnis der geparsten Ergebnisse> --pw=<Passwort für den PGP-Private-Key> --secretkey=<FQFN> --publickey=<FQFN>" )
            sys.exit()
        elif opt in ("-i", "--idir"):
            path = arg
        elif opt in ("-o", "--odir"):
            outpath = arg
        elif opt in ("-p", "--pw"):
            pgppw = arg
        elif opt in ("-sk", "--secretkey"):
            secretkey = arg
        elif opt in ("-pk", "--publickey"):
            publickey = arg
    # Defaults
    defaulthome=os.environ['HOME']+"/.gnupg"
    gpghome=os.environ.get('GNUPGHOME', defaulthome)
    # os.system("rm -rf "+outpath)
    # os.mkdir(outpath)

    #
    # Main, Loop über das Verzeichnis
    #
    listing = os.listdir(path)
    for fle in listing:
        os.system("rm -rf "+outpath+"/"+fle+"_attachments/")
        split_mail(path,outpath,fle)
        if pgppw != "#": # Ohne Passwort kein PGP:)
            isExist = os.path.exists(outpath +"/"+fle+"_attachments/PGP.pgp")
            if isExist:
                # https://zhou-en.github.io/2020/10/21/Encrypt-and-Decrypt-File-with-python-gnupg/
                os.system("echo -n '"+pgppw+"' | gpg --no-tty --batch --homedir "+gpghome+" --yes --passphrase-fd 0 --pinentry-mode loopback --trust-model always --output "+outpath +"/"+fle+"_attachments/PGP.txt --decrypt "+outpath +"/"+fle+"_attachments/PGP.pgp")
                os.system("cat "+outpath+"/"+fle+"_attachments/header.txt "+outpath +"/"+fle+"_attachments/PGP.txt > "+outpath +"/"+fle+".pgp.eml")
                split_mail(outpath,outpath,fle+".pgp.eml")
                os.system("rm -rf "+outpath+"/"+fle+"_attachments/") # Ursprüngliches Verzeichnis wird nicht mehr gebraucht.
                os.system("echo 'PGP-Encryption' > "+outpath+"/"+fle+".pgp.eml_attachments/body.pgp") # Ursprüngliches Verzeichnis wird nicht mehr gebraucht.
if __name__ == "__main__":
    if len(sys.argv) > 5:
        main(sys.argv[1:])
    else:
        print("Usage: --idir=<Verzeichnis mit EML-Dateien> --odir=<Verzeichnis der geparsten Ergebnisse> --pw=<Passwort für den PGP-Private-Key> --secretkey=<FQFN> --publickey=<FQFN>" )