#!/bin/bash
############################################################################################
#
# Löschen der verarbeiteten Mails vom Server
#
# https://everything.curl.dev/usingcurl/reademail
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
#shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition

if [[ -f /tmp/mails_to_do.txt ]];
then
  # Umgekehrte Reihenfolge (um richtig löschen zu können).
  cat  /tmp/mail.txt |
  while IFS=' ' read -r no size
  do
    if [[ ! -z "${no}" ]];
    then
      echo "Lösche Nachricht Nummer: ${no}"
      curl --ssl-reqd --request DELE --list-only --user "${MAIL_USER}:${MAIL_PASSWORD}" --url "pop3://${MAIL_HOST}"/${no}
      echo "Deleted: ${no}"
    fi
  done
fi