#!/usr/bin/env python3
#
# Bauen einer versendbaren Mail
#
# Es gibt die folgenden Dateien:
#   header.txt  - Empfänger und Absender und Subject
#
#   body.txt    - Einfache Nachricht
#   body.html   - Nachricht im HTML-Format
#   alles vorhandenen Attachments werden hinzugefügt.
#
#   body.pgp    - Indikator, das verschlüsselt werden muß
#
#
import getopt
import os

from email.message import Message
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email import utils, encoders
import mimetypes, sys

def alternative(data, contenttype, charset):
    maintype, subtype = contenttype.split('/')
    if maintype == 'text':
        retval = MIMEText(data, _subtype=subtype, _charset=charset)
    else:
        retval = MIMEBase(maintype, subtype)
        retval.set_payload(data)
        encoders.encode_base64(retval)
    return retval
def attachment(path,filename):
    mimetype, mimeencoding = mimetypes.guess_type(path+filename)
    if mimeencoding or (mimetype is None):
        mimetype = 'application/octet-stream'
    maintype, subtype = mimetype.split('/')
    if maintype == 'text':
        fd = open(path+filename, 'r')
        retval = MIMEText(fd.read(), _subtype=subtype)
    else:
        fd = open(path+filename, 'rb')
        retval = MIMEBase(maintype, subtype)
        retval.set_payload(fd.read())
        encoders.encode_base64(retval)

    retval.add_header('Content-Disposition', 'attachment',
                      filename = filename)
    fd.close()
    return retval

def compose_msg(msg,path):
    if os.stat(path+'body.html').st_size != 0:
        # Add the html version.  This converts the message into a multipart/alternative
        # container, with the original text message as the first part and the new html
        # message as the second part.
        msg2 = MIMEMultipart('alternative')
        text_file = open(path + 'body.txt', 'r')
        data = text_file.read()
        text_file.close()
        charset='none'
        if data.isascii():
            charset='us-ascii'
        else:
            charset='UTF-8'
        msg2.attach(alternative(data, 'text/plain', charset))
        text_file = open(path + 'body.html', 'r')
        data = text_file.read()
        text_file.close()
        charset='none'
        if data.isascii():
            charset='us-ascii'
        else:
            charset='UTF-8'
        msg2.attach(alternative(data, 'text/html', charset))
        msg.attach(msg2)
    else:
        if os.stat(path+'body.txt').st_size != 0:               # Entweder Body oder HTML
            text_file = open(path + 'body.txt', 'r')
            data = text_file.read()
            text_file.close()
            if data.isascii():
                body = MIMEText( _text=data, _subtype='plain', _charset='us-ascii')
            else:
                body = MIMEText(data, _subtype='plain', _charset='UTF-8')
            msg.attach(body)

    listing = os.listdir(path)
    for fle in listing:
        if fle != "header.txt":
            if fle != "body.txt":
                if fle != "body.html":
                    if fle != "body.pgp":
                        msg.attach(attachment(path,fle))


def main(argv):
    opts, args = getopt.getopt(argv,"hi:o:",["idir=","ofile=","pw="])
    outfile=""
    path=""
    pgppw=""
    for opt, arg in opts:
        if opt == '-h':
            print("Usage: --idir=<Verzeichnis mit Input-Dateien> --ofile=<FQFN für die Ausgabedatei> --pw=<Passwort des Secret-Keys>")
            sys.exit()
        elif opt in ("-i", "--idir"):
            path = arg
        elif opt in ("-o", "--ofile"):
            outfile = arg
        elif opt in ("-p", "--pw"):
            pgppw = arg

    # Create the base text message.
    sendermail="k8s@slainte.at"

    file1 = open(path + 'header.txt', 'r')
    for line in file1:
        xl=line.strip()
        result = xl.find('From:')
        if result == 0:
            tomail= xl[5:]
            toPureMail=tomail
            result = toPureMail.find('<')
            if result >=0:
                toPureMail= toPureMail[result+1:]
            result = toPureMail.find('>')
            if result >0:
                toPureMail= toPureMail[:result]
        result = xl.find('To:')
        if result == 0:
            result = xl.find('@')
            domainname=xl[result+1:]
            message_id=utils.make_msgid(domain=domainname)

            sendermail= xl[3:]
            result = sendermail.find('<')
            if result >=0:
                sendermail= sendermail[result+1:]
            result = sendermail.find('>')
            if result >0:
                sendermail= sendermail[:result]
        result = xl.find('Subject:')
        if result == 0:
            xsubject=xl[8:]
    file1.close()

    isPGP = os.path.exists(path +"body.pgp")


    if isPGP:
        pgpmsg = MIMEBase(_maintype="multipart", _subtype="mixed")
        pgpmsg.add_header(_name=" protected-headers", _value="v1")
        pgpmsg["From"]=sendermail
        pgpmsg["To"]=tomail
        pgpmsg["Message-ID"]=message_id
        pgpmsg["Subject"]=xsubject
        pgpmsg['Date'] = utils.formatdate(localtime = True)

        compose_msg(pgpmsg,path)

        with open('/tmp/mailbody.txt', 'wb') as f:
            f.write(bytes(pgpmsg))

        os.system("echo -n '"+pgppw+"' | gpg --no-tty --batch --yes --passphrase-fd 0 --pinentry-mode loopback --trust-model always -se --armor --recipient "+toPureMail+" -u "+sendermail+" -o /tmp/mailbody.pgp /tmp/mailbody.txt")

        pgp_msg_part1 = Message()
        pgp_msg_part1.add_header(_name="Content-Type", _value="application/pgp-encrypted")
        pgp_msg_part1.add_header(_name="Content-Description", _value="PGP/MIME version identification")
        pgp_msg_part1.set_payload("Version: 1" + "\n")

        text_file = open('/tmp/mailbody.pgp', 'r')
        data = text_file.read()
        text_file.close()
        os.system('rm -rf /tmp/mailbody.*')

        pgp_msg_part2 = Message()
        pgp_msg_part2.add_header(_name="Content-Type", _value="application/octet-stream", name="encrypted.asc")
        pgp_msg_part2.add_header(_name="Content-Description", _value="OpenPGP encrypted message")
        pgp_msg_part2.add_header(_name="Content-Disposition", _value="inline", filename="encrypted.asc")
        pgp_msg_part2.set_payload(data)

        msg = MIMEMultipart(_subtype="encrypted",protocol="application/pgp-encrypted", _subparts=[pgp_msg_part1, pgp_msg_part2])
#        msg['Subject'] = "..." # Das echte Subject muss schon im body.pgp drinnen sein.
#        msg["protected-headers"]="v1"
        msg['Subject'] = xsubject # Leider geht das nicht so
    else:
        msg = MIMEMultipart()
        msg['Subject'] = xsubject
        compose_msg(msg, path)

    tomail=tomail.strip()
    sendermail=sendermail.strip()
    msg['To'] = tomail
    msg['From'] = sendermail
    msg['Message-ID'] = message_id
    msg['Date'] = utils.formatdate(localtime = True)

    if pgppw != "#": # Ohne Passwort kein PGP:)
        os.system("muacrypt --basedir ${GNUPGHOME} add-account ")
        os.system("muacrypt --basedir ${GNUPGHOME} status ")
        os.system("muacrypt --basedir ${GNUPGHOME} make-header "+sendermail+" > /tmp/autocrypt.txt")
        text_file = open('/tmp/autocrypt.txt', 'r')
        autocr = text_file.read()
        text_file.close()
        msg['Autocrypt']=autocr[11:]

    # Make a local copy of what we are going to send.
    with open(outfile, 'wb') as f:
        f.write(bytes(msg))

    # CommandFile for sending
    with open(outfile+".sh", 'w') as f:
        f.writelines("#!/bin/bash\n")
        f.writelines("############################################################################################\n")
        f.writelines("#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.\n")
        f.writelines("shopt -o -s xtrace #—Displays each command before it’s executed.\n")
        f.writelines("shopt -o -s nounset #-No Variables without definition\n")
        f.writelines(" \n")
        f.writelines("curl  --ssl-reqd \\\n")
        f.writelines("      --url \"smtp://${MAIL_HOST}\" \\\n")
        f.writelines("      --user \"${MAIL_USER}:${MAIL_PASSWORD}\" \\\n")
        f.writelines("      --mail-from \""+sendermail+"\" \\\n")
        f.writelines("      --mail-rcpt \""+toPureMail+"\" \\\n")
        f.writelines("      --upload-file \"${1}\"\n")
        f.writelines(" \n")

if __name__ == "__main__":
    if len(sys.argv) > 2:
        main(sys.argv[1:])
    else:
        print("Usage: --idir=<Verzeichnis mit Input-Dateien> --ofile=<FQFN für die Ausgabedatei> --pw=<Passwort des Secret-Keys>")