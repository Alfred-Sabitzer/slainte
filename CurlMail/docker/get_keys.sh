#!/bin/bash
############################################################################################
#
# Holen der GPG-Schlüssel
#
############################################################################################
#shopt -o -s errexit # Terminates  the shell script  if a command returns an error code.
#shopt -o -s xtrace  # Displays each command before it’s executed.
shopt -o -s nounset  # No Variables without definition
#
user="${NEXTCLOUD_USER}"
pass="${NEXTCLOUD_TOKEN}"
nextcloud_url="${NEXTCLOUD_SERVER}"
secretkey="${MAIL_SECRET_KEY}"
publickey="${MAIL_PUBLIC_KEY}"
recipientkey="${MAIL_RECIPIENT_KEY}"
#
curl -X GET -u ${user}:${pass} ${nextcloud_url}/remote.php/dav/files/${user}/gpg/"${secretkey}"  --output /tmp/secretkey.asc
curl -X GET -u ${user}:${pass} ${nextcloud_url}/remote.php/dav/files/${user}/gpg/"${publickey}"  --output /tmp/publickey.asc
curl -X GET -u ${user}:${pass} ${nextcloud_url}/remote.php/dav/files/${user}/gpg/"${recipientkey}"  --output /tmp/recipientkey.asc
#
# Check PGP 0x888FE6A077A5A535 (0x1AFA9F13EA68D5A1)
# https://gist.github.com/ryantuck/56c5aaa8f9124422ac964629f4c8deb0
#
gpg -v --batch --homedir "${GNUPGHOME}" --allow-secret-key-import --passphrase "${MAIL_SECRET_KEY_PASSWORD}" --import /tmp/secretkey.asc
gpg -v --batch --homedir "${GNUPGHOME}" --import /tmp/publickey.asc
gpg -v --batch --homedir "${GNUPGHOME}" --import /tmp/recipientkey.asc
gpg -v --batch --homedir "${GNUPGHOME}" --list-keys
gpg -v --batch --homedir "${GNUPGHOME}" --list-public-keys
gpg -v --batch --homedir "${GNUPGHOME}" --list-secret-keys

#
# https://stackoverflow.com/questions/56582016/gpg-change-passphrase-non-interactively
# Using GnuPG to change PGP key passphrase non-interactively
cat <<EOF > /tmp/gpg_passwd.sh
gpg --trust-model always --no-tty --batch --yes --command-fd 0 --pinentry-mode loopback --edit-key ${MAIL_USER} <<END
passwd
${MAIL_SECRET_KEY_PASSWORD}


END
EOF
chmod 755 /tmp/gpg_passwd.sh
/tmp/gpg_passwd.sh

# https://buildmedia.readthedocs.org/media/pdf/muacrypt/latest/muacrypt.pdf
# Note: muacrypt does not support secret keys using passphrases. See also Autocrypt’s take on it: https://autocrypt.org/level1.html#secret-key-protection-at-rest
muacrypt --basedir ${GNUPGHOME} add-account --use-system-keyring --use-key ${MAIL_USER}
muacrypt --basedir ${GNUPGHOME}  status
#
# Jetzt ist auch interchange-Key von muacrypt dabei.
gpg -v --batch --homedir "${GNUPGHOME}" --list-secret-keys
#