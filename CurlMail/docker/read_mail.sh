#!/bin/bash
echo "############################################################################################"
echo "#"
echo "# Lesen der Email"
echo "#"
echo "# https://everything.curl.dev/usingcurl/reademail"
echo "#"
echo "############################################################################################"
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
#shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition

rm -Rf ${EML_IN}/*.eml
curl --ssl-reqd \
--url "pop3://${MAIL_HOST}" \
--user "${MAIL_USER}:${MAIL_PASSWORD}" > /tmp/mail.read

# Entfernen von Leerzeilen
sed -i '/^$/d' /tmp/mail.read
# Umgekehrte Reihenfolg (um später richtig löschen zu können).
sort -r /tmp/mail.read > /tmp/mail.txt
cat  /tmp/mail.txt |
while IFS=' ' read -r no size
do
  echo "Nummer:#${no}#${size}#"
  if [[ ! -z "${size}" ]];
  then
    curl --ssl-reqd \
    --url "pop3://${MAIL_HOST}"/${no} \
    --user "${MAIL_USER}:${MAIL_PASSWORD}" > ${EML_IN}/mail_${no}.eml
    touch /tmp/mails_to_do.txt
  fi
done
#
# Zerlegen der Email nach Boundaries
#
if [[ -f /tmp/mails_to_do.txt ]];
then
  echo "Nachrichten werden verarbeitet"
  python /ReadMail.py --idir=${EML_IN} --odir=${EML_PARSED}/ --pw="${MAIL_SECRET_KEY_PASSWORD}" --secretkey="${GNUPGHOME}/secretkey.asc" --publickey="${GNUPGHOME}/recipientkey.asc"
else
  echo "Keine Nachrichten zu verarbeiten"
fi