#!/bin/bash
# Disable exit on non 0
set +e
echo "############################################################################################"
echo "#"
echo "# Starten der Containers mit den richtigen Werten."
echo "#"
echo "############################################################################################"
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
#shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
#
# Setup der Verzeichnisse
mkdir -p ${GNUPGHOME}
chmod 700 ${GNUPGHOME}
mkdir ${EML_IN}
chmod 777 ${EML_IN}
mkdir ${EML_OUT}
chmod 777 ${EML_OUT}
mkdir ${EML_PARSED}
chmod 777 ${EML_PARSED}
#
/get_keys.sh
/read_mail.sh
/process_mail.sh
/send_mail.sh
/delete_mail.sh
#/dummy.sh
#
