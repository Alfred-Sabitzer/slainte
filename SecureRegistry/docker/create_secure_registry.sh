#!/bin/bash
############################################################################################
# Erzeugen einer Secure Registry mit Docker auf dem Build-Rechner
# Diese Registry dient zum Speichern von Basisimages, die der Cluster zum starten braucht
#
# https://docs.docker.com/registry/deploying/
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
git pull

docker_registry="monitoring.slainte.at:5000"

docker container stop registry
docker container rm $(docker container ls -a | grep -i registry:2 | awk '{print $1}')

docker run -d \
  --restart=always \
  --name registry \
  -v /mnt/rasp/registry:/var/lib/registry \
  -v /home/alfred/k8s/SecureRegistry/pem/server.crt:/etc/letsencrypt/live/monitoring.slainte.at/fullchain.pem \
  -v /home/alfred/k8s/SecureRegistry/pem/server.key:/etc/letsencrypt/live/monitoring.slainte.at/privkey.pem \
  -e REGISTRY_HTTP_ADDR=0.0.0.0:443 \
  -e REGISTRY_HTTP_TLS_CERTIFICATE=/etc/letsencrypt/live/monitoring.slainte.at/fullchain.pem \
  -e REGISTRY_HTTP_TLS_KEY=/etc/letsencrypt/live/monitoring.slainte.at/privkey.pem \
  -p 5000:443 \
  registry:2

sleep 5
curl -k -v https://${docker_registry}/v2/_catalog           # Check ob man was zurückbekommt
#
