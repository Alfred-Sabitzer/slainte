#!/bin/bash
############################################################################################
# Bauen und deployen
# Auch dieses Image kommt in die Basisregistry
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
#shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
docker_registry="registry.gitlab.com/alfred-sabitzer/slainte"
docker_username=$(cat ${HOME}/.password/docker_basis_username.txt)
docker_password=$(cat ${HOME}/.password/docker_basis_password.txt)
docker login ${docker_registry} -u ${docker_username} -p ${docker_password}
git pull --ff-only
image="registry-uik8s"
tag=$(date +"%Y%m%d")
docker build -t ${docker_registry}/${image}:${tag} -t ${docker_registry}/${image}:latest .
docker push ${docker_registry}/${image}:${tag}
docker push ${docker_registry}/${image}:latest
#
# Jetzt ist die UI in gitlab vorhanden
#
