# Docker Registry als Static-Beispiel

Diese Registry-UI ist von aussen sichtbar. In diesem Falle registry.gitlab.com

Die Konfiguration ist im `simple.yml` enthalten. Das Beispiel basiert auf https://github.com/Joxit/docker-registry-ui/tree/main/examples/ui-as-standalone 
und wurde für die lokalen Bedürfnisse angepasst.

Der Bau der Images und das befüllen der Registry erfolgt mit

```sh
./dc_secure_registry.sh
```

# Zweck dieser Registry

Diese Registry enthält alle Basis-Images, und dient dem Kubernetes-Cluster um
seine Services starten zu können. Das vermeidet unnötiges laden aus dem Internet. Ausserdem bleiben die Images stabil, und
können kontrolliert auf neuere Versionen upgedated werden. Ein direktes laden
aus dem Internet hat u.U. Nebeneffekte zur Folge.