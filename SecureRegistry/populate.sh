#!/bin/bash
############################################################################################
# Pushen der gebuildeten Sourcen in das Ziel-Registry
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
#shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition

docker_registry="monitoring.slainte.at:5000"
docker_username=$(cat ${HOME}/.password/docker_basis_username.txt)
docker_password=$(cat ${HOME}/.password/docker_basis_password.txt)
docker login ${docker_registry} -u ${docker_username} -p ${docker_password}
tag=${1}

docker push ${docker_registry}/registryk8s:${tag}
docker push ${docker_registry}/registryk8s:latest
curl -u ${docker_username}:${docker_password} -k -s https://${docker_registry}/v2/registryk8s/tags/list

docker push ${docker_registry}/registry-uik8s:${tag}
docker push ${docker_registry}/registry-uik8s:latest
curl -u ${docker_username}:${docker_password} -k -s https://${docker_registry}/v2/registry-uik8s/tags/list
