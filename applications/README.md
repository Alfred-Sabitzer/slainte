# Applications

Alle yamls hier werden von ArgoCD als Applikationen erkannt.

Muster:

```bash
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: redis
  namespace: argocd
spec:
  destination:
    server: https://kubernetes.default.svc
  project: slainte
  source:
    directory:
      jsonnet: {}
      recurse: false
    path: Redis
    repoURL: https://gitlab.com/Alfred-Sabitzer/slainte.git
    targetRevision: master
```

Das ist quasi der Erste Schritt fü CD - Continous Delivery.