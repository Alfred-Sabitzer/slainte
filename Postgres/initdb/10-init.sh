#!/bin/sh
set +x
echo "############################################################################################"
echo "#"
echo "# Kopieren der Config an den richtigen Platz. "
echo "#"
echo "############################################################################################"
#
whoami
for f in ../config/*.conf
do
        echo "Processing $f"
        cp -v $f ${PGDATA}/
done
for f in ${PGDATA}/*.conf
do
        echo "Processing $f"
        sed -i -r 's/@admins/'${POSTGRES_USER}'/g' $f
        #chown postgres:root $f
        chmod 700 $f
done
ls -lisa ${PGDATA}/*.conf
#
echo "############################################################################################"
echo "#"
echo "# Konfiguration done"
echo "#"
echo "############################################################################################"
#
