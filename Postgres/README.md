# Postgres-Datenbank

Das ist das Image für Postgres-Datenbanken. Im Dockerfile werde alle notwendigen Einstellungen und Parameter defniert.
Dieses Postgres verwendet TLS und eine einigermassen strikte Netzwerkpolicy. 

# Installation

```bash
./do.sh
```
Damit wird das Image mit allen notwendigen Einstellung erzeugt.

# Konfiguration

Im Verzeichnis config befinden sich die Konfigurationseinstellungen.

* pg_hba.conf (Netzwerkonfiguration)
* postgresql.conf (Datenbankkonfiguratio)

Im Verzeichnis init befinden sich die Initial-Skripten für die Anlage der Datenbank

* 10-init.sh (Shell-Skript um die Konfiguration an den richtigen Platz zu kopiern)
* 20-init.sql (Skript um in der Datenbank allgemeine Funktionen bereitzustellen)
* 20-superuser.sh (Anlage des lt. keepassxc konfiguierten Superusers)

Diese Skripten werden nur einmal (beim Anlegen der Datenbank) ausgeführt.
Diese Dateien werden auch mit create_config.sh zu configmaps umgewandelt.

```bash
cd /init/initdb/
ls
sh ./10-init.sh
sh ./20-superuser.sh 
```

Nach dem ersten Start muß man sich in den Container einloggen, und diese Skripten ausführen.
Danach muß die Instanz restartet werden.

