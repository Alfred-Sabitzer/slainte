#!/bin/sh
#############################################################################################"
#"
# Erzeugen der configmaps"
#"
#############################################################################################"
mpwd=$(pwd)
../tools/make_configmap_dir.sh ${mpwd}/config ${mpwd}/PostgresDB_config.yaml
../tools/make_configmap_dir.sh ${mpwd}/initdb ${mpwd}/PostgresDB_init.yaml
for f in *.yaml
do
  sed -i -r 's/#namespace#/slainte/g' ${f}
done
