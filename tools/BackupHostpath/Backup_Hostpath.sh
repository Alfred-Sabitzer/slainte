#!/bin/bash
############################################################################################
#
# Sichern aller PV's aus einem bestimmten Namespace
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
#shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
#backuproot="${1}"
old_days="5"
backuproot="${HOME}/Backups"
backudate=$(date +"%Y%m%d")

# Löschen alter Exports
find ${backuproot}/* -type d -ctime +${old_days} | xargs rm -rf

# Sichern der Hostpaths eines konkreten Namespaces
do_it() {
ns="${1}"
echo -n "" > /tmp/claims_${ns}.txt
kubectl get pods -n ${ns} | grep -v NAME | awk '{print $1 }' > /tmp/pods.txt
cat /tmp/pods.txt |
# Finden aller Claims im Namespace
while IFS=$'\n' read -r pod
do
  echo "pod: ${pod}"
  kubectl describe -n ${ns} pod ${pod} | grep -i claimname >> /tmp/claims_${ns}.txt
done
# Sortieren auf unique
sort -u  /tmp/claims_${ns}.txt > /tmp/claims_${ns}_unique.txt
cat /tmp/claims_${ns}_unique.txt |
# Finden aller Claims im Namespace
while IFS=' ' read -r xname xclaim
do
  echo "Name: ${xname} Claim: ${xclaim}"
  kubectl -n ${ns} describe persistentvolumeclaims ${xclaim} > /tmp/claims_${ns}_${xclaim}.txt
  volumemode=$(cat /tmp/claims_${ns}_${xclaim}.txt | grep -i VolumeMode: | awk '{print $2 }')
  storageclass=$(cat /tmp/claims_${ns}_${xclaim}.txt | grep -i StorageClass: | awk '{print $2 }')
  volume=$(cat /tmp/claims_${ns}_${xclaim}.txt | grep -i Volume: | awk '{print $2 }')
  echo "VolumeMode: ${volumemode} Storageclass: ${storageclass} Volume: ${volume}"

  path="NIX"
  if [ "${storageclass}x" == "nfsx" ];
  then
    path="/var/snap/microk8s/common/nfs-storage"
  elif [ "${storageclass}x" == "openebs-hostpathx" ];
  then
    path="/var/snap/microk8s/common/var/openebs/local"
  fi

  if [ ! "${path}x" == "NIXx" ];
  then
    mkdir -p ${backuproot}/${backudate}/${ns}
    rm -f ${backuproot}/${backudate}/${ns}/${storageclass}_${xclaim}_${volume}.tar.gz
    # Skript soll als Root laufen
    cat << EOF > /tmp/${ns}_${storageclass}_${xclaim}_${volume}.sh
#!/bin/bash
############################################################################################
# Sichern ${path}/${volume}
# Ausgabe auf ${backuproot}/${backudate}/${ns}/${storageclass}_${xclaim}_${volume}.tar.gz
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
#shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
cd ${path}/${volume} || exit
tar -czvf ${backuproot}/${backudate}/${ns}/${storageclass}_${xclaim}_${volume}.tar.gz .
EOF
    chmod +x /tmp/${ns}_${storageclass}_${xclaim}_${volume}.sh
    sudo /tmp/${ns}_${storageclass}_${xclaim}_${volume}.sh
  else
    echo "Keine exportierbare Konfiguration ${ns}/${storageclass}_${xclaim}_${volume}"
  fi
done
}

kubectl get namespaces | grep -v NAME | awk '{print $1 }' > /tmp/namespaces.txt
cat /tmp/namespaces.txt |
while IFS=' ' read -r xname
do
  echo "Namespace: ${xname}"
  do_it "${xname}"
done
