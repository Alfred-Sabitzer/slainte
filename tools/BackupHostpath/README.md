# Backup Hostpath

Dieses Skript loopt über alle Namespaces und erzeugt eine Sicherung.
Weiters werden alte Sicherungen gelöscht.

# Ausführung

```bash
./Backup_Hostpath.sh
```
Damit werden alle PVC's getared und gezippt.

# To Do

Man sollte eine "Auschluß-Liste" einbauen, für PVC's die anders gebackuped werden (z.b. Datenbanken).
Auch bei der Nextcloud sollte kein Backup notwendig sein (da ja jeder selbst sein lokales Backup hat).