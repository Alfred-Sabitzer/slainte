#!/bin/bash
############################################################################################
#
# Erzeugen der Credentials für das basic-auth htpasswd
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
#shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
docker_username=$(cat ${HOME}/.password/docker_basis_username.txt)
docker_password=$(cat ${HOME}/.password/docker_basis_password.txt)
docker_email=$(cat ${HOME}/.password/docker_email.txt)
htpasswd -cbB ./auth/htpasswd ${docker_username} ${docker_password}
docker_username=$(cat ${HOME}/.password/docker_username.txt)
docker_password=$(cat ${HOME}/.password/docker_password.txt)
docker_email=$(cat ${HOME}/.password/docker_email.txt)
htpasswd -bB ./auth/htpasswd ${docker_username} ${docker_password}
rm -f ./registry/auth/htpasswd
cp ./auth/htpasswd ./registry/auth/htpasswd
#
