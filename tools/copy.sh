#!/bin/bash
# Kopieren von Daten aus dem Nextcloud-Container
ns="slainte"
app="wordpress-app"
kubectl cp -n ${ns} $(kubectl get pod -n ${ns} | grep -i ${app} | awk '{print $1 }'):/usr/local/bin/ ./${ns}_${app}/ -c ${app}