#!/bin/bash
# Anzeige der Logs für den Service
ns="slainte"
app="wordpress-app"
kubectl logs -n ${ns} deployments/"$(kubectl get deployments -n ${ns} | grep -i ${app} | awk '{print $1 }')" -c ${app} -f