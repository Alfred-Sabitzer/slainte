#!/bin/bash
############################################################################################
#
# Sichern aller PVC's aus einem bestimmten Namespace
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
#shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
ns="mailu"
mkdir -p ${HOME}/backup

kubectl get pods -n ${ns} | grep -v NAME | awk '{print $1 }' > /tmp/pods.txt
cat /tmp/pods.txt |
while IFS=$'\n' read -r pod
do
  echo "pod: ${pod}"
  kubectl describe -n ${ns} pod ${pod} | grep -i claimname > /tmp/claims.txt
  cat /tmp/claims.txt |
  while IFS=' ' read -r cl claimname
  do
    echo "Claimname: ${claimname}"
    kubectl get -n ${ns}  persistentvolumes | grep -i ${claimname} | awk '{print $1 }' > /tmp/pvcs.txt
    cat /tmp/pvcs.txt |
    while IFS=$'\n' read -r pv
    do
      echo "Claimname: ${claimname} PV: ${pv}"
      cd /var/snap/microk8s/common/var/openebs/local/${pv}
      sudo tar -czvf ${HOME}/backup/${claimname}.tar.gz .
    done
  done
done