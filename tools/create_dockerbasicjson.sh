#!/bin/bash
############################################################################################
#
# Erzeugen der Credentials für das pullen der Images aus der Dockerregistry.
# Dieses Credential wird in jedem Namespace gebraucht.
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
#shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
docker_registry="registry.gitlab.com/alfred-sabitzer/slainte"
docker_username=$(cat ${HOME}/.password/docker_basis_username.txt)
docker_password=$(cat ${HOME}/.password/docker_basis_password.txt)
docker_email=$(cat ${HOME}/.password/docker_email.txt)
cat <<EOF > dockerbasicjson.yaml
#
# Das sind die Credentials, damit das Image-Pull aus dem privaten Repository funktioniert.
# Der Eintrag wird pro Namespace gebraucht.
# Erzeugt wird das initial mit
#
# kubectl -n <namespace> create secret docker-registry dockerbasicjson --docker-server=docker.k8s.slainte.at --docker-username=<username> --docker-password=<password>--docker-email=<email>
#
---
EOF
#
kubectl -n admin delete secret dockerbasicjson
kubectl -n admin create secret docker-registry dockerbasicjson \
	--docker-server=${docker_registry} \
	--docker-username=${docker_username} \
	--docker-password=${docker_password} \
	--docker-email=${docker_email}
#