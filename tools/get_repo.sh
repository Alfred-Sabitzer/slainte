#!/bin/bash
############################################################################################
#
# Download des Repos
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
#shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
rm -f ./slainte-master.tar.gz
wget https://gitlab.com/Alfred-Sabitzer/slainte/-/archive/master/slainte-master.tar.gz ./slainte-master.tar.gz
#