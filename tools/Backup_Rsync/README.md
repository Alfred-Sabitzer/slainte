# Backup Rsync

Backupen aller notwendigen PVC's mittels rsync.

# Vorbereitung

```bash
root@v22022112011208453:~# useradd -m slaintebackup
root@v22022112011208453:~# passwd slaintebackup
New password: 
Retype new password: 
passwd: password updated successfully
```
Damit wird der Useraccount angelegt.

```
AllowUsers alfred slaintebackup
```
In /etc/ssh/sshd_config muß der User als Login-Berechtigter eingetragen werden.

In ${HOME}/.ssh/authorized_keys muß der ssh-key eingetragen werden. Dann klappt es auch mit dem Login.


# Ausführung

```bash
./Backup_Rsync_all.sh
```
Damit werden alle PVC's mit rsync kopiert.

# Funktionsweise

Der Inhalt der PVC's wird mit rsync in das Verzeichnis "/home/slaintebackup/backup/*" gesynced.
Weiters werden dann die Berechtigungen auf "chmod o+r" geändert.

# Einbau in Crontab (Beispiel)

```cron
0 1 * * * /home/slainte/tools/Backup_Rsync/Backup_Rsync_all.sh > /var/log/cron/Backup_Rsync_all.cron 2>&1
```
Der Crontab muß im Root-User sein.