#!/bin/bash
############################################################################################
#
# Sichern bestimmter PV's
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
#shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition

mkdir -p /home/slaintebackup/backup/

# Nextcloud
# /var/snap/microk8s/common/var/openebs/local/pvc-dfb887d1-70f5-4232-8858-c63442d0f018

declare -a pvc=(
"/var/snap/microk8s/common/var/openebs/local/pvc-06b6ecf8-ed24-4a0d-a21d-72c899d74698"
"/var/snap/microk8s/common/var/openebs/local/pvc-110a8027-6536-42a9-891e-ae044e9a18ff"
"/var/snap/microk8s/common/var/openebs/local/pvc-1a44ee9a-b555-4c9f-878a-8c84bb935da7"
"/var/snap/microk8s/common/var/openebs/local/pvc-1d987cbc-6e78-4837-8654-267c7df77b7e"
"/var/snap/microk8s/common/var/openebs/local/pvc-1fccffd8-3710-4392-a81e-1bd50c38cca8"
"/var/snap/microk8s/common/var/openebs/local/pvc-2ca47ac9-8743-4c0d-86e1-1c6da02b1a7f"
"/var/snap/microk8s/common/var/openebs/local/pvc-2e9a8ba9-71b8-4623-9058-64d1d8da19a0"
"/var/snap/microk8s/common/var/openebs/local/pvc-328e3f83-76ab-4153-a690-c75e55746830"
"/var/snap/microk8s/common/var/openebs/local/pvc-4b422ff8-47c8-48ae-8797-f803be5cf7aa"
"/var/snap/microk8s/common/var/openebs/local/pvc-4edc2471-8cb9-4e5c-98b2-e569216c6e56"
"/var/snap/microk8s/common/var/openebs/local/pvc-6023ae78-626c-4c5b-84e6-a2a5910e1da5"
"/var/snap/microk8s/common/var/openebs/local/pvc-7ea28f84-0cf8-4825-8841-8d0b6ab6b635"
"/var/snap/microk8s/common/var/openebs/local/pvc-bd4032c5-2138-4657-9eab-0e598440245e"
"/var/snap/microk8s/common/var/openebs/local/pvc-c22de8bc-344e-47d1-912d-f7e6b77c2e6b"
"/var/snap/microk8s/common/var/openebs/local/pvc-d21c1d6d-2722-4700-87e0-eb2f34e6b862"
"/var/snap/microk8s/common/var/openebs/local/pvc-e3bd02be-214d-40d0-b1e3-ee604b7fd1fc"
"/var/snap/microk8s/common/var/openebs/local/pvc-e5e48555-88d3-4a04-861f-3fcbd0f56283"
"/var/snap/microk8s/common/var/openebs/local/pvc-e6fc98d4-32f3-4779-b76b-31f37af72a5b"
"/var/snap/microk8s/common/var/openebs/local/pvc-f5c9675f-1617-48e8-9759-58bd6e5e7990"
"/var/snap/microk8s/common/nfs-storage"
)
for pv in "${pvc[@]}"
do
  #echo ${pv}
  outdir=${pv%/*}
  #echo ${outdir}
  /usr/bin/rsync -r -t -p -o -g -v --mkpath --progress --delete -u -s ${pv} /home/slaintebackup/backup${outdir}
done
chmod -R o+rx /home/slaintebackup/backup
