#!/bin/bash
############################################################################################
#
# Manipulieren der microk8s.registry
# https://gist.github.com/Kevinrob/4c7f1e5dbf6ce4e94d6ba2bfeff37aeb
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
#shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
docker_registry="https://docker.slainte.at"
docker_username=$(cat ${HOME}/.password/docker_username.txt)
docker_password=$(cat ${HOME}/.password/docker_password.txt)
repositories=$(curl -u ${docker_username}:${docker_password} -s ${docker_registry}/v2/_catalog)
for repo in $(echo "${repositories}" | jq -r '.repositories[]'); do
  echo $repo
  tags=$(curl -u ${docker_username}:${docker_password} -s "${docker_registry}/v2/${repo}/tags/list" | jq -r '.tags[]')
  for tag in $tags; do
    echo "      "$repo:$tag
  done
done
#
