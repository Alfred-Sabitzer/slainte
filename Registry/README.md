# Docker Registry im Cluster

Diese Registry-UI ist von aussen sichtbar. In diesem Falle https://dockerui.slainte.at.
Die Registry dahinter ist unter https://docker.slainte.at erreicbar. Beide benutzen diesselben keys 
(kommen vom Server und werden über letsencrypt bereitgestellt).


# Zweck dieser Registry

Diese Registry enthält alle Images, die im Kubernetes Cluster verwendet werden (mit Ausnahme der Basis-Images, die zur Startzeit vorhanden sein müssen)

# Test der Registry

Die prinzipielle Erreichbarkeit und Verfügbarkeit kann mit curl getestet werden.

```sh
docker_registry="docker.slainte.at"
docker_username=$(cat ${HOME}/.password/docker_username.txt)
docker_password=$(cat ${HOME}/.password/docker_password.txt)
curl -u ${docker_username}:${docker_password}  -k -v https://${docker_registry}/v2/_catalog 
```
Beide Abfragen sollten vernünftige Ergebnisse liefern.

# Einspielen der Passworte

Damit die Secure-Registry abgefragt werden kann, muß das richtige Passwort in jedem Namespace vorhanden sein.
Dazu wird das dockerconfigjson Password verwendet, das sich bereits im Namespace admin befindet.