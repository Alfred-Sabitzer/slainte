# Helm Chart Test

Erzeugen und ausrollen eines eigenen Helm-Charts.
Dieses Helm-Chart wird dann in Argo-CD eingebunden.

Muster:

https://phoenixnap.com/kb/create-helm-chart

```bash
sudo snap install helm --classic
helm create helm-chart-test
Creating helm-chart-test
```
Dann entstehen mehrere Template-Files.

```bash
tree
.
├── helm-chart-test
│   ├── charts
│   ├── Chart.yaml
│   ├── templates
│   │   ├── deployment.yaml
│   │   ├── _helpers.tpl
│   │   ├── hpa.yaml
│   │   ├── ingress.yaml
│   │   ├── NOTES.txt
│   │   ├── serviceaccount.yaml
│   │   ├── service.yaml
│   │   └── tests
│   │       └── test-connection.yaml
│   └── values.yaml
```
Diese Dateien müssen dann individuell angepasst werden.
Wir ändern ein paar Namen.

Nun folgen wir der Anleitung aus https://codefresh.io/learn/argo-cd/argo-cd-helm-chart/
Dazu müssen wir aber erst pushen:)

Dann kann man das in ArgoCD einrichten. Und es läuft:

![ArgoCD_Helm_Chart_Test.png](ScreenShots%2FArgoCD_Helm_Chart_Test.png)

Die Applikation sieht dann so aus:

```yaml
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: helm-chart-test
  namespace: argocd
spec:
  project: slainte
  source:
    path: Helm-Chart-Test/helm-chart-test
    repoURL: https://gitlab.com/Alfred-Sabitzer/slainte.git
    targetRevision: master
    helm:
      valueFiles:
        - values.yaml
  destination:
    namespace: secrets-test
    server: https://kubernetes.default.svc
```
Somit kann jede eigene Helm-Applikation leicht eingebunden werden.
Für "Fremde" Helm-Applikationen (wie Mailu) sollte man sich das Chart und die Values holen, und dann für sich adaptieren.
Dies gibt auch den Freiraum, die vorhandenen Images zu "verbesern" (z.b. Zeitzone).

