#!/bin/bash
############################################################################################
#
# test aller möglichen Verbindungen
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
#
# Slainte
#
curl -k -v http://test.slainte.at		# liefert 308 - Permanent verzogen auf https://
curl -k -v https://test.slainte.at		# liefert 200 - Zeigt die index.html an
curl -k -v https://test.slainte.at/index.html	# liefert 200 - Zeigt die index.html an
curl -k -v https://test.slainte.at/healthz	# liefert 200 - Liefert OK
curl -k -v https://test.slainte.at/unbekannt	# liefert 200 - Zeigt die index.html an
curl -k -v https://test.slainte.at/favicon.ico --output ./favicon.ico # liefert 200 - Zeigt die index.html an
curl -k -v https://test.slainte.at/website_under_construction.jpg	--output ./website_under_construction.jpg # liefert 200 - Liefert die Graphik an
#

