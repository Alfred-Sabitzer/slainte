# NextCloud Installation

NextCloud mit Redis und Postgres. 

# Installation

```bash
./docker/do.sh
```
Damit wird das kokrete Image erzeugt, und im Docker-Repository abgelegt.

Diese Installation benutzt das Apache-Image.

# Adaptionen in der Config.php

Damit die Nextcloud richtig funktioniert, müssen noch ein paar Adaptionen an der Config.php gemacht werden (https://ei23user.de/viewtopic.php?t=24).

```
'overwrite.cli.url' => 'https://nc.slainte.at',
'overwritehost' => 'nc.slainte.at',
'overwriteprotocol' => 'https',
'default_phone_region' => 'at',
```
Sonst bekommt der Client seltsame Fehlermeldungen.

## To Do

Einbinden des Mectric-Exporters
Erzeugen eines Services-Monitors
Erzeugen eines Grafana-Dashboards
Einbinden des Cron-Jobs