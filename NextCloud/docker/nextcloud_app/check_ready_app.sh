#!/bin/sh
############################################################################################
#
# Prüfen, ob php-fpm bereits läuft
#
############################################################################################"
#
app=$(ps -ef | grep -i "apache2 -DFOREGROUND" | grep -v grep)
if [ "${app}x"  == "x"  ] ; then # php-fpm läuft noch nicht
  exit 1
fi
#