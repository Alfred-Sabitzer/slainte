#!/bin/sh
# Disable exit on non 0
set +e
echo "############################################################################################"
echo "#"
echo "# Starten der Nextcloud mit den richtigen Werten."
echo "#"
echo "############################################################################################"
#
#export
#
#sed -i 's|set -eu|set -eux|' /entrypoint.sh
chmod 0770 ${NEXTCLOUD_DATA_DIR}
chown www-data:www-data ${NEXTCLOUD_DATA_DIR}
/Nextcloud_Config.sh &
# Wird als Daemon gestartet
/entrypoint.sh "apache2-foreground"
# Sicherstellen, dass der Prozess oben bleibt
#/dummy.sh
#