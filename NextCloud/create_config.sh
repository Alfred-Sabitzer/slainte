#!/bin/sh
#############################################################################################"
#"
# Erzeugen der configmaps"
#"
#############################################################################################"
#'../dev/make_configmap_dir.sh' ./config ./nextcloud_nginx_config.yaml
#'../dev/make_configmap_dir.sh' ./config/nginxconfig.io ./nextcloud_nginx_config_nginxconfig.yaml
#'../dev/make_configmap_dir.sh' ./config/sites-available ./nextcloud_nginx_config_sites-available.yaml
#'../dev/make_configmap_dir.sh' ./config/sites-enabled ./nextcloud_nginx_config_sites-enabled.yaml
for f in *.yaml
do
  sed -i -r 's/#namespace#/slainte/g' ${f}
done
