#!/bin/sh
# Kopieren von Daten aus dem Nextcloud-Container
mypod=$(kubectl get pod -n slainte | grep -i nextcloud | awk '{print $1 }')
kubectl cp -n slainte ${mypod}:/usr/local/etc/ ./n/ -c nextcloud-app
