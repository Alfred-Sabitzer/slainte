#!/bin/sh
# Connect in den Container
app="wordpress-app"
kubectl exec -i -t -n wordpress "$(kubectl get pod -n wordpress | grep -i ${app} | awk '{print $1 }')" -c ${app} "--" sh -c "clear; (bash || ash || sh)"
