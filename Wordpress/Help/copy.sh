#!/bin/sh
# Kopieren von Daten aus dem Nextcloud-Container
app="wordpress-app"
kubectl cp -n slainte $(kubectl get pod -n slainte | grep -i ${app} | awk '{print $1 }'):/usr/local/bin/ ./m/ -c ${app}