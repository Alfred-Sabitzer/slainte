# Wordpress Installation

Wordpress braucht mysql, und kann nicht mit Postgres.

Darum legen wir jetzt alles in einen eigenen Namespace "wordpress"

Wir installieren unsere MySQL nach diesem Schema https://phoenixnap.com/kb/kubernetes-mysql

Siehe auch https://dev.mysql.com/doc/mysql-operator/en/mysql-operator-introduction.html

Dies ist ein Beispiel für eine "richtige" MySQL-Installation mit InnoDB-Cluster. Für einen Single-Node vielleicht etwas übertrieben.
