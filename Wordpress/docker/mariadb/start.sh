#!/bin/sh
# Disable exit on non 0
set +e
echo "############################################################################################"
echo "#"
echo "# Starten der Mariadb mit den richtigen Werten."
echo "#"
echo "############################################################################################"
#
#export
#
#sed -i 's|set -eu|set -eux|' /entrypoint.sh
#ENTRYPOINT ["docker-entrypoint.sh"]
#CMD ["mariadbd"]
# Sicherstellen, dass der Prozess oben bleibt
/dummy.sh
#