#!/bin/bash
############################################################################################
#
# Bauen und deployen - Wordpress
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
git pull --ff-only
cd ./wordpress_app
./do.sh
#cd ../wordpress_nginx
#./do.sh
cd ../mariadb
./do.sh