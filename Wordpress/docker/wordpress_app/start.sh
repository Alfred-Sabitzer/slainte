#!/bin/sh
# Disable exit on non 0
set +e
echo "############################################################################################"
echo "#"
echo "# Starten der Wordpress mit den richtigen Werten."
echo "#"
echo "############################################################################################"
#
# Wird als Daemon gestartet
#/usr/local/bin/docker-entrypoint.sh  php-fpm --fpm-config /usr/local/etc/php/php-fpm.conf --daemonize=yes &
# Sicherstellen, dass der Prozess oben bleibt
/dummy.sh
#