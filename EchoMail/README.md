# EchoMail

Ein Pod, der Emails beantwortet.

# Design

1. [ ] Pod wird als CronJob mit helm-Methodik realisiert
2. [ ] User ist echo@slainte.at
3. [ ] Basis-Imaage ist Curl-Mail
4. [ ] Empfangene Email wird als Attachment zurückgesandt
5. [ ] Es gibt immer ein Empty-Dir (dient für Variable und Ersatz für /tmp)
6. [ ] Root-Filesystem ist unmutable, Group >= 10000, User >= 1000
7. [ ] Starboard-Empfehlungen werden berücksichtigt

# Helm Chart

Erzeugen und ausrollen eines eigenen Helm-Charts.
Dieses Helm-Chart wird dann in Argo-CD eingebunden.

```bash
helm create echomail
Creating echomail
```

# Installation

Installiert wird das mit einem Helm-Kommando

```bash
 helm install echomail --set namespace=secrets . --values ./values.yaml
NAME: echomail
LAST DEPLOYED: Tue Jan  3 15:07:15 2023
NAMESPACE: default
STATUS: deployed
REVISION: 1
NOTES:
1. Get the application URL by running these commands:
  export POD_NAME=$(kubectl get pods --namespace default -l "app.kubernetes.io/name=echomail,app.kubernetes.io/instance=echomail" -o jsonpath="{.items[0].metadata.name}")
  export CONTAINER_PORT=$(kubectl get pod --namespace default $POD_NAME -o jsonpath="{.spec.containers[0].ports[0].containerPort}")
  echo "Visit http://127.0.0.1:8080 to use your application"
  kubectl --namespace default port-forward $POD_NAME 8080:$CONTAINER_PORT
```
# Deinstallation

```bash
helm uninstall echomail
release "echomail" uninstalled
```