#!/bin/bash
echo "############################################################################################"
echo "#"
echo "# Verarbeiten der Mails"
echo "#"
echo "############################################################################################"
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
#shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition

ls -d -R -A -l ${EML_PARSED}/* > /tmp/dir.txt

#-rw-r--r--    1 root     root          5566 Dec 31 13:16 /EML_PARSED/mail_1.eml.pgp.eml
#drwxr-xr-x    2 root     root          4096 Dec 31 13:16 /EML_PARSED/mail_1.eml.pgp.eml_attachments

if [[ -f /tmp/mails_to_do.txt ]];
then
  COUNTER=0
  cat /tmp/dir.txt |
  while IFS=' ' read -r att size owner group byte month day time xdir
  do
    if [[ -d "${xdir}" ]];
    then
      # $f is a directory
      echo "Dir: ${xdir}"
      let COUNTER++
      cd "${xdir}/"
      pwd
      header=$(cat header.txt)
      rm -f *
      echo "${header}" > header.txt
      cp ${EML_IN}/mail_${COUNTER}.eml ./mail_${COUNTER}.txt
      cp /mail/body.txt ./body.txt
      cp /mail/body.html ./body.html
      python /WriteMail.py --idir="${xdir}/" --ofile=${EML_OUT}/mail_${COUNTER}.eml --pw="${MAIL_SECRET_KEY_PASSWORD}"
      touch /tmp/mails_to_send.txt
    fi
  done
fi
