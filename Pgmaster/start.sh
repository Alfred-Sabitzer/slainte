#!/bin/sh
echo "############################################################################################"
echo "#"
echo "# Starten des pgadmin "
echo "#"
echo "############################################################################################"
#
# https://github.com/pgadmin-org/pgadmin4/blob/master/docs/en_US/import_export_servers.rst
#
echo  -n  "${POSTGRES_HOST}:5432:${POSTGRES_USERNAME}:${POSTGRES_USERNAME}:${POSTGRES_PASSWORD}" > /pgpassfile
sed -i -r 's/"Username": "postgresadmin"/"Username": "'${POSTGRES_USERNAME}'"/g' /pgadmin4/servers.json
sed -i -r 's/"Host": "postgresdb-svc.slainte.svc.cluster.local"/"Host": "'${POSTGRES_HOST}'"/g' /pgadmin4/servers.json
sed -i -r 's/"MaintenanceDB": "postgresadmin"/"MaintenanceDB": "'${POSTGRES_USERNAME}'"/g' /pgadmin4/servers.json
/entrypoint.sh
#/dummy.sh