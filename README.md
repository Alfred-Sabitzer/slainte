# Slainte

Slainte (siehe auch https://de.wiktionary.org/wiki/sl%C3%A1inte) ist der gemeinsame Versuch von ein paar Nerds einen Kubernetes-Node sicher im Internet zu betreiben.

Die Homepage ist auf https://www.slainte.at/ .

<img alt="Slainte.jpg" height="300" src="Slainte.jpg" width="200"/>

Volunteers Welcome.

