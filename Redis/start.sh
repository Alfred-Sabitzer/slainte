#!/bin/sh
echo "############################################################################################"
echo "#"
echo "# Starten der Redis mit den richtigen Werten."
echo "#"
echo "############################################################################################"
#
export REDISCLI_AUTH=$(cat /tmp/redissecret/password)
export REDIS_USER=$(cat /tmp/redissecret/username)
export REDIS_HOSTNAME=$(cat /etc/hostname)
export REDIS_HOSTNAME_FULL=$(hostname  -f)
export MASTER_FDQN=`hostname  -f | sed -e 's/redis-[0-9]\./redis-0./'`

sh /config.sh
redis-server /etc/redis/redis.conf

#/dummy.sh