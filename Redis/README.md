# Redis Image

Bau eines Redis-Image gebaut. Hier werden auch die richtigen Verzeichnisse gesetzt. Die Inhalte kommen dann später aus der Kubernetes-Config.

# Installation

```bash
./do.sh
```
Damit wird das konkrete Image erzeugt, und im Docker-Repository abgelegt.
