#!/bin/sh
#############################################################################################"
#"
# Testen der Redis-Funktionalität
#"
#############################################################################################"
export REDISCLI_AUTH=$(cat /tmp/redissecret/password)
export REDIS_USER=$(cat /tmp/redissecret/username)
export REDIS_HOSTNAME=$(cat /etc/hostname)
export REDIS_HOSTNAME_FULL=$(hostname  -f)
export MASTER_FDQN=`hostname  -f | sed -e 's/redis-[0-9]\./redis-0./'`

if [ "${REDIS_USE_TLS}x" == "YESx" ]; then
  echo "TLS-Configuration for Standard-Port"
  export REDIS_TLS=" --tls --cert /etc/ssl/certs/client.crt --key /etc/ssl/certs/client.key --cacert /etc/ssl/certs/ca.crt "
else
  echo "No TLS-Configuration"
  export REDIS_TLS=" "
fi


echo " validate the redis-cli"
echo redis-cli -h ${MASTER_FDQN} -p 6379 ${REDIS_TLS} auth ${REDIS_USER} ${REDISCLI_AUTH}
redis-cli -h ${MASTER_FDQN} -p 6379 ${REDIS_TLS} auth ${REDIS_USER} ${REDISCLI_AUTH}

echo "CAT-TEST"
cat <<EOF | redis-cli -h ${MASTER_FDQN} -p 6379 ${REDIS_TLS}
auth ${REDIS_USER} ${REDISCLI_AUTH}
PING
EOF

echo "RESPONSE-TEST"
RESPONSE=$(cat <<EOF | redis-cli -h ${MASTER_FDQN} -p 6379 ${REDIS_TLS} | tail -n 1
           auth ${REDIS_USER} ${REDISCLI_AUTH}
           PING
EOF
)

echo "RESPONSE=${RESPONSE}"

echo "ACL-TEST"
cat <<EOF | redis-cli -h ${MASTER_FDQN} -p 6379 ${REDIS_TLS}
auth ${REDIS_USER} ${REDISCLI_AUTH}
ACL LIST
keys *
INFO keyspace
EOF

echo "KEY-SET MASTER"
cat <<EOF | redis-cli -h ${MASTER_FDQN} -p 6379 ${REDIS_TLS}
auth ${REDIS_USER} ${REDISCLI_AUTH}
SET ALFRED SABITZER
SET FRANZ HUMANIC
SET SUSI PRUSI
EOF

echo "KEY-GET Lokaler Node"
cat <<EOF | redis-cli -h ${REDIS_HOSTNAME_FULL} -p 6379 ${REDIS_TLS}
auth ${REDIS_USER} ${REDISCLI_AUTH}
GET ALFRED
EOF

echo "KEY-DISPLAY Lokaler Node"
cat <<EOF | redis-cli -h ${REDIS_HOSTNAME_FULL} -p 6379 ${REDIS_TLS}
auth ${REDIS_USER} ${REDISCLI_AUTH}
keys *
INFO keyspace
EOF