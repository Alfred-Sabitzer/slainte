#!/bin/bash
############################################################################################
#
# Schnell-Installation microk8s - Zweiter Teil
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
indir=$(dirname "$0")
${indir}/MicroK8SKube.sh
${indir}/MicroK8SCommunity.sh
${indir}/MicroK8SOpenEBS.sh
${indir}/MicroK8SNFS.sh
${indir}/MicroK8SHelm.sh
${indir}/MicroK8SIngress.sh
${indir}/MicroK8SCertManager.sh
${indir}/MicroK8SDashboard.sh
${indir}/MicroK8SGrafanaReports.sh
######## ${indir}/MicroK8SSecureRegistry.sh # Passworte
######## ${indir}/MicroK8S_Registry.sh # Wir arbeiten mit der Secure Registry
${indir}/MicroK8SLinkerd.sh
${indir}/MicroK8Starboard.sh
${indir}/MicroK8SArgoCD.sh
#
# Nun ist MicroK8S bereit
#
exit
#
