#!/bin/bash
############################################################################################
#
# Schnell-Installation microk8s - Erster Teil
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
indir=$(dirname "$0")
#
# Vorraussetzung: Sauber installierte Nodes
#
${indir}/MicroK8SInit.sh
sudo usermod -a -G microk8s slainte
sudo chown -f -R slainte ~/.kube
echo "#############################################################################################"
echo "#"
echo "# Basis-Installation wurde geschafft."
echo "# Einmaliges durchführen des Kommandos: "
echo "#"
echo "# ./alias.sh "
echo "#"
echo "# Jetzt ausloggen, wieder einloggen, und SetupMicroK8S.sh ausführen"
echo "#############################################################################################"
exit
#
