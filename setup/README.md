# Setup des Clusters

Dokumentation aller Schritte und Einstellungen, die für das initiale Setup des Clusters notwendig sind.

# Voraussetzung

Secrets wurden vorher mit keepassxc generiert und bereits in das System eingespielt.
Die nachfolgenden Schritte sind notwendig, um den Rechner richtig zu konfigurieren.

## Funktionierendes Ubuntu LTS
Das richtige Image wird über die Netcup-Konsole installiert.

Einrichten der cgroups
Enablen des Swaps

```bash
$ stat -fc %T /sys/fs/cgroup/
cgroup2fs
```
Dieses Kommando prüft, welche Version von cgroups aktiv ist.

Dann ändern der Zeile für den Start des Grub

`GRUB_CMDLINE_LINUX=" net.ifnames=0 video=1024x768"`

in /etc/default/grub auf

`GRUB_CMDLINE_LINUX=" net.ifnames=0 video=1024x768 cgroup_enable=memory cgroup_memory=1 swapaccount=1 systemd.unified_cgroup_hierarchy=0"`

Danach update des grub und

```bash
sudo update-grub
sudo shutdown -r now
```

Danach sollte das System mit cgroups enabled wieder hochkommen.

```bash
dd if=/dev/zero of=/swapfile.img bs=1024 count=3M
mkswap /swapfile.img
chmod 0600 /swapfile.img
nano /etc/fstab # Eintragen von /swapfile.img swap swap sw 0 0
swapon /swapfile.img
cat /proc/swaps
grep 'Swap' /proc/meminfo
shutdown -r now
cat /proc/swaps
grep Swap /proc/meminfo
dmesg
```

## Visudo-Eintrag:

```bash
# Allow members of group sudo to execute any command
%sudo   ALL=(ALL:ALL) NOPASSWD:ALL
```

Alle Schritte werden als user slainte durchgeführt.
Um Slainte zu etablieren muß man als root die folgenden Schritte durchführen.

```bash
adduser slainte
usermod -aG sudo slainte
```
Beispiel:

```bash
root@v22022112011208453:~# adduser slainte
Adding user `slainte' ...
Adding new group `slainte' (1001) ...
Adding new user `slainte' (1001) with group `slainte' ...
Creating home directory `/home/slainte' ...
Copying files from `/etc/skel' ...
New password:
Retype new password:
passwd: password updated successfully
Changing the user information for slainte
Enter the new value, or press ENTER for the default
Full Name []: Slainte User
Room Number []:
Work Phone []:
Home Phone []:
Other []:
Is the information correct? [Y/n] Y
root@v22022112011208453:~# usermod -aG sudo slainte
```
Danach wechselt man in den User Slainte

```bash
root@v22022112011208453:~# su -l slainte
To run a command as administrator (user "root"), use "sudo <command>".
See "man sudo_root" for details.
slainte@v22022112011208453:~$ whoami
slainte
```
# Konfiguration des sshd

```bash
nano /etc/ssh/sshd_config


# Authentication:

LoginGraceTime 2m
StrictModes yes
MaxAuthTries 6
MaxSessions 10

PubkeyAuthentication yes

PasswordAuthentication no
PermitRootLogin no

AllowUsers meier müller huber 
```

Diese Einträge müssen im sshd vorhanden sein. Die Usernamen müssen den richtigen Accounts entsprechen.

# Installation

Am besten kopiert man die Skripten aus dem setup auf den Rechner in das Verzeichnis setup.
Dann kann man die Skripten einfach durchführen.

```bash
slainte@v22022112011208453:~$ mkdir setup
slainte@v22022112011208453:~$ cd setup/
slainte@v22022112011208453:~$ ./Setup.sh
```
Danach ist mikrok8s initial verfügbar.

## Jetzt ausloggen, wieder einloggen, und SetupMicroK8S.sh ausführen

Nach aus-und einlogggen sind die aliase usw. richtig. Dann kann man mit dem Setup weitermachen.

```bash
alfred@k8s:~/setup$ ./SetupMicroK8S.sh 
```

Zusätzlich (siehe https://discuss.kubernetes.io/t/dockerhub-download-rate-limits/18729)
muß man noch dafür sorgen, dass microk8s die richtigen Docker-Settings hat.

```
$ cat /var/snap/microk8s/current/args/certs.d/docker.io/hosts.toml
server = "https://docker.io"

[host."https://registry-1.docker.io"]
capabilities = ["pull", "resolve"]

server = "https://docker.slainte.at"

[host."https:/docker.slainte.at"]
capabilities = ["pull", "resolve"]
```
Dann sollte alles funktionieren.

# Zusatz-Features

Man kann einzelne Services nach außen enablen.

* kubernetes_dashboard_ingress.yaml
* linkerd_ingress.yaml
* grafana_ingress.yaml
* argocd_ingress.yaml
* prometheus-ingress.yaml

* Dazu müssen wir die Ingresse der Umgebung anpassen.

```bash
cd slainte
microk8s kubectl apply -f .
```
Dies ist die Konfiguration für Slainte ("Produktion").

```bash
cd k8s
microk8s kubectl apply -f .
```
Dies ist die Konfiguration für den lokalen Testrechner. 

Damit der Cert-Manager zufrieden ist, müssen alle Hosts am DNS-Server eingetragen sein.

Bitte vorsichtig sein. 

# CLuster Storage

Cluster-Storage wird gebraucht, wenn man eine RWMANX-PVC machen muß (ist in manchmal notwendig).
Longhorn funktioniert derzeit nicht in Kubernetes V25 (wegen PodPolicy).

-> Dies wird automatisch in /MicroK8SNFS.sh gemacht.

# Anzahl Pods per Node

Wir haben nur einen Node (und da müssen alle Pods drauf passen).

Wir vergrößern von der Standardgröße 110 Pods per Node auf 234 (ist einfach eine schöne Zahl).
Dabei folgen wir der Anleitung in https://blog.kubovy.eu/2020/05/16/the-kubernetes-110-pod-limit-per-node/

```bash
cat /var/snap/microk8s/current/args/kubelet
--cluster-dns=10.152.183.10
--cluster-domain=cluster.local
--kubeconfig=${SNAP_DATA}/credentials/kubelet.config
--cert-dir=${SNAP_DATA}/certs
--client-ca-file=${SNAP_DATA}/certs/ca.crt
--anonymous-auth=false
--root-dir=${SNAP_COMMON}/var/lib/kubelet
--log-dir=${SNAP_COMMON}/var/log
--fail-swap-on=false
--feature-gates=DevicePlugins=true
--eviction-hard="memory.available<100Mi,nodefs.available<1Gi,imagefs.available<1Gi"
--container-runtime=remote
--container-runtime-endpoint=${SNAP_COMMON}/run/containerd.sock
--containerd=${SNAP_COMMON}/run/containerd.sock
--node-labels="microk8s.io/cluster=true,node.kubernetes.io/microk8s-controlplane=microk8s-controlplane"
--authentication-token-webhook=true
--read-only-port=0
--max-pods=234

microk8s stop
microk8s start
```

Und dann

```bash
kubectl get nodes -o yaml
apiVersion: v1
items:
- apiVersion: v1
  kind: Node
  metadata:
  annotations:
  csi.volume.kubernetes.io/nodeid: '{"cstor.csi.openebs.io":"v22022112011208453","jiva.csi.openebs.io":"v22022112011208453"}'
  node.alpha.kubernetes.io/ttl: "0"
  projectcalico.org/IPv4Address: 89.58.16.23/22
  projectcalico.org/IPv4VXLANTunnelAddr: 10.1.145.128
  volumes.kubernetes.io/controller-managed-attach-detach: "true"
  creationTimestamp: "2022-11-28T19:49:04Z"
  labels:
  beta.kubernetes.io/arch: amd64
  beta.kubernetes.io/os: linux
  kubernetes.io/arch: amd64
  kubernetes.io/hostname: v22022112011208453
  kubernetes.io/os: linux
  microk8s.io/cluster: "true"
  node.kubernetes.io/microk8s-controlplane: microk8s-controlplane
  topology.cstor.openebs.io/nodeName: v22022112011208453
  topology.jiva.openebs.io/nodeName: v22022112011208453
  name: v22022112011208453
  resourceVersion: "2834719"
  uid: 2197fb65-b26d-4ff9-b9e5-744c3e1088bf
  spec: {}
  status:
  addresses:
    - address: 89.58.16.23
      type: InternalIP
    - address: v22022112011208453
      type: Hostname
      allocatable:
      cpu: "6"
      ephemeral-storage: 658338644Ki
      hugepages-1Gi: "0"
      hugepages-2Mi: "0"
      memory: 16282648Ki
      pods: "234"
      capacity:
      cpu: "6"
      ephemeral-storage: 659387220Ki
      hugepages-1Gi: "0"
      hugepages-2Mi: "0"
      memory: 16385048Ki
      pods: "234"
      conditions:
```
Schon können wir mehr Pods auf unserem Node enablen.
