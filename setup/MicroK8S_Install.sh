#!/bin/bash
############################################################################################
#
# Installieren microk8s
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition

MyVersion="snap install microk8s --classic --channel=1.25/stable"

sudo ${MyVersion}
rc=$?
echo "Returncode: ${rc}"

while  [ ${rc} -gt 0 ]
do
  sleep 30s
  sudo ${MyVersion}
  rc=$?
  echo "Returncode: ${rc}"
done

sudo snap info microk8s | grep -i tracking
sudo snap unalias kubectl
sudo snap alias microk8s.kubectl kubectl

sudo ./MicroK8S_Start.sh
microk8s inspect > microk8s_inspect.log
#
# Jetzt ist microk8s installiert und laufend
#