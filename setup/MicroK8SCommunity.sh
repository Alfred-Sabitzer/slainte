#!/bin/bash
############################################################################################
#
# MicroK8S - Features
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
indir=$(dirname "$0")
microk8s enable dns rbac hostpath-storage
microk8s status --wait-ready
microk8s enable community             # Ermöglichen der Zusatzfeatures
microk8s status --wait-ready
${indir}/check_running_pods.sh
#